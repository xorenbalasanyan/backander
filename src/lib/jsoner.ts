import Sequelize from 'sequelize';
import {
	File,
	Good,
	Incident,
	IncidentDaylyReport,
	IncidentImport,
	IncidentType,
	InfoMessage,
	InfoMessageToAddressee,
	Log,
	Shop,
	ShopManager,
	ShopManagerRole,
	StructureImport,
	Task,
	TaskToShop,
	User,
	ViolationTask,
	ViolationTaskToShop,
} from '../stores/db/entity';
import { FreshTask, FreshViolationTask } from '../stores/db/views';

type JsonObject = any | any[];

type EntityType = {
	id: number,
	fk_owner?: EntityType,
	// fk_df?: EntityType TODO xxx
	// fk_tm?: EntityType TODO xxx
	// fk_upf?: EntityType TODO xxx
	// fk_dms?: EntityType[] TODO xxx
	// fk_zdms?: EntityType[] TODO xxx
	fk_executor?: EntityType,
	fk_rejector?: EntityType,
	fk_shop?: EntityType,
	fk_good?: EntityType,
	fk_file?: EntityType,
	fk_fileType?: EntityType,
	fk_incidentType?: EntityType,
	fk_taskToShops?: EntityType[],
	fk_violationTaskToShops?: EntityType[],
	fk_infoMessageToAddressees?: EntityType[],
	fk_addressee?: EntityType,
	fk_log?: EntityType,
	shopManagers?: EntityType[],
	shopManagerRole?: EntityType,
	user?: EntityType,
	infoMessageId?: number,
	addresseeUserId?: number,
	toDto?: () => JsonObject,
};

type MetaCollection = {
	shopMap?: Map<number, typeof Shop>,
	goodMap?: Map<number, typeof Good>,
	userMap?: Map<number, typeof User>,
	shopManagerMap?: Map<number, typeof ShopManager>,
	shopManagerRoleMap?: Map<number, typeof ShopManagerRole>,
	taskMap?: Map<number, typeof Task>,
	violationTaskMap?: Map<number, typeof ViolationTask>,
	fileMap?: Map<number, typeof File>,
	logMap?: Map<number, typeof Log>,
	incidentTypeMap?: Map<number, typeof IncidentType>,
	infoMessageToAddresseeMap?: Map<string, typeof InfoMessageToAddressee>,
};

/**
 * Получает на вход объект или массив, которые надо подготовить к передаче в Response.
 * Дополнительно перед конвертацией Entity в Dto вырезает другие Entity и размещает в Meta.
 * @param value
 * @returns {[JsonObject, MetaCollection]}
 */
export default function jsoner(value: EntityType | EntityType[]): [JsonObject, JsonObject] {
	const meta: MetaCollection = {};
	const result = convertValue(value, meta);
	// конвертируем каждый Map из meta в Json
	const metaKeys = Object.keys(meta);
	const jsonMeta = metaKeys.reduce((json, metaKey) => {
		json[metaKey] = {};
		Array.from(meta[metaKey].keys()).forEach(mapKey => {
			json[metaKey][mapKey] = meta[metaKey].get(mapKey);
		});
		return json;
	}, {});
	return [result, jsonMeta];
}

function convertValue(value: EntityType | EntityType[], meta: MetaCollection): JsonObject {
	if (value instanceof Array) {
		return value.map(i => convertValue(i, meta));
	} else if (value instanceof Sequelize.Model) {
		return entityMapper(value, meta);
	} else {
		return value;
	}
}

/**
 * Вырезает подчиненные Entity в Entity, складывает их в Meta.
 * Для входящего entity выполняет конвертацию в Dto.
 * @param entity
 * @param {MetaCollection} meta
 */
const entityMapper = (entity: EntityType, meta: MetaCollection) => {
	if (entity instanceof File) {
		if (entity.fk_fileType) {
			addFileType(entity.fk_fileType, meta);
			delete entity.fk_fileType;
		}
	}
	if (entity instanceof Shop) {
		if (entity.shopManagers?.length) {
			entity.shopManagers.forEach(shopManager => {
				addShopManager(shopManager, meta);
			});
			delete entity.shopManagers;
		}
	}
	if (entity instanceof ShopManager) {
		if (entity.shopManagerRole) {
			addShopManagerRole(entity.shopManagerRole, meta);
			delete entity.shopManagerRole;
		}
		if (entity.user) {
			addUser(entity.user, meta);
			delete entity.user;
		}
	}
	// TODO xxx
	/*
	if (entity.fk_df && (entity instanceof Shop || entity instanceof User)) {
		addUser(entity.fk_df, meta);
		delete entity.fk_df;
	}
	if (entity.fk_tm && (entity instanceof Shop || entity instanceof User)) {
		addUser(entity.fk_tm, meta);
		delete entity.fk_tm;
	}
	if (entity.fk_upf && (entity instanceof Shop || entity instanceof User)) {
		addUser(entity.fk_upf, meta);
		delete entity.fk_upf;
	}
	if (entity.fk_dms && (entity instanceof Shop || entity instanceof User)) {
		entity.fk_dms.forEach(i => addUser(i, meta));
		delete entity.fk_dms;
	}
	if (entity.fk_zdms && (entity instanceof Shop || entity instanceof User)) {
		entity.fk_zdms.forEach(i => addUser(i, meta));
		delete entity.fk_zdms;
	}
	 */
	if (entity.fk_owner && (entity instanceof Task || entity instanceof FreshTask || entity instanceof ViolationTask || entity instanceof FreshViolationTask || entity instanceof IncidentImport || entity instanceof StructureImport)) {
		addUser(entity.fk_owner, meta);
		delete entity.fk_owner;
	}
	if (entity.fk_executor && (entity instanceof TaskToShop || entity instanceof ViolationTaskToShop)) {
		addUser(entity.fk_executor, meta);
		delete entity.fk_executor;
	}
	if (entity.fk_rejector && (entity instanceof TaskToShop || entity instanceof ViolationTaskToShop)) {
		addUser(entity.fk_rejector, meta);
		delete entity.fk_rejector;
	}
	if (entity.fk_shop && (entity instanceof User || entity instanceof TaskToShop || entity instanceof ViolationTaskToShop)) {
		addShop(entity.fk_shop, meta);
		delete entity.fk_shop;
	}
	if (entity.fk_good && (entity instanceof Incident)) {
		addGood(entity.fk_good, meta);
		delete entity.fk_good;
	}
	if (entity.fk_incidentType && (entity instanceof IncidentImport || entity instanceof Incident)) {
		addIncidentType(entity.fk_incidentType, meta);
		delete entity.fk_incidentType;
	}
	if (entity.fk_file && (entity instanceof IncidentImport || entity instanceof StructureImport || entity instanceof IncidentDaylyReport)) {
		addFile(entity.fk_file, meta);
		delete entity.fk_file;
	}
	if (entity.fk_log && (entity instanceof IncidentImport || entity instanceof StructureImport)) {
		addLog(entity.fk_log, meta);
		delete entity.fk_log;
	}
	if (entity.fk_taskToShops && (entity instanceof Task || entity instanceof FreshTask)) {
		entity.fk_taskToShops.forEach(i => addTaskToShop(i, meta));
		delete entity.fk_taskToShops;
	}
	if (entity.fk_violationTaskToShops && (entity instanceof ViolationTask || entity instanceof FreshViolationTask)) {
		entity.fk_violationTaskToShops.forEach(i => addViolationTaskToShop(i, meta));
		delete entity.fk_violationTaskToShops;
	}
	if (entity.fk_infoMessageToAddressees && (entity instanceof InfoMessage)) {
		entity.fk_infoMessageToAddressees
			?.forEach(i => addInfoMessageToAddressee(i, meta));
		delete entity.fk_infoMessageToAddressees;
	}
	if (entity.fk_addressee && (entity instanceof InfoMessageToAddressee)) {
		addUser(entity.fk_addressee, meta);
		delete entity.fk_addressee;
	}
	// конвератция entity в Dto
	if (entity.toDto instanceof Function) {
		return entity.toDto();
	} else {
		return undefined;
	}
};

function addItemToMetaMap(mapKey: string, entity: EntityType, meta: MetaCollection) {
	if (!meta[mapKey]) meta[mapKey] = new Map();
	if (!meta[mapKey].has(entity.id)) meta[mapKey].set(entity.id, convertValue(entity, meta));
}

const addFileType = addItemToMetaMap.bind(null, 'fileTypeMap');

const addShop = addItemToMetaMap.bind(null, 'shopMap');

const addGood = addItemToMetaMap.bind(null, 'goodMap');

const addUser = addItemToMetaMap.bind(null, 'userMap');

const addShopManager = addItemToMetaMap.bind(null, 'shopManagerMap');

const addShopManagerRole = addItemToMetaMap.bind(null, 'shopManagerRoleMap');

const addFile = addItemToMetaMap.bind(null, 'fileMap');

const addLog = addItemToMetaMap.bind(null, 'logMap');

const addIncidentType = addItemToMetaMap.bind(null, 'incidentTypeMap');

const addTaskToShop = addItemToMetaMap.bind(null, 'taskToShopMap');

const addViolationTaskToShop = addItemToMetaMap.bind(null, 'violationTaskToShopMap');

// У InfoMessageToAddressee нет id, поэтому используем составной ключ
function addInfoMessageToAddressee(entity: EntityType, meta: MetaCollection) {
	if (!meta.infoMessageToAddresseeMap) meta.infoMessageToAddresseeMap = new Map();
	const key = `${entity.infoMessageId}_${entity.addresseeUserId}`;
	if (!meta.infoMessageToAddresseeMap.has(key)) meta.infoMessageToAddresseeMap.set(key, convertValue(entity, meta));
}
