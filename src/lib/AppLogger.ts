import util from 'util';
import Winston from 'winston';
import chalk from 'chalk';
import { isDev, isProd, isTest } from '~utils/env';
import config from '~lib/config';
import { parseError } from '~utils/errors';
import { validateString } from '~utils/validate';
import LokiWinstonTransport from '~lib/LokiWinstonTransport';
// Levels:
// error: 0
// warn: 1
// info: 2 ==> TEST level
// http: 3 ==> PROD level
// verbose: 4
// debug: 5 ==> DEV level
// silly: 6
// Уровень лога определяется в следующем порядке:
// 1. env LOG_LEVEL
// 2. config.logLevel
// 3. Для тестового окружения => info
// 4. Для разработки => debug
const logLevel = process.env.LOG_LEVEL || config.get('logLevel') || (isTest ? 'info' : isDev ? 'debug' : undefined);
// Проверяем уровень лога.
const availableLogLevels = ['error', 'warn', 'info', 'http', 'verbose', 'debug', 'silly'];
if (!availableLogLevels.includes(logLevel || '')) {
	// tslint:disable-next-line:no-console
	console.error([
		`[Ошибка логгера] значение уровня лога может принимать одно значение из ${availableLogLevels}.`,
		'Для указания уровня лога используется env LOG_LEVEL или переменная конфига logLevel.',
		'Process will exit.',
	].join('\n'));
	process.exit(1);
}

// имя лог файла
const logFileName = process.env.LOGGER_FILE_NAME;
if (isProd && validateString(5, 50)(logFileName || '')) {
	// tslint:disable-next-line:no-console
	console.error(`[Ошибка логгера] env LOGGER_FILE_NAME должна указывать на имя файла лога и иметь длину от 5 до 50 символов.\nProcess will exit.`);
	process.exit(1);
}

let lastLogDate: Date;

// doc: https://github.com/winstonjs/winston
const myConsoleFormat = Winston.format.printf((logMessageInfo): string => {
	const { level, label, ...errorParts } = logMessageInfo;
	const date = new Date();
	const timestamp = date.toISOString();
	const delta = lastLogDate ? date.getTime() - lastLogDate.getTime() : 0;
	lastLogDate = date;
	const deltaStr = `+${delta > 9_000
		? `${Math.floor(delta / 100) / 10}s`
		: delta > 99_000
			? `${Math.floor(delta / 1000)}s`
			: delta > 600_000
				? `${Math.floor(delta / 60000) / 10}m`
				: `${delta}ms`}`.padStart(6);
	let startLine: string;
	if (level === 'debug') {
		startLine = `${timestamp} ${deltaStr} ${chalk.bgWhite.gray(level)} [${chalk.magenta(label)}]`;
	} else {
		const color = level === 'error' ? chalk.bgRed.yellow
			: level === 'warn' ? chalk.red
				: level === 'info' ? chalk.bgCyan.black
					: level === 'http' ? chalk.bgYellow.black
						: chalk.bgWhiteBright.black;
		startLine = `${timestamp} ${deltaStr} ${color(level)} [${chalk.magenta(label)}]`;
	}
	const text = startLine + ' ' + parseError(errorParts);
	return (level === 'debug' ? chalk.gray(text) : text);
});

const lokiFormat = Winston.format.printf((logMessageInfo) => {
	const log = parseError(logMessageInfo);
	return JSON.stringify(log);
});

export class AppLogger
{
	private readonly loggers = new Map<string, Winston.Logger>();

	constructor(private readonly alternativeInstance: string = '') {}

	private initLogger(instance: string): void {
		const options: Winston.LoggerOptions = {
			level: logLevel,
			transports: [],
		};
		const logger = Winston.createLogger(options);

		// пишем логи в консоль
		logger.add(new Winston.transports.Console({
			format: Winston.format.combine(
				Winston.format.label({ label: instance || this.alternativeInstance }),
				myConsoleFormat,
			),
		}));

		// добавляем логирование в loki
		const lokilog = config.get('lokilog');
		if (lokilog) {
			// check
			const batchingInterval: number | false = lokilog.batchingInterval === false ? false
				: Number(lokilog.batchingInterval);
			if (batchingInterval !== false) {
				if (Number.isNaN(batchingInterval) || batchingInterval < 5 || batchingInterval > 60) {
					// tslint:disable-next-line:no-console
					console.error(util.format(
						'[Ошибка настройки логгера] Значение для batchingInterval может быть false или целое [5..60]. Сейчас равно %o',
						batchingInterval));
					process.exit(1);
				}
			}
			logger.add(new LokiWinstonTransport({
				host: lokilog.host,
				timeout: 30,
				basicAuth: [lokilog.username, lokilog.password].join(':'),
				minLevel: lokilog.minLevel || logLevel,
				labels: {
					platform: 'backend',
					serverEnv: process.env.SERVER_ENV, // стенд
					instance,
				},
				format: Winston.format.combine(lokiFormat),
				batchingInterval,
			}));
		}

		// если указан лог файл, то будем писать логи в него
		if (logFileName) {
			logger.add(new Winston.transports.File({
				format: Winston.format.combine(
					Winston.format.label({ label: instance }),
					myConsoleFormat,
				),
				filename: logFileName,
				maxFiles: 25,
			}));
		}

		this.loggers.set(instance, logger);
	}

	private getLogger(instance: string): Winston.Logger {
		if (!this.loggers.has(instance)) {
			this.initLogger(instance);
		}
		return this.loggers.get(instance)!;
	}

	child = (data: any): AppLogger => {
		// TODO добавить расширение логгера с информацией
		const s = 'x';
		return this;
	}

	error = (error: Error, ...optionalParams: any[]): void => {
		const instance = this.alternativeInstance;
		const logger = this.getLogger(instance);
		if (error instanceof Error) {
			logger.error('', error, optionalParams);
		} else {
			logger.error(error, optionalParams);
		}
	}

	warn = (message: any, ...optionalParams: any[]): void => {
		const instance = optionalParams.pop() || this.alternativeInstance;
		const logger = this.getLogger(instance);
		logger.warn(message, optionalParams);
	}

	info = (message: any, ...optionalParams: any[]): void => {
		const instance = optionalParams.pop() || this.alternativeInstance;
		const logger = this.getLogger(instance);
		logger.info(message, optionalParams);
	}

	http = (message: any, ...optionalParams: any[]): void => {
		const instance = optionalParams.pop() || this.alternativeInstance;
		const logger = this.getLogger(instance);
		logger.http(message, optionalParams);
	}

	verbose = (message: any, ...optionalParams: any[]): void => {
		const instance = optionalParams.pop() || this.alternativeInstance;
		const logger = this.getLogger(instance);
		logger.info('verbose', message, optionalParams);
	}

	debug = (message: any, ...optionalParams: any[]): any => {
		const instance = optionalParams.pop() || this.alternativeInstance;
		const logger = this.getLogger(instance);
		logger.debug(message, optionalParams);
	}

	log = (message: any, ...optionalParams: any[]): void => {
		const instance = optionalParams.pop() || this.alternativeInstance;
		const logger = this.getLogger(instance);
		logger.info(message, optionalParams);
	}
}
