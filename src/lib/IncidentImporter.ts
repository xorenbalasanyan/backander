import fs from 'fs';
import util from 'util';
import assert from 'assert';
import csvParser from 'csv-parser';
import { fromHumanDateToSqlDate } from '~utils/date';
import { FileService } from '~services';
import { AppLogger } from './AppLogger';

const VALUE_FORMAT_SPLIT = 'SPLIT';
const VALUE_FORMAT_STRING = 'STRING';
const VALUE_FORMAT_FLOOR = 'FLOOR';
const VALUE_FORMAT_DOUBLE = 'DOUBLE';
const VALUE_FORMAT_HUMAN_DATE = 'HUMAN_DATE';

const AVAILABLE_FORMATS = [VALUE_FORMAT_SPLIT, VALUE_FORMAT_STRING, VALUE_FORMAT_FLOOR,
	VALUE_FORMAT_DOUBLE, VALUE_FORMAT_HUMAN_DATE];

const moduleLogger = new AppLogger('IncidentImporter');

export default class IncidentImporter
{
	private readonly schema: any;
	private readonly _inputFields: any;
	private _rowReaderFoo?: (rowData, rowNumber, warnings) => any;

	constructor(incidentType) {
		this.schema = JSON.parse(incidentType.data);
		this._inputFields = this.checkAndInitSchema(this.schema);
		Object.freeze(this);
	}

	checkAndInitSchema = schema => {
		const inputFields = {};
		assert(Number.isInteger(schema.schemaVersion), `Поле schemaVersion типа натуральное число является обязательным`);
		assert(schema.separator, `Поле separator типа строка является обязательным`);
		// проверяем и готвим формат для первой версии
		if (schema.schemaVersion === 1) {
			const tag = 'для схемы версии 1';
			assert(schema.inputData instanceof Object, `Поле inputData типа объект является обязательным ${tag}`);
			schema.inputData.forEach(field => {
				assert(field.headColumnText, `Поле headColumnText является обязательным ${tag}`);
				assert(field.format, `Поле format является обязательным ${tag}`);
				assert(field.id || field.ids, `Поле id или ids является обязательным ${tag}`);
				assert(AVAILABLE_FORMATS.includes(field.format), `Значение '${field.format}' в поле format не распознано ${tag}`);
				if (field.format === VALUE_FORMAT_SPLIT) {
					assert(field.delimiter, `Поле delimiter является обязательным для типа '${VALUE_FORMAT_SPLIT}' ${tag}`);
					assert(field.ids instanceof Array, `Поле ids типа массив является обязательным для типа '${VALUE_FORMAT_SPLIT}' ${tag}`);
					field.ids.forEach((idMeta, i) => {
						assert(Number.isInteger(idMeta.index), `Поле ids[${i}].index типа целое положительное число является обязательным для типа '${VALUE_FORMAT_SPLIT}' ${tag}`);
						assert(AVAILABLE_FORMATS.includes(idMeta.format), `Поле ids[${i}].format является обязательным для типа '${VALUE_FORMAT_SPLIT}' ${tag}`);
						assert(idMeta.id, `Поле ids[${i}].id типа строка является обязательным для типа '${VALUE_FORMAT_SPLIT}' ${tag}`);
					});
				}
				inputFields[field.headColumnText] = field;
			});
			this._rowReaderFoo = this._readRowV1;
		} else {
			throw new Error(`Обработчик импорта инцидентов не знаком с версией схемы '${schema.schemaVersion}'`);
		}
		return inputFields;
	}

	/**
	 * Импортирует список инцидентов из файла CSV.
	 * @param file: File
	 * @param req
	 * @param skipRowCount Сколько строк с данными пропустить (0 - не пропускать, 1 - одну строку и тд)
	 * @param maxReadRows Сколько строк прочитать
	 * @returns {Promise<{ data: [], warnings: {} }>}
	 */
	importFromFile = (file, req, skipRowCount = 0, maxReadRows = 1000) => {
		const logger = req.getLogger(moduleLogger);
		logger.info(`Читаем файл импорта #${file.id}, строки с ${skipRowCount}, количество строк ${maxReadRows}`);
		const warnings = {
			badCodes: [],
			missingRequiredValues: {},
			noGoodName: [],
		};
		const data: any[] = [];
		const fileName = new FileService(req).getFullPath(file.pathName);
		let rowNumber = 1;
		return new Promise(resolve => {
			const readStream = fs.createReadStream(fileName)
				.pipe(csvParser({ separator: this.schema.separator }))
				.on('data', rowData => {
					// шапка находится в первой строке, поэтому отсчет номеров строк будем вести со строки 2 (надо добавить 1)
					if (rowNumber++ < skipRowCount + 1) return;
					const item = this._rowReaderFoo!(rowData, rowNumber, warnings);
					if (item) {
						data.push(item);
						if (data.length >= maxReadRows) {
							readStream.destroy();
						}
					}
				})
				// end - когда стрим закрывается при достижении файла
				.on('end', () => resolve([data, warnings, rowNumber]))
				// close - когда стрим закрывается при принудительном закрытии через destroy
				.on('close', () => resolve([data, warnings, rowNumber]));
		});
	}

	_readByFormat = (fieldMeta, value, item) => {
		// дефолтного значения быть не может
		const { id, ids, format, delimiter, isRequired } = fieldMeta;
		if (isRequired && (value === null || value === undefined || value === '')) {
			return true;
		}
		if (format === VALUE_FORMAT_SPLIT) {
			// оно значение содержит несколько значений
			const parts = (String(value) || '').split(delimiter);
			return ids.some(idMeta => this._readByFormat({
				format: idMeta.format,
				id: idMeta.id,
			}, parts[idMeta.index], item));
		} else if (format === VALUE_FORMAT_STRING) {
			// значение как строка
			value = value || value === 0 ? String(value) : value;
			item[id] = value;
		} else if (format === VALUE_FORMAT_FLOOR) {
			// значение как целое число
			value = Number(value);
			if (Number.isNaN(value)) {
				return true;
			}
			item[id] = Math.floor(value);
		} else if (format === VALUE_FORMAT_DOUBLE) {
			// значение как вещественное число
			value = Number(value);
			if (Number.isNaN(value)) return true;
			item[id] = value;
		} else if (format === VALUE_FORMAT_HUMAN_DATE) {
			// значение как дата
			value = fromHumanDateToSqlDate(value);
			if (!value) return true;
			item[id] = value;
		} else {
			throw new Error(util.format(`Неизвестный формат '%s' для значения %o`, format, value));
		}
	}

	/**
	 * Читает строку CSV для первой версии схемы типа инцидента.
	 * @param rowData
	 * @param rowNumber
	 * @param warnings
	 * @returns {{data, shopExId: *, goodExId: *, goodName, rowNumber}|undefined}
	 * @private
	 */
	_readRowV1 = (rowData, rowNumber, warnings) => {
		const item: any = { rowNumber };
		const warningId = Object.keys(this._inputFields).find(headColumnText => {
			const value = rowData[headColumnText];
			const fieldMeta = this._inputFields[headColumnText];
			// check required
			if (fieldMeta.isRequired && (value === null || value === undefined || value === '')) {
				return true;
			}
			// читаем значение с учетом формата
			return this._readByFormat(fieldMeta, value, item);
		});
		if (warningId) {
			const column = this._inputFields[warningId].headColumnText;
			if (!warnings.missingRequiredValues[column]) {
				warnings.missingRequiredValues[column] = [];
			}
			warnings.missingRequiredValues[column].push(rowNumber);
			return undefined;
		}
		// проверка обязательных полей
		if (!item.shopExternalId || !item.goodExternalId) {
			warnings.badCodes.push(rowNumber);
			return undefined;
		}
		if (!item.goodName) {
			warnings.noGoodName.push(rowNumber);
			return undefined;
		}
		return item;
	}
}
