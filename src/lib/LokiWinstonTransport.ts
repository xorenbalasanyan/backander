import Transport from 'winston-transport';
import LokiBatcher, { LokiBatcherOptions } from './LokiBatcher';

const MESSAGE = Symbol.for('message');

type LokiWinstonTransportOptions = LokiBatcherOptions & {
	// минимальный уровень отправки логов
	minLevel: string // error|warn|info|http|verbose|debug|silly
	format: any
	labels?: any
};

const LEVEL_INDEX = {
	error: 1,
	warn: 2,
	info: 3,
	http: 4,
	verbose: 5,
	debug: 6,
	silly: 7,
};

/**
 * A Winston transport for Grafana Loki.
 *
 * @class LokiTransport
 * @extends {Transport}
 */
export default class LokiWinstonTransport extends Transport
{
	private batcher: LokiBatcher;
	private labels: any;
	private minLevel?: number;

	/**
	 * Creates an instance of LokiTransport.
	 * @param {*} options
	 * @memberof LokiTransport
	 */
	constructor(options: LokiWinstonTransportOptions) {
		super(options);

		// Pass all the given options to batcher
		this.batcher = new LokiBatcher({
			host: options.host,
			basicAuth: options.basicAuth,
			batchingInterval: options.batchingInterval,
			clearOnError: options.clearOnError,
			replaceTimestamp: options.replaceTimestamp,
			timeout: options.timeout,
		});

		this.labels = options.labels;
		this.minLevel = LEVEL_INDEX[options.minLevel || ''];
	}

	/**
	 * An overwrite of winston-transport's log(),
	 * which the Winston logging library uses
	 * when pushing logs to a transport.
	 *
	 * @param {*} info
	 * @param {*} callback
	 * @memberof LokiTransport
	 */
	log(info, callback) {
		// Immediately tell Winston that this transport has received the log.
		setImmediate(() => {
			this.emit('logged', info);
		});

		// Deconstruct the log
		const { label, labels, timestamp, level } = info;

		if (this.minLevel) {
			const levelIndex = LEVEL_INDEX[level];
			// уровень ниже минимального
			if (levelIndex > this.minLevel) return;
		}

		// build custom labels if provided
		let lokiLabels: any = { level };
		lokiLabels = Object.assign(lokiLabels, labels);

		if (this.labels) {
			lokiLabels = Object.assign(lokiLabels, this.labels);
		} else {
			lokiLabels.job = label;
		}

		// follow the format provided
		const line = info[MESSAGE];

		// Make sure all label values are strings
		lokiLabels = Object.fromEntries(
			Object.entries(lokiLabels).map(
				([key, value]: [string, any]) => [key, value ? value.toString() : value]));

		// Construct the log to fit Grafana Loki's accepted format
		const logEntry = {
			labels: lokiLabels,
			entries: [
				{
					ts: timestamp || Date.now().valueOf(),
					line,
				},
			],
		};

		// Pushes the log to the batcher
		this.batcher.pushLogEntry(logEntry)
			.catch(error => {
				// tslint:disable-next-line:no-console
				console.error('[Ошибка отправки логов в Loki]', error);
			});

		// Trigger the optional callback
		callback();
	}

	/**
	 * Send batch to loki when clean up
	 */
	close() {
		this.batcher.close(undefined);
	}
}
