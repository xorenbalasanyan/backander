import FirebaseAdmin from 'firebase-admin';
import { AppLogger } from '~lib/AppLogger';

const logger = new AppLogger('initFirebaseAdmin');
let fbAdminInitialized = false;

export default function initFirebaseAdmin(configPath) {
	logger.info(`Init from Firebase config file: ${configPath}`);
	FirebaseAdmin.initializeApp({
		credential: FirebaseAdmin.credential.cert(configPath),
	});
	fbAdminInitialized = true;
}

export function isFirebaseInitialized() {
	return fbAdminInitialized;
}
