import fs from 'fs';
import HttpsRequester from '~lib/Requester';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const logger = new AppLogger('SelectelRequester');

export default class SelectelRequester
{
	private readonly caller: HttpsRequester;
	private readonly username: string;
	private readonly password: string;
	private readonly accountPath: string;
	private readonly container: string;
	private token: string | undefined;

	constructor(username: string, password: string, accountPath: string, container: string) {
		this.caller = new HttpsRequester('api.selcdn.ru');
		this.username = username;
		this.password = password;
		this.accountPath = accountPath;
		this.container = container;
	}

	/**
	 * Авторизация
	 */
	auth = () => new Promise((resolve, reject) => {
		this.caller.postJson(
			'/v2.0/tokens',
			undefined,
			{
				auth: {
					passwordCredentials: {
						username: this.username,
						password: this.password,
					},
				},
			},
			(error, traceId, response, buffer) => {
				if (error) {
					logger.error(fillError(Error, '[Ошибка авторизации в Selectel]', error as Error));
					return reject(error);
				} else {
					try {
						const data = JSON.parse(buffer?.toString() || '0');
						this.token = data?.access?.token?.id;
						if (this.token) {
							resolve(undefined);
						} else {
							reject(new Error('Пришел пустой токен'));
						}
					} catch (error1) {
						return reject(error1);
					}
				}
			});
	})

	/**
	 * Отправка файла в хранилище
	 * @param from - расположение в файловой системе
	 * @param to - путь до файла в хранилище, например `path1/path2/file.txt`
	 */
	sendFile = (from: string, to: string) => new Promise((resolve, reject) => {
		// проверка файла
		if (!fs.existsSync(from)) {
			reject(new Error(`Файл "${from}" не существует`));
			return;
		}
		this.caller.put(
			`/v1/${this.accountPath}/${this.container}/${to}`,
			{ 'X-Auth-Token': this.token },
			fs.readFileSync(from),
			(error, traceId, response, responseBody) => {
				if (error) {
					return reject(error);
				}
				if (response && response.statusCode === 201) {
					logger.info(`Загрузился файл "${from}" в хранилище Selectel по пути "${to}"`);
					return resolve(undefined);
				}
				// eslint-disable-next-line max-len
				logger.warn(`[Ошибка отправки файла в Selectel] Запрос ${traceId}. Не удалось отправить файл "${from}" в хранилище по пути "${to}", потому что в ответе вернулся статус ${response?.statusCode} ${response?.statusMessage} и такое тело ответа: ${String(responseBody)}`);
				return reject(new Error(`Файл "${from}" не загрузился в Selectel, статус ${response?.statusCode} ${response?.statusMessage}`));
			});
	})

	/**
	 * Удаление файла из хранилища
	 * @param from - путь файла в хранилище
	 */
	deleteFile = (from: string): Promise<void> => new Promise((resolve, reject) => {
		const path = `/v1/${this.accountPath}/${this.container}/${from}`;
		this.caller.delete(
			path,
			{ 'X-Auth-Token': this.token },
			(error, traceId, response, responseBody) => {
				if (error) {
					return reject(error);
				}
				if (response && response.statusCode === 204) {
					logger.info(`Удалился файл "${from}" в хранилище Selectel по пути "${from}"`);
					return resolve();
				}
				// eslint-disable-next-line max-len
				logger.warn(`[Ошибка удаления файла из Selectel] Запрос ${traceId}. Не удалось удалить файл "${from}" в хранилище по пути "${from}", потому что в ответе вернулся статус ${response?.statusCode} ${response?.statusMessage} и такое тело ответа: ${String(responseBody)}`);
				return reject(new Error(`Файл "${from}" не удален в Selectel, статус ${response?.statusCode} ${response?.statusMessage}`));
			});
	})
}
