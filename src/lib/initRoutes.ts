import { Router } from 'express';
import { applyRoutes } from '~middlewares';
import controllers from '~controllers';

/**
 * Настройка роутов и доступа к ним.
 * В списке ролей необходимо указать роли пользователей, для которых будут доступны роуты.
 */
export default applyRoutes(controllers, Router());
