import fs from 'fs';
import { FileLocationEnum } from '~enums/FileLocationEnum';
import {
	DATABASE_READ_ERROR,
	FILE_NOT_FOUND,
	NOT_ALL_REQUIRED_FIELDS,
} from '~errors/ResponseError';
import { FileService } from '~services';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const moduleLogger = new AppLogger('fileDownloadController');

export default [
	/**
	 * Пользователь скачивает файл.
	 * При скачивании увеличивается счетчик скачиваний для файла.
	 */
	{
		getPath: '/api/file/download/:fileId',
		anyAuthorizedRole: true,
		doNotLogNormalStatus: true,
		handler: async (req: any, res: any) => {
			const logger = req.getLogger(moduleLogger);
			const { fileId } = req.params;
			if (!fileId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const user = req.getAuthorizedUser();
			logger.debug(`User #${user.id} download file #${fileId}`);
			const fileService = new FileService(req);
			return fileService.findOneById(fileId)
				.then((file) => {
					if (!file) {
						return res.sendHttpError(FILE_NOT_FOUND);
					}
					if (file.location === FileLocationEnum.CDN) {
						return res.redirect(301, file.pathName);
					}
					const fullPath = fileService.getFullPath(file.pathName);
					if (!fs.existsSync(fullPath)) {
						return res.status(404).end();
					}
					// картинки мутабельны и для них не нужно подсчитывать количество скачиваний,
					// поэтому можно добавить заголовки с кешем на 1 сутки
					if (file.name.match(/\.(png|jpg|jpeg|gif)$/)) {
						// похоже не работает, запросы все равно идут на бэк
						res.set('Cache-control', 'public, max-age=84600');
						res.set('Expires', new Date(Date.now() + 84600e3).toString());
					}
					res.sendFile(fullPath);
					return file.update({
						downloadCount: file.downloadCount + 1,
						moderatorId: user.id,
					});
				})
				.catch(error => {
					logger.error(fillError(Error, '[Ошибка при чтении File]', error));
					res.sendHttpError(DATABASE_READ_ERROR);
				});
		},
	},
];
