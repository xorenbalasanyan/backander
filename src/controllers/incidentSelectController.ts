import {
	NOT_ALL_REQUIRED_FIELDS,
	REQUESTED_ITEM_NOT_FOUND,
	USER_HAS_NO_ACCESS_TO_CHAGE_VALUE,
} from '~errors/ResponseError';
import { toUTCSqlDate } from '~utils/date';
import {
	GoodRepository,
	IncidentRepository,
	IncidentTypeRepository,
	ShopRepository,
} from '~repositories';
import { Incident } from '../stores/db/entity';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('incidentSelectContainer');

/**
 * Возвращает список магазинов, доступных пользователю
 */
async function getShopsByUser(user) {
	if (user.isAdmin || user.isSupport || user.isDt) {
		return ShopRepository.findAllByAccountId(user.accountId);
	} else if (user.isDf) {
		return ShopRepository.findAllByAccountIdAndDfId(user.accountId, user.id);
	} else if (user.isTm) {
		return ShopRepository.findAllByAccountIdAndTmId(user.accountId, user.id);
	} else if (user.isUpf) {
		return ShopRepository.findAllByAccountIdAndUpfId(user.accountId, user.id);
	} else if (user.isDm || user.isZdm) {
		return user.getFk_shop().then(shop => [shop]);
	}
}

/**
 * Возвращает списки магазинов и инцидентов, доступных пользователю на текущее время
 */
async function getShopsAndIncidents(logger, user) {
	// в первую очередь необходимо получить список магазинов
	// получение списка магазинов зависит от роли пользователя
	const shops = await getShopsByUser(user);
	// для каждого магазина определяем текущее время в его часовой зоне
	// формируем карту суток и магазинов
	// например, если на сервере сейчас `01.01.2020 21:00 UTC`, то для Москвы нужно выбрать
	// инциденты за `01.01.2020`, а в Екатеринбурге уже нужно выбрать инциденты за `02.01.2020`
	// аналогично для отрицательных часовых поясов
	const now = new Date();
	const dayMinutes = now.getUTCHours() * 60 + now.getUTCMinutes();
	const today = toUTCSqlDate(now);
	now.setDate(now.getDate() + 1);
	const nextday = toUTCSqlDate(now);
	now.setDate(now.getDate() - 2);
	const prevday = toUTCSqlDate(now);
	const date2shop: any = {
		[prevday]: [],
		[today]: [],
		[nextday]: [],
	};
	shops.forEach((shop: any) => {
		const shopDm = dayMinutes + shop.timeOffset;
		const key = shopDm < 0 ? prevday : shopDm <= 1440 ? today : nextday;
		date2shop[key].push(shop.id);
	});
	// выполняем три запроса - для каждого дня
	const incidents: any[] = [];
	await Promise.all(Object.keys(date2shop)
		.filter(date => date2shop[date].length > 0)
		.map(date => IncidentRepository.findAllByDateAndShopIds(date, date2shop[date])
			.then(items => incidents.push(...items)),
		));
	return { shops, incidents };
}

/**
 * Проверка доступа к инциденту для пользователя.
 * Пользователь должен относиться к магазину, в котором случился инцидент.
 */
async function checkAccessToIncident(user, incidentId, res) {
	if (!incidentId) {
		res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
		return false;
	}
	const incident: any = await Incident.findOne({ where: { id: incidentId } });
	if (!incident) {
		res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
		return false;
	}
	const shops = await getShopsByUser(user);
	const shop = shops.find(s => s.id === incident.shopId);
	if (!shop) {
		res.sendHttpError(USER_HAS_NO_ACCESS_TO_CHAGE_VALUE);
		return false;
	}
	return incident;
}

export default [
	{
		/**
		 * Получение данных об одном инциденте по id
		 */
		getPath: '/api/incident/item/:incidentId',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const incident = await checkAccessToIncident(req.getAuthorizedUser(), req.params.incidentId, res);
			if (!incident) return undefined;
			res.json(incident.toDto());
		},
	},
	{
		/**
		 * Получение заданий на текущие сутки
		 */
		getPath: '/api/incident/work-data',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const logger = req.getLogger(moduleLogger);
			const user = req.getAuthorizedUser();
			const { shops, incidents } = await getShopsAndIncidents(logger, user);
			// получаем список типов инцидентов
			const incidentTypeIds = Array.from(new Set(incidents.map(i => i.incidentTypeId)));
			const incidentTypes = await IncidentRepository.findAllIncidentTypeByUserAndIds(user, incidentTypeIds);
			// получаем список товаров
			const goodIds = incidents.map(ig => ig.goodId);
			const goodRepository = new GoodRepository(req);
			const goods = await goodRepository.findAllByIds(goodIds);
			res.json({
				shops: shops.map(i => i.toDto()),
				incidents: incidents.map((i: any) => i.toDto()),
				incidentTypes: incidentTypes.map(i => i.toDto()),
				goods: goods.map(i => i.toDto()),
			});
		},
	},
	/**
	 * Получение типов инцидентов
	 */
	{
		getPath: '/api/incident/types_OLD/:ids',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const { ids } = req.params;
			const currentUser = req.getAuthorizedUser();
			if (!ids) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			await IncidentTypeRepository.findAllByAccountAndIds(currentUser.accountId, ids.split(','))
				.then(items => res.json(items.map((i: any) => i.toDto())))
				.finally(() => res.end());
		},
	},
];
