import { Op } from 'sequelize';
import { ResponseErrorEnum } from '~enums/ResponseErrorEnum';
import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError, { NOT_ALL_REQUIRED_FIELDS } from '~errors/ResponseError';
import { checkSqlDate } from '~utils/date';
import { IncidentTypeRepository } from '~repositories';
import { IncidentServiceJsonImporter } from '~services';
import { Good, Shop } from '../stores/db/entity';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('incidentImportController');

export default [
	{
		/**
		 * В этом запросе проверяем номенклатуру и магазины.
		 * Если магазин отсутствует, сообщим об этом.
		 * Если номенклатура отсутствует, сообщим об этом.
		 */
		putPath: '/api/incident/import/check',
		accessRoles: [UserRoles.ADMIN],
		handler: async (req: any, res: any) => {
			const logger = req.getLogger(moduleLogger);
			const currentUser = req.getAuthorizedUser();
			const checkData = req.body;
			logger.debug(`Request /api/incident/import/check: ${JSON.stringify(checkData)}`);

			// список магазинов и товаров
			// в этих списках сначала будут храниться externalId магазинов и товаров из списка инцидентов
			const shopExIds = new Set();
			const goodExIds = new Set();
			checkData.forEach((item) => {
				shopExIds.add(`${item.shopExId}`); // Shop.externalId is VARCHAR
				goodExIds.add(`${item.goodExId}`); // Good.externalId is VARCHAR
			});

			logger.debug(`Id магазинов в checkData: (${shopExIds.size}) ${Array.from(shopExIds)}`);
			logger.debug(`Id товаров в checkData: (${goodExIds.size}) ${Array.from(goodExIds)}`);

			// вычеркиваем из списка магазины и товары, которые есть в БД
			const shops = await Shop.findAll({
				where: {
					accountId: currentUser.accountId,
					externalId: {
						[Op.in]: Array.from(shopExIds),
					},
				},
			} as any);
			shops.forEach((shop: any) => shopExIds.delete(shop.externalId));
			const goods = await Good.findAll({
				where: {
					accountId: currentUser.accountId,
					externalId: {
						[Op.in]: Array.from(goodExIds),
					},
				},
			} as any);
			goods.forEach((good: any) => goodExIds.delete(good.externalId));

			logger.debug(`Магазины не существуют в БД: (${shopExIds.size}) ${Array.from(shopExIds)}`);
			logger.debug(`Товары не существуют в БД: (${goodExIds.size}) ${Array.from(goodExIds)}`);

			// сейчас в shopExIds хранятся externalId магазинов, которых нет в БД
			// сейчас в goodExIds хранятся externalId товаров, которых нет в БД
			res.json({
				notExistShopExIds: Array.from(shopExIds),
				notExistGoodExIds: Array.from(goodExIds),
			});
		},
	},
	{
		/**
		 * В этом запросе создаются новые инциденты.
		 * Если магазин отсутствует, инцидент не будет добавлен.
		 * Если номенклатура отсутсвует, то она будет создана.
		 * TODO Deprecated
		 */
		postPath: '/api/incident/import',
		accessRoles: [UserRoles.ADMIN],
		handler: async (req: any, res: any) => {
			throw new Error('Роут не обслуживается');
			const logger = req.getLogger(moduleLogger);
			// проверка входных данных
			const { date, type } = req.query;
			if (!checkSqlDate(date)) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const currentUser = req.getAuthorizedUser();
			const incidentType = await IncidentTypeRepository.findOneWithLastVersionByAccountIdAndType(currentUser.accountId, type);
			if (!incidentType) {
				return res.sendHttpError(new ResponseError(400,
					ResponseErrorEnum.ERROR_CODE_NOT_ALL_REQUIRED_FIELDS,
					`Cannot find IncidentType with type '${type}'`));
			}
			const importData = req.body;
			if (!importData) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			logger.info(`Начат импорт ицнидентов для date='${date}', type='${incidentType}', данные: ${JSON.stringify(importData)}`);
			// import
			return IncidentServiceJsonImporter.importIncidentTasksFromJSON(req, res, date, incidentType, importData);
		},
	},
];
