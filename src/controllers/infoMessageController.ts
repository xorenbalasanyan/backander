import fs from 'fs';
import util from 'util';
import { Op } from 'sequelize';
import { ONE_DAY_MS } from '~enums/DateEnum';
import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError, {
	DATABASE_READ_ERROR,
	DATABASE_WRITE_ERROR,
	NOT_ALL_REQUIRED_FIELDS,
	REQUESTED_ITEM_NOT_FOUND,
	SERVER_ERROR,
	USER_HAS_NO_ACCESS,
} from '~errors/ResponseError';
import { copyFields } from '~utils/copyFields';
import { arrayToMapById } from '~utils/list';
import PageInfo from '~utils/PageInfo';
import { validateIsBoolean, validateString } from '~utils/validate';
import sequelize from '~lib/sequelize';
import { FileService, InfoMessageService, InfoMessageToAddresseeService } from '~services';
import { fileTypeStore, infoMessageStore, infoMessageToAddresseeStore } from '~stores';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import { InfoMessageToAddressee, User } from '../stores/db/entity';

const moduleLogger = new AppLogger('infoMessageController');

export default [
	{
		/**
		 * Получение списка сообщений (всех, либо по номерам)
		 */
		getPath: '/api/infomessages/list/:itemIds?',
		anyAuthorizedRole: true,
		doNotLogNormalStatus: true,
		handler: async (req: any, res: any) => {
			const pageInfo = PageInfo.parseFromObject(req.query);
			const { itemIds } = req.params;
			const currentUser = req.getAuthorizedUser();
			const options: any = { where: {} };
			if (itemIds?.length) {
				options.where.id = itemIds.split(',');
			}
			const infoMessageService = new InfoMessageService(req);
			const infoMessageToAddresseeService = new InfoMessageToAddresseeService(req);
			// набор данных зависит от роли пользователя
			if (currentUser.isAdmin || currentUser.isSupport) {
				await infoMessageService.findAllWithPageInfo(options, pageInfo)
					.then((x: any) => {
						const [infoMessages, pageInfo1] = x;
						const imIds = infoMessages.map(i => i.id);
						// Выполняем работу за sequelize - заполняем адресатов для каждого письма.
						// Sequelize через include в один запрос это делает крайне тяжело,
						// поэтому решил разбить не запросы "полегче".
						return infoMessageToAddresseeService
							.countByStatusAndInfoMessageIds('CREATED', imIds) // TODO заменить на InfoMessageToAddresseeEnum
							.then(createdLinks => infoMessageToAddresseeService
								.countByStatusAndInfoMessageIds('VIEWED', imIds) // TODO заменить на InfoMessageToAddresseeEnum
								.then(viewedLinks => {
									const infoMessageMap = arrayToMapById(infoMessages);
									infoMessages.forEach(i => {
										i.stats = {};
									});
									createdLinks.forEach(addressee => {
										(infoMessageMap.get(addressee.infoMessageId) as any).stats.created = addressee.count;
									});
									viewedLinks.forEach(addressee => {
										(infoMessageMap.get(addressee.infoMessageId) as any).stats.viewed = addressee.count;
									});
								}),
							)
							.then(() => res.sendOk(infoMessages, pageInfo1))
							.catch(error => {
								req.getLogger(moduleLogger).error('[Ошибка при чтении списка InfoMessage]', error);
								res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
							});
					});
			} else if (currentUser.isChief || currentUser.isManager) {
				// выбирает только сообщения для текущего пользователя
				options.include = {
					model: InfoMessageToAddressee,
					as: 'fk_infoMessageToAddressees',
					where: {
						addresseeUserId: currentUser.id,
					},
				};
				// возвращаем пользователю сообщения за неделю
				options.where.externalMessageDate = {
					[Op.or]: [
						{
							[Op.eq]: null,
						},
						{
							[Op.gte]: new Date(Date.now() - 7 * ONE_DAY_MS),
						},
					],
				};
				// добавляем фильтр
				const statusFilter = pageInfo.customParams?.status;
				if (statusFilter) {
					options.include.where.status = statusFilter;
				}
				new InfoMessageService(req).findAll(options)
					.then(res.sendOk)
					.catch(error => {
						req.getLogger(moduleLogger).error('[Ошибка при чтении списка InfoMessage]', error);
						res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
					});
			} else {
				res.sendHttpError(USER_HAS_NO_ACCESS);
			}
		},
	},
	{
		/**
		 * Получение информационного сообщения по id
		 */
		getPath: '/api/infomessages/one/:infoMessageId',
		anyAuthorizedRole: true,
		doNotLogNormalStatus: true,
		handler: async (req: any, res: any) => {
			const { infoMessageId } = req.params;
			if (!infoMessageId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const currentUser = req.getAuthorizedUser();
			const options: any = { where: { id: infoMessageId } };
			if (currentUser.isAdmin || currentUser.isSupport) {
				options.include = [
					{
						model: InfoMessageToAddressee,
						as: 'fk_infoMessageToAddressees',
						include: { // админу нужен список адресатов, чтобы проверить, кто прочитал
							model: User,
							as: 'fk_addressee',
						},
					},
				];
				// } else if (SUPERVISOR_ROLES.includes(currentUser.role) || currentUser.isDm || currentUser.isZdm) { TODO xxx
			} else if (currentUser.isChief || currentUser.isManager) {
				options.include = [
					{
						model: InfoMessageToAddressee,
						as: 'fk_infoMessageToAddressees',
						where: { addresseeUserId: currentUser.id },
					},
				];
			}
			new InfoMessageService(req).findOne(options)
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error(`[Произошла ошибка при чтении InfoMessage]`, error);
					res.sendHttpError(error instanceof ResponseError ? error : DATABASE_READ_ERROR);
				});
		},
	},
	{
		/**
		 * Создание информационного сообщения.
		 * Request body scheme {
		 *   subject: !String(1, 300),
		 *   text: !String(1, 10000),
		 *   isImportant: !Boolean,
		 *   linkAddressees: Integer[]
		 * }
		 */
		postPath: '/api/infomessages/one',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			// check input
			const data = req.body;
			if (!data) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			if (validateString(1, 300)(data.subject)) {
				return res.sendHttpError(ResponseError.validationInput('Поле subject должно быть строкой длиной от 1 до 300'));
			}
			if (validateString(1, 10000)(data.text)) {
				return res.sendHttpError(ResponseError.validationInput('Поле text должно быть строкой длиной от 1 до 10000'));
			}
			if (validateIsBoolean()(data.isImportant)) {
				return res.sendHttpError(ResponseError.validationInput('Поле isImportant должно быть логическим значением'));
			}
			const availableKeys = ['subject', 'text', 'isImportant', 'linkAddressees'];
			const newData = copyFields(data, {}, availableKeys);

			// сохраним html тело в файл
			const fileService = new FileService(req);
			const {
				fileName: name,
				pathName,
				fullPath,
			} = FileService.generateNewFileName('emailmsg-', '.html');
			// TODO удалить файл в случае ошибки
			fs.writeFileSync(fullPath, newData.text);
			delete newData.text;
			const size = fs.statSync(fullPath).size;
			const htmlFileType = await fileTypeStore.findIdByExt('html');
			const savedFile = await fileService.createOne({
				name,
				size,
				fileTypeId: htmlFileType.id,
				pathName,
				ownerId: req.getAuthorizedUser().id,
				accountId: req.getAuthorizedUser().accountId,
			});
			newData.fileId = savedFile.id;

			const logger = req.getLogger(moduleLogger);
			logger.info(util.format('Создание InfoMessage с данными: %o', data));
			new InfoMessageService(req).createOne(newData)
				.then(newInfoMessage => res.sendOk(newInfoMessage.toDto()))
				.catch(error => {
					logger.error(fillError(Error, `Произошла ошибка при создании InfoMessage:`, error));
					res.sendHttpError(error instanceof ResponseError ? error : DATABASE_WRITE_ERROR);
				});
		},
	},
	{
		/**
		 * Обновление информационного сообщения.
		 * Request body scheme {
		 *   subject: String(1, 300),
		 *   text: String(1, 10000),
		 *   isImportant: Boolean,
		 *   linkAddressees: Integer[],
		 *   unlinkAddressees: Integer[]
		 * }
		 */
		putPath: '/api/infomessages/one/:infoMessageId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			// check input
			const data = req.body;
			const { infoMessageId } = req.params;
			if (!data || !infoMessageId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			if (data.subject && validateString(1, 300)(data.subject)) {
				return res.sendHttpError(ResponseError.validationInput('Поле subject должно быть строкой длиной от 1 до 300'));
			}
			const availableKeys = ['subject', 'text', 'isImportant', 'linkAddressees', 'unlinkAddressees'];
			const newData = copyFields(data, {}, availableKeys);
			if (!Object.keys(newData).length) {
				return res.sendHttpError(ResponseError.requiredFields(
					`Тело запроса должно содержать одно из полей: ${availableKeys}`));
			}
			// check exist item
			const infoMessageService = new InfoMessageService(req);
			const infoMessage = await infoMessageService.findOne({
				where: { id: infoMessageId },
				include: { model: InfoMessageToAddressee, as: 'fk_infoMessageToAddressees' },
			});
			if (!infoMessage) {
				return res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
			}
			// update
			const logger = req.getLogger(moduleLogger);
			logger.info(util.format(`Обновление InfoMessage #${infoMessageId} с данными: %o`, data));
			infoMessageService.updateOne(infoMessage, newData)
				.then(newInfoMessage => {
					res.sendOk(newInfoMessage.toDto());
				})
				.catch(error => {
					logger.error(fillError(Error, `Произошла ошибка при обновлении InfoMessage #${infoMessageId}:`, error));
					res.sendHttpError(error instanceof ResponseError ? error : DATABASE_WRITE_ERROR);
				});
		},
	},
	{
		/**
		 * Удаление информационного сообщения
		 */
		deletePath: '/api/infomessages/one/:infoMessageId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			// check input
			const { infoMessageId } = req.params;
			if (!infoMessageId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			// check exist item
			const infoMessageService = new InfoMessageService(req);
			const infoMessage = await infoMessageService.findOne({
				where: { id: infoMessageId },
				include: { model: InfoMessageToAddressee, as: 'fk_infoMessageToAddressees' },
			});
			if (!infoMessage) {
				return res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
			}
			// delete
			const logger = req.getLogger(moduleLogger);
			logger.info(`Удаление InfoMessage #${infoMessageId}`);
			await sequelize.transaction(transaction => infoMessageService
				.deleteOne(infoMessage, transaction),
			)
				.then(() => res.sendOk())
				.catch(error => {
					logger.error(fillError(Error, `Произошла ошибка при удалении InfoMessage #${infoMessageId}:`, error));
					res.sendHttpError(error instanceof ResponseError ? error : DATABASE_WRITE_ERROR);
				});
		},
	},
	{
		/**
		 * Устанавливает статус VIEWED для информационного сообщения.
		 * Пользователь для InfoMessageToAddressee выбирается по авторизованному пользователю.
		 */
		putPath: '/api/infomessages/one/:infoMessageId/set-viewed',
		accessRoles: [UserRoles.CHIEF, UserRoles.MANAGER],
		handler: async (req: any, res: any) => {
			const logger = req.getLogger(moduleLogger);
			const currentUser = req.getAuthorizedUser();
			const { infoMessageId } = req.params;
			if (!infoMessageId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			logger.info(util.format('Set VIEWED status for InformationMessage.id %o', infoMessageId));
			const options = {
				where: { accountId: currentUser.accountId, id: infoMessageId },
				include: {
					model: InfoMessageToAddressee,
					as: 'fk_infoMessageToAddressees',
					where: { addresseeUserId: currentUser.id, status: 'CREATED' },
				},
			};
			const infoMessage = await infoMessageStore.findOne(options);
			if (!infoMessage) {
				return res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
			}
			const infoMessageToAddressee = infoMessage.fk_infoMessageToAddressees[0];
			await infoMessageToAddresseeStore.updateOne(infoMessageToAddressee, { status: 'VIEWED' });
			res.sendOk();
		},
	},
];
