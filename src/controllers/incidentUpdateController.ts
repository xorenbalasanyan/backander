import fs from 'fs';
import util from 'util';
import ResponseError, {
	DATABASE_WRITE_ERROR,
	NOT_ALL_REQUIRED_FIELDS,
	REQUESTED_ITEM_NOT_FOUND,
	USER_HAS_NO_ACCESS_TO_CHAGE_VALUE,
} from '~errors/ResponseError';
import { UserRoles } from '~enums/UserRoleEnum';
import { IncidentTypeRepository, ShopRepository } from '~repositories';
import { FileService } from '~services';
import { fileTypeStore } from '~stores';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import IncidentStatsCuberBackgroundTask
	from '../background/backgroundTasks/IncidentStatsCuberBackgroundTask';
import { File, Incident } from '../stores/db/entity';

const moduleLogger = new AppLogger('incidentUpdateController');

/**
 * Проверка доступа к инциденту для пользователя.
 * Пользователь должен относиться к магазину, в котором случился инцидент.
 */
async function checkAccessToIncident(user, incidentId, res) {
	if (!incidentId) {
		res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
		return false;
	}
	const incident: any = await Incident.findOne({ where: { id: incidentId } });
	if (!incident) {
		res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
		return false;
	}
	return incident;
}

export default [
	{
		/**
		 * Обновление данных по инциденту
		 */
		putPath: '/api/incident/item/:incidentId',
		// accessRoles: [USER_ROLE_DM, USER_ROLE_ZDM], TODO xxx
		accessRoles: [UserRoles.MANAGER],
		handler: async (req: any, res: any) => {
			const logger = req.getLogger(moduleLogger);
			const user = req.getAuthorizedUser();
			const incident = await checkAccessToIncident(req.getAuthorizedUser(), req.params.incidentId, res);
			if (!incident) {
				return;
			}
			// обновляем данные
			const { state, report: bodyReport } = req.body;
			const incidentType: any = await IncidentTypeRepository.findOneByAccountAndId(req.getAuthorizedUser().accountId, incident.incidentTypeId);
			if (!incidentType) {
				return res.sendHttpError(ResponseError.notFound(`Тип инцидента #${incident.incidentTypeId} не найден или не доступен`));
			}
			const { solutionData } = JSON.parse(incidentType.data);
			if (!solutionData) {
				return res.sendHttpError(ResponseError.notFound(`Поле solutionData для типа инцидента #${incidentType.id} не заполнено`));
			}
			const report = JSON.parse(incident.report || '{}');
			// TODO добавить транзакцию
			try {
				await Promise.all(solutionData.map(async field => {
					const { id: fieldId, type, isRequired } = field;
					const oldValue = report[fieldId];
					const newValue = bodyReport[fieldId];
					if ((oldValue === undefined || oldValue === null) && (newValue === undefined || newValue === null) && isRequired) {
						throw ResponseError.requiredFields('Поле %s является обязательным', fieldId);
					}
					if (newValue === undefined) return undefined;
					// для каждого типа поля применяется свое правило сохранения данных
					if (type === 'CENTRAL_PHOTO') {
						// картинка должна прийти в кодировке base64 и mime type = png
						if (!/data:image\/(png|jpg|jpeg|gif);base64,/.test(newValue)) {
							throw ResponseError.requiredFields('Значение для поля %s должно быть картинкой в кодировке base64. '
								+ 'Поддерживаемые mime-типы: image/png, image/jpg, image/jpeg, image/gif.', fieldId);
						}
						const [found, mimeType, imageExt] = newValue.match('^data:(image/(png|jpg|jpeg|gif));base64,');
						const data = Buffer.from(newValue.substr(found.length), 'base64');
						const imageFileType = await fileTypeStore.findIdByMimeType(mimeType);
						// сохраняем картинку в файл
						const {
							fileName: name,
							pathName,
							fullPath,
						} = FileService.generateNewFileName('', `.${imageExt}`);
						fs.writeFileSync(fullPath, data);
						const size = fs.statSync(fullPath).size;
						const file: any = await File.create({
							name,
							size,
							fileTypeId: imageFileType.id,
							pathName,
							ownerId: user.id,
							accountId: user.accountId,
						});
						report[fieldId] = { fileId: file.id };
					} else {
						report[fieldId] = newValue;
					}
				}));
			} catch (error: any) {
				logger.error(fillError(Error, util.format('Cannot update Incident: %o', incident), error));
				return (error instanceof ResponseError)
					? res.sendHttpError(error)
					: res.sendHttpError(ResponseError.unhandledException(error.message));
			}
			incident.update({ state, report: JSON.stringify(report), moderatorId: user.id })
				.then(() => {
					res.json({ ok: true });
					// запускаем фоновую задачу для обновления кубов
					const currentUser = req.getAuthorizedUser();
					new IncidentStatsCuberBackgroundTask().execute(currentUser.accountId);
				})
				.catch(err => {
					logger.error(fillError(Error, util.format('Cannot update Incident: %o', incident), err));
					res.sendHttpError(DATABASE_WRITE_ERROR);
				});
		},
	},
];
