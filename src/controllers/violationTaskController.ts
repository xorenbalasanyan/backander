import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError, { NOT_ALL_REQUIRED_FIELDS, SERVER_ERROR } from '~errors/ResponseError';
import { distinctListByField } from '~utils/list';
import PageInfo from '~utils/PageInfo';
import { UserService, ViolationTaskService, ViolationTaskToShopService } from '~services';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import { Shop, User, ViolationTaskToShop } from '../stores/db/entity';

const moduleLogger = new AppLogger('violationTaskController');

export default [
	{
		/**
		 * Получение списка нарушений (всех, либо по номерам)
		 */
		getPath: '/api/violation_tasks/list/:violationTaskIds?',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const { violationTaskIds } = req.params;
			const pageInfo = PageInfo.parseFromObject(req.query);
			const { shopId } = pageInfo.customParams || {};
			const options: any = { where: {} };
			const filterStatuses = (pageInfo.customParams?.statuses || '').split(',').filter(s => !!s);
			if (violationTaskIds?.length) {
				options.where.id = violationTaskIds.split(',');
			}
			// набор данных зависит от роли пользователя
			const currentUser = req.getAuthorizedUser();
			const userService = new UserService(req);
			await chargeOptionsByShopIdAndUserRole(options, shopId, currentUser, userService, filterStatuses);
			// вытаскиваем только "свежие" нарушения из view, у которых срок протухания был в течение 30 дней
			return new ViolationTaskService(req).findAllFreshViolationTasks(options)
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error('[Ошибка при чтении списка ViolationTask]', error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Получение нарушения по номеру
		 */
		getPath: '/api/violation_tasks/one/:violationTaskId',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const { violationTaskId } = req.params;
			if (!violationTaskId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const pageInfo = PageInfo.parseFromObject(req.query);
			const { shopId } = pageInfo.customParams || {};
			const options = { where: { id: violationTaskId } };
			// набор данных зависит от роли пользователя
			const currentUser = req.getAuthorizedUser();
			const userService = new UserService(req);
			await chargeOptionsByShopIdAndUserRole(options, shopId, currentUser, userService);
			return new ViolationTaskService(req).findOne(options)
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error(`[Произошла ошибка при чтении ViolationTask]`, error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Создание списка нарушений
		 */
		postPath: '/api/violation_tasks/list',
		accessRoles: [UserRoles.ROBOT],
		handler: async (req: any, res: any) => {
			const { violationTasks } = req.body || {};
			if (!violationTasks) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(`[Создается список ViolationTask] ${JSON.stringify(violationTasks)}`);
			return new ViolationTaskService(req)
				.createItems(violationTasks)
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Произошла ошибка при создании списка ViolationTask]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Создание нарушения
		 * <pre>
		 * Request body scheme {
		 *   title: String(5, 200),
		 *   description: String(0, 2000) | null,
		 *   executionPeriodDays: Integer(min: 1, max: 15),
		 *   subordinateShopIds: Integer[]
		 * }
		 * </pre>
		 */
		postPath: '/api/violation_tasks/one',
		accessRoles: [UserRoles.ROBOT],
		handler: async (req: any, res: any) => {
			const violationTaskData = req.body;
			if (!violationTaskData) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(`[Создается ViolationTask] ${JSON.stringify(violationTaskData)}`);
			return new ViolationTaskService(req)
				.createOne(violationTaskData)
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Произошла ошибка при создании ViolationTask]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Обновление нарушения по внутреннему id
		 */
		putPath: '/api/violation_tasks/one/:violationTaskId',
		accessRoles: [UserRoles.ROBOT, UserRoles.MANAGER],
		handler: async (req: any, res: any) => {
			const { violationTaskId } = req.params;
			if (!violationTaskId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const violationTaskData = req.body;
			if (!violationTaskData) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			res.json({ inWork: true, violationTaskId, violationTaskData });
		},
	},
	{
		/**
		 * Обновление нарушения по внешнему id
		 */
		putPath: '/api/violation_tasks/one',
		accessRoles: [UserRoles.ROBOT],
		handler: async (req: any, res: any) => {
			const pageInfo = PageInfo.parseFromObject(req.query);
			const { externalId } = pageInfo.customParams || {};
			if (!externalId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const violationTaskData = req.body;
			if (!violationTaskData) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			res.json({ inWork: true, externalId, violationTaskData });
		},
	},
	{
		/**
		 * Сохранение решения задачи по нарушению
		 * TODO Разобраться
		 */
		putPath: '/api/violation_tasks/one___',
		accessRoles: [UserRoles.ROBOT],
		handler: async (req: any, res: any) => {
			const { violationTaskToShopId } = req.params;
			if (!violationTaskToShopId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const data = req.body;
			if (!data) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(`[Сохраняется решение ViolationTaskToShop] ${JSON.stringify({
				violationTaskToShopId,
				data,
			})}`);
			return new ViolationTaskToShopService(req).executeOne(violationTaskToShopId, data)
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Произошла ошибка при сохранении решения ViolationTaskToShop]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Сохранение решения задачи по нарушению
		 */
		putPath: '/api/violation_tasks/execute/:violationTaskToShopId',
		accessRoles: [UserRoles.MANAGER],
		handler: async (req: any, res: any) => {
			const { violationTaskToShopId } = req.params;
			if (!violationTaskToShopId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const data = req.body;
			if (!data) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(`[Сохраняется решение ViolationTaskToShop] ${JSON.stringify({
				violationTaskToShopId,
				data,
			})}`);
			return new ViolationTaskToShopService(req).executeOne(violationTaskToShopId, data)
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Произошла ошибка при сохранении решения ViolationTaskToShop]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Отклонение задачи по нарушению
		 */
		putPath: '/api/violation_tasks/reject/:violationTaskToShopId',
		accessRoles: [UserRoles.CHIEF, UserRoles.MANAGER],
		handler: async (req: any, res: any) => {
			const { violationTaskToShopId } = req.params;
			if (!violationTaskToShopId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const data = req.body;
			if (!data) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(`[Отклоняется решение ViolationTaskToShop] ${JSON.stringify({
				violationTaskToShopId,
				data,
			})}`);
			return new ViolationTaskToShopService(req).rejectOne(violationTaskToShopId, data)
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Произошла ошибка при отклонении ViolationTaskToShop]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
];

async function chargeOptionsByShopIdAndUserRole(options, shopId, user, userService, filterStatuses?: string[]) {
	if (user.isRobot || user.isDt || user.isDf || user.isTm || user.isUpf) {
		// определяем список подчиненных магазинов
		let shopIds;
		if (shopId) {
			shopIds = [shopId];
		} else {
			shopIds = distinctListByField(await userService.getSubordinateShopList(user), 'id');
		}
		options.include = [
			{
				model: ViolationTaskToShop,
				as: 'fk_violationTaskToShops',
				where: {
					shopId: shopIds,
				},
				include: [
					{ model: User, as: 'fk_executor', required: false },
					{ model: User, as: 'fk_rejector', required: false },
					{
						model: Shop,
						as: 'fk_shop',
						required: false,
						// TODO xxx
						/*
						include: [
							{ model: User, as: 'fk_df', required: false },
							{ model: User, as: 'fk_tm', required: false },
							{ model: User, as: 'fk_upf', required: false },
						],
						 */
					},
				],
			},
			{ model: User, as: 'fk_owner' },
		];
		if (filterStatuses?.length) {
			options.include[0].where.status = filterStatuses;
		}
	} else if (user.isDm || user.isZdm) {
		options.include = [
			{
				model: ViolationTaskToShop,
				as: 'fk_violationTaskToShops',
				where: { shopId: user.shopId },
				include: [
					{ model: User, as: 'fk_executor', required: false },
					{ model: User, as: 'fk_rejector', required: false },
					{ model: Shop, as: 'fk_shop', required: false },
				],
			},
			{ model: User, as: 'fk_owner' },
		];
		if (filterStatuses?.length) {
			options.include[0].where.status = filterStatuses;
		}
	}
}
