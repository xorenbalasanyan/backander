import util from 'util';
import { DATABASE_WRITE_ERROR } from '~errors/ResponseError';
import { AppLogger } from '~lib/AppLogger';
import { UserSettingRepository } from '~repositories';
import { fillError } from '~utils/errors';
import { UserSetting } from '../stores/db/entity';

const moduleLogger = new AppLogger('firebaseController');

export default [
	/**
	 * Обновление токена FCM для пользователя
	 */
	{
		putPath: '/api/firebase/user/fcm',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const currentUser = req.getAuthorizedUser();
			const logger = req.getLogger(moduleLogger);
			const { token } = req.body;
			const userFcmSetting: any = await UserSettingRepository.findOneFcmTokenByUserId(currentUser.id);
			Promise.resolve()
				.then(() => {
					if (!userFcmSetting) {
						// create one user setting
						logger.info(`Creating FCM token User setting: ${token}`);
						return UserSetting.create({
							userId: currentUser.id,
							key: UserSettingRepository.KEY_FCM_TOKEN,
							value: token,
							ownerId: currentUser.id,
						});
					} else if (userFcmSetting.value !== token) {
						// update exist setting
						logger.info(`Updating FCM token User setting to: ${token}`);
						return userFcmSetting.update({
							value: token,
							moderatorId: currentUser.id,
						});
					} else {
						logger.info('No need to update FCM token User setting because values is identical');
					}
				})
				.then(() => {
					res.sendOk();
				})
				.catch((error) => {
					logger.error(fillError(Error, util.format('Error when FCM token saves'), error));
					res.sendHttpError(DATABASE_WRITE_ERROR);
				});
		},
	},
];
