import { UserRoles } from '~enums/UserRoleEnum';
import { IncidentTypeService } from '~services';
import PageInfo from '~utils/PageInfo';
import ResponseError, { NOT_ALL_REQUIRED_FIELDS, SERVER_ERROR } from '~errors/ResponseError';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const moduleLogger = new AppLogger('incidentTypesController');

export default [
	{
		/**
		 * Получение данных о типе инцидента.
		 */
		getPath: '/api/incident/types/list/:requiredIds?',
		accessRoles: [UserRoles.ADMIN],
		handler: (req, res) => {
			const options: any = { where: {} };
			const { requiredIds } = req.params;
			if (requiredIds?.length) {
				options.where.id = requiredIds.split(',');
			}
			const incidentTypeService = new IncidentTypeService(req);
			const pageInfo = PageInfo.parseFromObject(req.query);
			return incidentTypeService.findAllWithPageInfo(options, pageInfo)
				.then(([items, pageInfo1]) => res.sendOk(items, pageInfo1))
				.catch(error => {
					const logger = req.getLogger(moduleLogger);
					logger.error(fillError(Error, '[Ошибка при чтении списка IncidentType]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		getPath: '/api/incident/types/one/:id',
		accessRoles: [UserRoles.ADMIN],
		handler: (req, res) => {
			const { id } = req.params;
			if (!id) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const incidentTypeService = new IncidentTypeService(req);
			return incidentTypeService.findOneById(id)
				.then(res.sendOk)
				.catch(error => {
					const logger = req.getLogger(moduleLogger);
					logger.error(fillError(Error, '[Ошибка при чтении IncidentType]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
];
