import { SearchService, UserService } from '~services';
import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError, { NOT_ALL_REQUIRED_FIELDS, SERVER_ERROR } from '~errors/ResponseError';
import PageInfo from '~utils/PageInfo';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const moduleLogger = new AppLogger('searchController');

export default [
	{
		/**
		 * Поиск пользователей.
		 */
		getPath: '/api/search/users',
		accessRoles: [UserRoles.ADMIN, UserRoles.CHIEF],
		handler: async (req: any, res: any) => {
			const pageInfo = PageInfo.parseFromObject(req.query);
			const userService = new UserService(req);
			await userService.findAllWithPageInfo({ where: {} }, pageInfo)
				.then(([items, pageInfo1]) => res.sendOk(items, pageInfo1))
				.catch(error => {
					const logger = req.getLogger(moduleLogger);
					logger.error(fillError(Error, '[Ошибка при поиске пользователей]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Поиск подчиненных менеджеров и магазинов.
		 */
		getPath: '/api/search/subordinates',
		// accessRoles: [USER_ROLE_DT, USER_ROLE_DF, USER_ROLE_TM, USER_ROLE_UPF], TODO xxx
		accessRoles: [UserRoles.CHIEF],
		handler: async (req: any, res: any) => {
			let caughtError: any = false;
			const { query, offset } = req.query;
			if (!query) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			if (offset && (!Number.isInteger(offset) || offset <= 0)) {
				return res.sendHttpError(ResponseError.validationInput(`Значение offset должно быть натуральным числом`));
			}
			const searchService = new SearchService(req);
			const logger = req.getLogger(moduleLogger);
			const managers = await searchService.findSubordinateManagers(query, offset)
				.catch(error => caughtError = error);
			if (caughtError) {
				logger.error(fillError(Error, '[Ошибка при поиске подчиненных менеджеров]', caughtError));
				return res.sendHttpError(caughtError);
			}
			const shops = await searchService.findSubordinateShops(query, offset)
				.catch(error => caughtError = error);
			if (caughtError) {
				logger.error(fillError(Error, '[Ошибка при поиске подчиненных магазинов]', caughtError));
				return res.sendHttpError(caughtError);
			}
			res.sendOk({
				shops: shops?.map(i => i.toDto()),
				managers: managers?.map(i => i.toDto()),
			});
		},
	},
];
