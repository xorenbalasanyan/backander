import util from 'util';
import { ResponseErrorEnum } from '~enums/ResponseErrorEnum';
import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError, {
	CSV_FILE_REQUIRED,
	NOT_ALL_REQUIRED_FIELDS,
	SERVER_ERROR,
} from '~errors/ResponseError';
import { checkSqlDate } from '~utils/date';
import { IncidentTypeRepository } from '~repositories';
import { IncidentService, IncidentServiceJsonExporter } from '~services';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const moduleLogger = new AppLogger('taskIntegrationController');

export default [
	{
		/**
		 * В этом запросе проверяем номенклатуру и магазины.
		 * Если магазин отсутствует, сообщим об этом.
		 * Если номенклатура отсутствует, сообщим об этом.
		 */
		putPath: '/api/tasks/import/incident',
		accessRoles: [UserRoles.ROBOT],
		handler: async (req: any, res: any) => {
			try {
				let caughtError: any = false;
				const logger = req.getLogger(moduleLogger);
				// проверка входных данных
				const { date, type } = req.query;
				if (!checkSqlDate(date)) {
					return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
				}
				const currentUser = req.getAuthorizedUser();
				const incidentType = await IncidentTypeRepository
					.findOneWithLastVersionByAccountIdAndType(currentUser.accountId, type)
					.catch(error => caughtError = error);
				if (caughtError) throw caughtError;
				if (!incidentType) {
					return res.sendHttpError(new ResponseError(400,
						ResponseErrorEnum.ERROR_CODE_NOT_ALL_REQUIRED_FIELDS,
						`Cannot find IncidentType with type '${type}'`));
				}
				if (!req.csvTmpFileName) {
					return res.sendHttpError(CSV_FILE_REQUIRED);
				}
				// import
				logger.info(util.format('Начат импорт инцидентов для date=%o, type=%s', date, incidentType.type));
				await new IncidentService(req)
					.importIncidentTasksFromCSV(res, date, incidentType, req.csvTmpFileName)
					.catch(error => caughtError = error);
				if (caughtError) throw caughtError;
			} catch (error: any) {
				const logger = req.getLogger(moduleLogger);
				logger.error(fillError(Error, '[Ошибка импорта инцидентов]', error));
				res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
			}
		},
	},

	/**
	 * Выгрузка данных по инцидентам
	 */
	{
		getPath: '/api/tasks/export/incident',
		accessRoles: [UserRoles.ROBOT],
		handler: async (req: any, res: any) => {
			try {
				let caughtError: any = false;
				const logger = req.getLogger(moduleLogger);
				// проверка входных данных
				const { date, type } = req.query;
				if (!checkSqlDate(date)) {
					return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
				}
				const currentUser = req.getAuthorizedUser();
				// TODO: сделать выгрузку инцидентов по версии типа инцидента
				const incidentType = await IncidentTypeRepository.findOneByAccountIdAndType(currentUser.accountId, type);
				if (!incidentType) {
					return res.sendHttpError(new ResponseError(400,
						ResponseErrorEnum.ERROR_CODE_NOT_ALL_REQUIRED_FIELDS,
						`Cannot find IncidentType with type '${type}'`));
				}
				// export
				logger.info(util.format('Начат экспорт результатов инцидентов для date=%o, type=%s', date, type));
				await IncidentServiceJsonExporter
					.exportIncidentResultData(req, res, date, incidentType)
					.catch(error => caughtError = error);
				if (caughtError) throw caughtError;
			} catch (error: any) {
				const logger = req.getLogger(moduleLogger);
				logger.error(fillError(Error, '[Ошибка экспорта инцидентов]', error));
				res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
			}
		},
	},
];
