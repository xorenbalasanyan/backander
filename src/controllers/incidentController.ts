import moment from 'moment';
import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError, { NOT_ALL_REQUIRED_FIELDS, SERVER_ERROR } from '~errors/ResponseError';
import { distinctListByField } from '~utils/list';
import PageInfo from '~utils/PageInfo';
import { validateInteger } from '~utils/validate';
import { CubeIncidentStatsService, IncidentService, ShopService, UserService } from '~services';
import { Good, IncidentType, Shop, ShopDiscount } from '../stores/db/entity';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('incidentController');

export default [
	{
		/**
		 * Получение списка инцидентов (всех, либо по номерам) за текущие сутки
		 */
		getPath: '/api/incidents/list/:incidentIds?',
		accessRoles: [UserRoles.CHIEF, UserRoles.MANAGER],
		doNotLogNormalStatus: true,
		handler: async (req: any, res: any) => {
			const { incidentIds } = req.params;
			const pageInfo = PageInfo.parseFromObject(req.query);
			const { shopId } = pageInfo.customParams || {};
			const options: any = {
				where: {},
				attributes: [
					'id', 'shopId', 'shopDiscountId', 'incidentTypeId', 'incidentTypeVersion',
					'goodId', 'date', 'state'],
			};
			const filterStatuses = (pageInfo.customParams?.statuses || '').split(',').filter(s => !!s);
			if (incidentIds?.length) {
				options.where.id = incidentIds.split(',');
			}
			// набор данных зависит от роли пользователя
			const currentUser = req.getAuthorizedUser();
			const userService = new UserService(req);
			const shopService = new ShopService(req);
			await chargeOptionsByShopIdAndUserRole(options, shopId, currentUser, userService, shopService, filterStatuses);
			return new IncidentService(req).findAll(options)
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error('[Ошибка при чтении списка Incident]', error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Получение инцидента по номеру
		 */
		getPath: '/api/incidents/one/:incidentId',
		anyAuthorizedRole: true,
		doNotLogNormalStatus: true,
		handler: async (req: any, res: any) => {
			const { incidentId } = req.params;
			if (!incidentId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const pageInfo = PageInfo.parseFromObject(req.query);
			const { shopId } = pageInfo.customParams || {};
			const options = { where: { id: incidentId } };
			// набор данных зависит от роли пользователя
			const currentUser = req.getAuthorizedUser();
			const userService = new UserService(req);
			const shopService = new ShopService(req);
			await chargeOptionsByShopIdAndUserRole(options, shopId, currentUser, userService, shopService);
			return new IncidentService(req).findOne(options)
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error(`[Произошла ошибка при чтении Incident]`, error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Получение дерева статистики по инцидентам для менеджеров
		 */
		getPath: '/api/incidents/manager-stats',
		// accessRoles: [USER_ROLE_DT, USER_ROLE_DF, USER_ROLE_TM, USER_ROLE_UPF],
		accessRoles: [UserRoles.CHIEF, UserRoles.MANAGER], // TODO xxx
		doNotLogNormalStatus: true,
		handler: async (req: any, res: any) => {
			const pageInfo = PageInfo.parseFromObject(req.query);
			const filterStatuses = (pageInfo.customParams?.statuses || '').split(',').filter(s => !!s);
			// запрос данных
			const cubeIncidentStatsService = new CubeIncidentStatsService(req);
			return cubeIncidentStatsService.getManagerStats(filterStatuses)
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error(`[Произошла ошибка при чтении статистики по инцидентам]`, error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Получение списка типов инцидентов для магазина.
		 * Если не указан shopId, получаем список для всех магазинов.
		 */
		getPath: '/api/incidents/type-stats/:shopId',
		accessRoles: [UserRoles.CHIEF, UserRoles.MANAGER],
		doNotLogNormalStatus: true,
		handler: async (req: any, res: any) => {
			const shopId = Number(req.params.shopId);
			if (!shopId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			// shopId должно быть положительный целым числом, заодно проверяем инъекции
			const check = validateInteger(1, Number.MAX_SAFE_INTEGER)(shopId);
			if (check) {
				return res.sendHttpError(ResponseError.validationInput(`Номер магазина указан с ошибкой: ${check}`));
			}
			const pageInfo = PageInfo.parseFromObject(req.query);
			const filterStatuses = (pageInfo.customParams?.statuses || '').split(',').filter(s => !!s);
			// запрос данных
			const cubeIncidentStatsService = new CubeIncidentStatsService(req);
			return cubeIncidentStatsService.getIncidentTypeStatByShopId(shopId, filterStatuses)
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error(`[Произошла ошибка при чтении статистики по типам инцидентов]`, error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Получение списка товаров по магазину и типу инцидента.
		 * Дополнительно возвращаем описание типа инцидента.
		 */
		getPath: '/api/incidents/good/list/:shopId/:incidentTypeId',
		accessRoles: [UserRoles.CHIEF, UserRoles.MANAGER],
		doNotLogNormalStatus: true,
		handler: async (req: any, res: any) => {
			const shopId = Number(req.params.shopId);
			if (!shopId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			// shopId должно быть положительный целым числом, заодно проверяем инъекции
			const check = validateInteger(1, Number.MAX_SAFE_INTEGER)(shopId);
			if (check) {
				return res.sendHttpError(ResponseError.validationInput(`Номер магазина указан с ошибкой: ${check}`));
			}
			const currentUser = req.getAuthorizedUser();
			const shop = await new ShopService(req).findOne({
				where: {
					id: shopId,
					accountId: currentUser.accountId,
				},
				attributes: ['timeOffset'],
				raw: true,
			});
			if (!shop) {
				return res.sendHttpError(ResponseError.notFound(`Магазин #${shopId} не найден`));
			}
			const incidentTypeId = Number(req.params.incidentTypeId);
			if (!incidentTypeId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			// incidentTypeId должно быть положительный целым числом, заодно проверяем инъекции
			const check1 = validateInteger(1, Number.MAX_SAFE_INTEGER)(incidentTypeId);
			if (check1) {
				return res.sendHttpError(ResponseError.validationInput(`Номер типа инцидента указан с ошибкой: ${check}`));
			}
			const pageInfo = PageInfo.parseFromObject(req.query);
			const filterStatuses = (pageInfo.customParams?.statuses || '').split(',').filter(s => !!s);
			// определяем сутки в магазине
			const date = moment().utcOffset(shop.timeOffset).format('YYYY-MM-DD');
			// запрос данных
			const options: any = {
				where: {
					shopId,
					incidentTypeId,
					date,
				},
				include: [
					{
						model: Good,
						as: 'fk_good',
						attributes: ['id', 'externalId', 'name'],
						required: false,
					},
					{
						model: IncidentType,
						as: 'fk_incidentType',
						required: false,
					},
				],
			};
			if (filterStatuses?.length) {
				options.where.state = filterStatuses;
			}
			const incidentService = new IncidentService(req);
			return incidentService.findAllWithPageInfo(options, pageInfo)
				.then(([items, pageInfo1]) => res.sendOk(items, pageInfo1))
				.catch(error => {
					req.getLogger(moduleLogger).error(`[Произошла ошибка при чтении Incident]`, error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
];

async function chargeOptionsByShopIdAndUserRole(options, shopId, user, userService, shopService, filterStatuses?: string[]) {
	// определяем часовой пояс пользователя
	let timeOffset;
	if (user.idDm || user.isZdm) {
		const shop = await shopService.findOneById(user.shopId);
		timeOffset = shop.timeOffset;
	} else {
		// FIXME для менеджеров выбираем часовой пояс хардкодом
		timeOffset = 300;
	}
	// дата для инцидентов определяется по часовому поясу пользователя
	const date = moment().utcOffset(timeOffset).format('YYYY-MM-DD');
	options.where.date = date;

	if (user.isDt || user.isDf || user.isTm || user.isUpf) {
		// определяем список подчиненных магазинов
		let shopIds;
		if (shopId) {
			shopIds = [shopId];
		} else {
			shopIds = distinctListByField(await userService.getSubordinateShopList(user), 'id');
		}
		options.where.shopId = shopIds;
		options.include = [
			{ model: IncidentType, as: 'fk_incidentType', required: false },
			{
				model: Shop,
				as: 'fk_shop',
				required: false,
				// TODO xxx
				/*
				include: [
					{ model: User, as: 'fk_df', required: false },
					{ model: User, as: 'fk_tm', required: false },
					{ model: User, as: 'fk_upf', required: false },
				],
				 */
			},
			{ model: ShopDiscount, as: 'fk_shopDiscount', required: false },
		];
		if (filterStatuses?.length) {
			options.where.status = filterStatuses;
		}
	} else if (user.isDm || user.isZdm) {
		options.where.shopId = user.shopId;
		options.include = [
			{ model: IncidentType, as: 'fk_incidentType', required: false },
			{ model: Shop, as: 'fk_shop', required: false },
			{ model: ShopDiscount, as: 'fk_shopDiscount', required: false },
		];
		if (filterStatuses?.length) {
			options.where.status = filterStatuses;
		}
	}
}
