import { RegionService } from '~services';

export default [
	{
		/**
		 * Получение списка регионов
		 */
		getPath: '/api/regions',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			await new RegionService(req).findAll({ where: {} })
				.then(items => res.sendOk(items.map(i => i.toDto())));
		},
	},
];
