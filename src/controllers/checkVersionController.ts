import { Request } from 'express';
import config from '~lib/config';

export default [
	/**
	 * Проверка версии мобильного приложения Android
	 */
	{
		getPath: '/api/check-version/android',
		publicAccess: true,
		doNotLogNormalStatus: true,
		handler: async (req: Request, res: any) => {
			const appVersion = req.headers['mobile-app-version'] || '';
			const minAndroidVersion = config.get('mobileApp:minAndroidVersion');
			res.sendOk({ checkPassed: compareVersions(String(appVersion), minAndroidVersion) });
		},
	},
	/**
	 * Проверка версии мобильного приложения iOs
	 */
	{
		getPath: '/api/check-version/ios',
		publicAccess: true,
		doNotLogNormalStatus: true,
		handler: async (req: Request, res: any) => {
			const appVersion = req.headers['mobile-app-version'] || '';
			const minIosVersion = config.get('mobileApp:minIosVersion');
			res.sendOk({ checkPassed: compareVersions(String(appVersion), minIosVersion) });
		},
	},
];

// сравнивает две версии
function compareVersions(appVer: string, minVer: string): boolean {
	const parts1 = appVer.split('.').map(s => Number(s));
	const parts2 = minVer.split('.').map(s => Number(s));
	if (parts1.length < parts2.length) {
		return false;
	}
	return !parts2.some((b, index) => b > parts1[index]);
}
