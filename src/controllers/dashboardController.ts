import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError, { SERVER_ERROR } from '~errors/ResponseError';
import PageInfo from '~utils/PageInfo';
import { IncidentImportService, StructureImportService } from '~services';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const moduleLogger = new AppLogger('dashboardController');

export default [
	{
		/**
		 * Выгружает историю загрузки инцидентов.
		 */
		getPath: '/api/dashboard/incident/import',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			const options = { where: {} };
			const incidentImportService = new IncidentImportService(req);
			const pageInfo = PageInfo.parseFromObject(req.query);
			return incidentImportService.findAllWithPageInfo(options, pageInfo)
				.then(([items, pageInfo1]: any) => res.sendOk(items, pageInfo1))
				.catch((error: Error) => {
					const logger = req.getLogger(moduleLogger);
					logger.error(fillError(Error, '[Ошибка при чтении истории импорта инцидентов]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Выгружает историю загрузки структуры.
		 */
		getPath: '/api/dashboard/structure/import',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			const options = { where: {} };
			const structureImportService = new StructureImportService(req);
			const pageInfo = PageInfo.parseFromObject(req.query);
			structureImportService.findAllWithPageInfo(options, pageInfo)
				.then(([items, pageInfo1]: any) => res.sendOk(items, pageInfo1))
				.catch((error: Error) => {
					const logger = req.getLogger(moduleLogger);
					logger.error(fillError(Error, '[Ошибка при чтении истории импорта структуры]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
];
