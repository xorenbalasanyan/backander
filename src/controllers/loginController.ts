import util from 'util';
import {
	DATABASE_WRITE_ERROR,
	NOT_ALL_REQUIRED_FIELDS,
	REQUESTED_ITEM_NOT_FOUND,
	USER_NOT_FOUND,
} from '~errors/ResponseError';
import { UserRoles } from '~enums/UserRoleEnum';
import { UserService } from '~services';
import { authCredentialStore, userStore } from '~stores';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('loginController');

export default [
	{
		/**
		 * Авторизация пользователя в системе
		 */
		postPath: '/api/login',
		publicAccess: true,
		handler: async (req: any, res: any) => {
			const logger = req.getLogger(moduleLogger);
			const { username, password: key } = req.body;
			if (!username || !key) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			// TODO: убрать способ авторизации по логину и паролю и использовать метод авторизации по хешу пароля
			// const passwordHash = md5(username + md5(key));
			// logger.info(`Find by username='${username}' and passwordHash='${passwordHash}'`);
			// const user = await UserService.findOne({ where: { passwordHash } });
			logger.debug(util.format('Авторизация пользователя с кредами: %o', req.body));
			const creds = await authCredentialStore.findOne({
				where: { username, key },
				attributes: ['userId'],
				raw: true,
			});
			if (!creds) return res.sendHttpError(USER_NOT_FOUND);
			const user = await userStore.findOne({ where: { id: creds.userId } });
			return (!user)
				? res.sendHttpError(USER_NOT_FOUND)
				: UserService.registerAuthToken(req, res, user);
		},
	},

	/**
	 * Повторная авторизация пользователя в системе по refresh токену
	 */
	{
		postPath: '/api/relogin',
		publicAccess: true,
		handler: async (req: any, res: any) => {
			const logger = req.getLogger(moduleLogger);
			const userAuth: any = await UserService.getUserByRefreshTokenFromRequest(req);
			if (!userAuth) {
				return UserService.clearAuthCookie(res)
					.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
			}
			logger.debug(`Found userAuth #${userAuth.id} by refreshToken from cookie`);
			const user = await userAuth.getFk_user();
			return UserService.registerAuthToken(req, res, user)
				.then(() => userAuth.destroy()) // удаляем запись об авторизации, потому что она уже использована
				.catch(() => res.sendHttpError(DATABASE_WRITE_ERROR));
		},
	},
	{
		/**
		 * Разлогин
		 */
		getPath: '/api/logout',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => UserService.logout(req, res),
	},
	{
		/**
		 * Вход под другим пользователем
		 */
		postPath: '/api/sign-as-user',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			const logger = req.getLogger(moduleLogger);
			const currentUser = req.getAuthorizedUser();
			const { secondaryUserId, lastUrl } = req.body;
			if (!secondaryUserId || typeof secondaryUserId !== 'number') {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			logger.debug(`Sign as another userId=${secondaryUserId} and lastUrl='${lastUrl}'`);
			const secondaryUser = await userStore.findOne({
				where: { accountId: currentUser.accountId, id: secondaryUserId },
			});
			logger.debug(`Found secondary user=${secondaryUser && `${secondaryUser.id} ${secondaryUser.fullName}`}`);
			if (!secondaryUser) {
				return res.sendHttpError(USER_NOT_FOUND);
			}
			return UserService.signAsUser(req, res, secondaryUserId, lastUrl);
		},
	},
];
