import { ShopReportingService } from '~services';
import ResponseError, { NOT_ALL_REQUIRED_FIELDS, SERVER_ERROR } from '~errors/ResponseError';
import { ShopReportingType } from '~enums/ShopReportingType';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const moduleLogger = new AppLogger('shopReportingController');

export default [
	{
		/**
		 * Получение репорта по типу
		 */
		getPath: '/api/shop-reporting/:reportType',
		anyAuthorizedRole: true,
		doNotLogNormalStatus: true,
		handler: async (req: any, res: any) => {
			const { reportType } = req.params;
			if (!reportType) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const shopReportingService = new ShopReportingService(req);
			let promise;
			const currentUser = req.getAuthorizedUser();
			if (reportType === ShopReportingType.EMPLOYEE_COUNT) {
				promise = shopReportingService.findEmployeeCountReportForCurrentUser(currentUser);
			} else if (reportType === ShopReportingType.TEMPERATURE_CHECK) {
				promise = shopReportingService.findTemperatureCheckReportForCurrentUser(currentUser);
			} else if (reportType === ShopReportingType.PROBLEM_CHECK) {
				promise = shopReportingService.findProblemCheckReportForCurrentUser(currentUser);
			} else if (reportType === ShopReportingType.PERSONNEL_CHECK) {
				promise = shopReportingService.findPersonnelCheckReportForCurrentUser(currentUser);
			} else if (reportType === ShopReportingType.CAMERA_CHECK) {
				promise = shopReportingService.findCameraCheckReportForCurrentUser(currentUser);
			} else {
				return res.sendHttpError(ResponseError.validationInput(`Неизвестный тип отчета ${reportType}`));
			}
			const logger = req.getLogger(moduleLogger);
			promise
				.then(res.sendOk)
				.catch(err => {
					logger.error(fillError(Error, `[Ошибка при чтении отчета ${reportType}]`, err));
					res.sendHttpError(err instanceof ResponseError ? err : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Обновление репорта по типу
		 */
		postPath: '/api/shop-reporting/:reportType',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const { reportType } = req.params;
			if (!reportType || !req.body) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const shopReportingService = new ShopReportingService(req);
			let promise;
			const currentUser = req.getAuthorizedUser();
			if (reportType === ShopReportingType.EMPLOYEE_COUNT) {
				promise = shopReportingService.updateEmployeeCountReportForCurrentUser(currentUser, req.body);
			} else if (reportType === ShopReportingType.TEMPERATURE_CHECK) {
				promise = shopReportingService.updateTemperatureCheckReportForCurrentUser(currentUser, req.body);
			} else if (reportType === ShopReportingType.PROBLEM_CHECK) {
				promise = shopReportingService.updateProblemCheckReportForCurrentUser(currentUser, req.body);
			} else if (reportType === ShopReportingType.PERSONNEL_CHECK) {
				promise = shopReportingService.updatePersonnelCheckReportForCurrentUser(currentUser, req.body);
			} else if (reportType === ShopReportingType.CAMERA_CHECK) {
				promise = shopReportingService.updateCameraCheckReportForCurrentUser(currentUser, req.body);
			} else {
				return res.sendHttpError(ResponseError.validationInput(`Неизвестный тип отчета ${reportType}`));
			}
			const logger = req.getLogger(moduleLogger);
			promise
				.then(res.sendOk)
				.catch(err => {
					logger.error(fillError(Error, `[Ошибка при сохранении отчета ${reportType}]`, err));
					res.sendHttpError(err instanceof ResponseError ? err : SERVER_ERROR);
				});
		},
	},
];
