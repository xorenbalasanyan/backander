import moment from 'moment';
import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError, { NOT_ALL_REQUIRED_FIELDS, SERVER_ERROR } from '~errors/ResponseError';
import PageInfo from '~utils/PageInfo';
import { TodoItemService } from '~services';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const moduleLogger = new AppLogger('todoController');

export default [
	{
		/**
		 * Получение списка туду (всех, либо по номерам) за текущие сутки
		 */
		getPath: '/api/todos/list/:todoItemIds?',
		anyAuthorizedRole: true,
		doNotLogNormalStatus: true,
		handler: async (req: any, res: any) => {
			const { todoItemIds } = req.params;
			const currentUser = req.getAuthorizedUser();
			const options: any = {
				where: {
					addresseeId: currentUser.id,
					date: moment().format('YYYY-MM-DD'),
				},
			};
			const pageInfo = PageInfo.parseFromObject(req.query);
			const filterStatuses = pageInfo.customParams?.statuses;
			if (filterStatuses) {
				options.where.status = filterStatuses.split(',');
			}
			if (todoItemIds?.length) {
				options.where.id = todoItemIds.split(',');
			}
			return new TodoItemService(req).findAllWithPageInfo(options, pageInfo)
				.then(([items, pageInfo1]) => res.sendOk(items, pageInfo1))
				.catch(error => {
					req.getLogger(moduleLogger).error('[Ошибка при чтении списка TodoItem]', error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Получение туду по номеру
		 */
		getPath: '/api/todos/one/:todoItemId',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const { todoItemId } = req.params;
			if (!todoItemId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const currentUser = req.getAuthorizedUser();
			const options: any = {
				where: {
					id: todoItemId,
					addresseeId: currentUser.id,
				},
			};
			return new TodoItemService(req).findOne(options)
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error(`[Произошла ошибка при чтении TodoItem]`, error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Добавление списка туду
		 */
		postPath: '/api/todos/list',
		accessRoles: [UserRoles.ROBOT],
		handler: async (req: any, res: any) => {
			const { date } = req.query;
			const { todos } = req.body || {};
			if (!todos) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(`[Создается список TodoItem] ${JSON.stringify({ date, todos })}`);
			return new TodoItemService(req)
				.createItems(date, todos)
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Произошла ошибка при создании списка TodoItem]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Обновление туду
		 */
		putPath: '/api/todos/one',
		accessRoles: [UserRoles.ROBOT],
		handler: async (req: any, res: any) => {
			const { id, externalId } = req.query;
			if (!id && !externalId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const data = req.body;
			if (!data) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const todoItemService = new TodoItemService(req);
			const todoItems = id
				? [await todoItemService.findOneById(Number(id))]
				: await todoItemService.findAllByExternalId(externalId);
			if (!todoItems?.length) {
				return res.sendHttpError(ResponseError.notFound('Запись не найдена'));
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(`[Обновление TodoItem] ${JSON.stringify({ id, externalId, data })}`);
			return new TodoItemService(req)
				.updateTodos(todoItems, data)
				.then(() => res.sendOk())
				.catch(error => {
					logger.error(fillError(Error, `[Произошла ошибка при обновлении TodoItem]`, error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Обновление списка туду
		 */
		putPath: '/api/todos/list',
		accessRoles: [UserRoles.ROBOT],
		handler: async (req: any, res: any) => {
			const { date } = req.query;
			const { todos } = req.body || {};
			if (!todos || !(todos instanceof Array)) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			if (todos.length > 10_000) {
				return res.sendHttpError(ResponseError.validationInput('Количество записей в поел todos не должно превышать 10000'));
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(`[Обновляется список TodoItem] ${JSON.stringify({ date, todos })}`);
			return new TodoItemService(req).updateItemsByDate(date, todos)
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Произошла ошибка при создании списка TodoItem]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
];
