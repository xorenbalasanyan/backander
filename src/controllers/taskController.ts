import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError, { NOT_ALL_REQUIRED_FIELDS, SERVER_ERROR } from '~errors/ResponseError';
import { distinctListByField } from '~utils/list';
import PageInfo from '~utils/PageInfo';
import { TaskService, TaskToShopService, UserService } from '~services';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import { Shop, TaskToShop, User } from '../stores/db/entity';
import { select } from '~lib/sequelize';

const moduleLogger = new AppLogger('taskController');

export default [
	{
		/**
		 * Получение списка задач (всех, либо по номерам)
		 */
		getPath: '/api/tasks/list/:taskIds?',
		anyAuthorizedRole: true,
		doNotLogNormalStatus: true,
		handler: async (req: any, res: any) => {
			const { taskIds } = req.params;
			const pageInfo = PageInfo.parseFromObject(req.query);
			const { shopId } = pageInfo.customParams || {};
			const options: any = { where: {} };
			const filterStatuses = (pageInfo.customParams?.statuses || '').split(',').filter(s => !!s);
			if (taskIds?.length) {
				options.where.id = taskIds.split(',');
			}
			// набор данных зависит от роли пользователя
			const currentUser = req.getAuthorizedUser();
			const userService = new UserService(req);
			await chargeOptionsByShopIdAndUserRole(options, shopId, currentUser, userService, filterStatuses);
			// вытаскиваем только "свежие" задачи из view, у которых срок протухания был в течение 30 дней
			return new TaskService(req).findAllFreshTasks(options)
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error('[Ошибка при чтении списка Task]', error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Получение задачи по номеру
		 */
		getPath: '/api/tasks/one/:taskId',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const { taskId } = req.params;
			if (!taskId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const pageInfo = PageInfo.parseFromObject(req.query);
			const { shopId } = pageInfo.customParams || {};
			const options = { where: { id: taskId } };
			// набор данных зависит от роли пользователя
			const currentUser = req.getAuthorizedUser();
			const userService = new UserService(req);
			await chargeOptionsByShopIdAndUserRole(options, shopId, currentUser, userService);
			return new TaskService(req).findOne(options)
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error(`[Произошла ошибка при чтении Task]`, error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Создание списка задач
		 */
		postPath: '/api/tasks/list',
		// accessRoles: [USER_ROLE_DT, USER_ROLE_DF, USER_ROLE_TM, USER_ROLE_UPF], TODO xxx
		accessRoles: [UserRoles.CHIEF],
		handler: async (req: any, res: any) => {
			const { tasks } = req.body || {};
			if (!tasks) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(`[Создается список Task] ${JSON.stringify(tasks)}`);
			return new TaskService(req).createItems(tasks)
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Произошла ошибка при создании списка Task]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Создание задачи
		 * <pre>
		 * Request body scheme {
		 *   title: String(5, 200),
		 *   description: String(0, 2000) | null,
		 *   executionPeriodDays: Integer(min: 1, max: 15),
		 *   subordinateShopIds: Integer[]
		 * }
		 * </pre>
		 */
		postPath: '/api/tasks/one',
		// accessRoles: [USER_ROLE_DT, USER_ROLE_DF, USER_ROLE_TM, USER_ROLE_UPF], TODO xxx
		accessRoles: [UserRoles.CHIEF, UserRoles.MANAGER],
		handler: async (req: any, res: any) => {
			const taskData = req.body;
			if (!taskData) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(`[Создается Task] ${JSON.stringify(taskData)}`);
			return new TaskService(req)
				.createOne(taskData)
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Произошла ошибка при создании Task]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Сохранение решения по задаче
		 */
		putPath: '/api/tasks/execute/:taskToShopId',
		// accessRoles: [USER_ROLE_DM, USER_ROLE_ZDM], TODO xxx
		accessRoles: [UserRoles.MANAGER],
		handler: async (req: any, res: any) => {
			const { taskToShopId } = req.params;
			if (!taskToShopId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const data = req.body;
			if (!data) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(`[Сохраняется решение TaskToShop] ${JSON.stringify({
				taskToShopId,
				data,
			})}`);
			return new TaskToShopService(req).executeOne(taskToShopId, data)
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Произошла ошибка при сохранении решения TaskToShop]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Отклонение задачи
		 */
		putPath: '/api/tasks/reject/:taskToShopId',
		// accessRoles: [USER_ROLE_DT, USER_ROLE_DF, USER_ROLE_TM, USER_ROLE_UPF], TODO xxx
		accessRoles: [UserRoles.CHIEF, UserRoles.MANAGER],
		handler: async (req: any, res: any) => {
			const { taskToShopId } = req.params;
			if (!taskToShopId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const data = req.body;
			if (!data) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(`[Отклоняется решение TaskToShop] ${JSON.stringify({
				taskToShopId,
				data,
			})}`);
			return new TaskToShopService(req).rejectOne(taskToShopId, data)
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Произошла ошибка при отклонении TaskToShop]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
];

async function chargeOptionsByShopIdAndUserRole(options, shopId, user, userService, filterStatuses?: string[]) {
	const shopManagers = await select(`SELECT *
                                    FROM shop_manager sm
                                             INNER JOIN shop_manager_role smr ON smr.id = sm.roleId
                                    WHERE sm.userId = :userId`, {
		userId: user.id,
	});
	const isShopUser = !!shopManagers.find(s => s.isShopUser);
	if (user.isChief || (user.isManager && !isShopUser)) {
		// определяем список подчиненных магазинов
		let shopIds;
		if (shopId) {
			shopIds = [shopId];
		} else {
			shopIds = distinctListByField(await userService.getSubordinateShopList(user), 'id');
		}
		options.include = [
			{
				model: TaskToShop,
				as: 'fk_taskToShops',
				where: {
					shopId: shopIds,
				},
				include: [
					{ model: User, as: 'fk_executor', required: false },
					{ model: User, as: 'fk_rejector', required: false },
					{
						model: Shop,
						as: 'fk_shop',
						required: false,
						// TODO xxx
						/*
						include: [
							{ model: User, as: 'fk_df', required: false },
							{ model: User, as: 'fk_tm', required: false },
							{ model: User, as: 'fk_upf', required: false },
						],
						 */
					},
				],
			},
			{ model: User, as: 'fk_owner' },
		];
		if (filterStatuses?.length) {
			options.include[0].where.status = filterStatuses;
		}
	} else if (user.isManager && isShopUser) {
		options.include = [
			{
				model: TaskToShop,
				as: 'fk_taskToShops',
				where: { shopId: shopManagers.map(i => i.shopId) },
				include: [
					{ model: User, as: 'fk_executor', required: false },
					{ model: User, as: 'fk_rejector', required: false },
					{ model: Shop, as: 'fk_shop', required: false },
				],
			},
			{ model: User, as: 'fk_owner' },
		];
		if (filterStatuses?.length) {
			options.include[0].where.status = filterStatuses;
		}
	}
}
