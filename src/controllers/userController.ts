import util from 'util';
import { Op } from 'sequelize';
import { UserRoles } from '~enums/UserRoleEnum';
import PageInfo from '~utils/PageInfo';
import { AuthCredentialService, UserService } from '~services';
import ResponseError, {
	DATABASE_WRITE_ERROR,
	NOT_ALL_REQUIRED_FIELDS,
	SERVER_ERROR,
	UNHANDLED_EXCEPTION,
	UNIQUE_CONSTRAIN_TERROR,
	USER_HAS_NO_ACCESS,
	USER_NOT_FOUND,
} from '~errors/ResponseError';
import { UserRepository } from '~repositories';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import sequelize from '~lib/sequelize';
import { userStore } from '~stores';
import { AuthCredential } from '../stores/db/entity';

const moduleLogger = new AppLogger('accountController');

export default [
	{
		/**
		 * Получение пользователей по списку id
		 */
		getPath: '/api/users/:userIds?',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			const options: any = { where: {} };
			const userIds = req.params.userIds?.split(',');
			if (userIds) options.where.id = userIds;
			const userService = new UserService(req);
			const pageInfo = PageInfo.parseFromObject(req.query);
			return await userService.findAllWithPageInfo(options, pageInfo)
				.then(([users, pageInfo1]) => res.sendOk(users, pageInfo1))
				.catch(error => {
					const logger = req.getLogger(moduleLogger);
					logger.error(fillError(Error, '[Ошибка при чтении списка пользователей]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Получение пользователя по id
		 */
		getPath: '/api/user/:userId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			const { userId } = req.params;
			if (!userId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const pageInfo = PageInfo.parseFromObject(req.query);
			const user = await new UserService(req).findOne({
				where: {
					id: userId,
				},
				...(pageInfo.meta?.has('creds') && {
					include: [{
						model: AuthCredential,
						attributes: ['username'],
						required: false,
					}],
				}),
			});
			res.json(user);
		},
	},
	{
		/**
		 * Получение пользователя по externalId
		 */
		getPath: '/api/user/by-externalId/:externalId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			const { externalId } = req.params;
			if (!externalId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const user = await new UserService(req).findOne({ where: { externalId } });
			res.json(user?.toDto());
		},
	},
	{
		/**
		 * Получение пользователей по списку externalId
		 */
		getPath: '/api/users/by-externalIds/:externalIds',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			const externalIds = req.params.externalIds?.split(',');
			if (!externalIds?.length) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const users = await new UserService(req).findAll({ where: { externalId: { [Op.in]: externalIds } } });
			res.json(users?.map(u => u.toDto()));
		},
	},
	{
		/**
		 * Получение пользователей по роли
		 */
		getPath: '/api/users/by-role/:role',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			const { role } = req.params;
			if (!role) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const where = { role };
			// TODO: добавить проверку доступа для разных ролей
			//  например, ДМ не может получать инфу по любому из ДФ и даже админов
			const pageInfo = PageInfo.parseFromObject(req.query);
			const currentUser = req.getAuthorizedUser();
			if (!currentUser.isAdmin && !currentUser.isSupport) {
				return res.sendHttpError(USER_HAS_NO_ACCESS);
			}
			const [users, metaData] = await new UserService(req).findAllWithPageInfo({ where }, pageInfo);
			res.sendOk(users?.map(u => u.toDto()), metaData);
		},
	},
	{
		/**
		 * Создание пользователя
		 */
		postPath: '/api/user',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			// check
			const data = req.body;
			if (!data || !Object.keys(data).length) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(util.format('Create user with body %o', data));
			// check body
			const {
				externalId,
				role,
				username,
				key,
				firstName,
				lastName,
				middleName,
				email,
				phone,
				comment,
			} = data;
			if (!username || !firstName || !role) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			// проверка на уникальность логина
			const authCredentialService = new AuthCredentialService(req);
			const existAuthCred = await authCredentialService.findAllWithAccountId({
				where: {
					username,
				},
			});
			if (existAuthCred.length) {
				return res.sendHttpError(ResponseError.validationInput('Пользователь с таким логином уже существует'));
			}
			const currentUser = req.getAuthorizedUser();
			const newUserData: any = {
				accountId: currentUser.accountId,
				externalId,
				role,
				username,
				key,
				firstName,
				lastName,
				middleName,
				email,
				phone,
				comment,
				ownerId: currentUser.id,
			};
			// save
			await sequelize
				.transaction(async transaction => await userStore
					.createOne(newUserData, transaction)
					.then(async newUser => await authCredentialService
						.createOne({
							accountId: newUser.accountId,
							userId: newUser.id,
							username,
							key,
						}, transaction)))
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Ошибка создания пользователя]', error));
					if (error.constructor.name === 'UniqueConstraintError') {
						// ошибка уникальной записи
						res.sendHttpError(UNIQUE_CONSTRAIN_TERROR);
					} else {
						res.sendHttpError(UNHANDLED_EXCEPTION);
					}
				});
		},
	},
	{
		/**
		 * Обновление пользователя
		 */
		putPath: '/api/user/:userId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			// check
			const userId = +req.params.userId;
			if (!userId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const userService = new UserService(req);
			const existUser = await userService.findOne({ where: { id: userId } });
			if (!existUser) {
				return res.sendHttpError(USER_NOT_FOUND);
			}
			const data = req.body;
			if (!data) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(util.format('Update user with body %o', data));
			const {
				username, // в другой модели
				externalId,
				role,
				firstName,
				lastName,
				middleName,
				email,
				phone,
				skype,
				telegram,
				comment,
			} = data;
			// проверка на уникальность логина
			const authCredentialService = new AuthCredentialService(req);
			const existAuthCred = username && await authCredentialService.findOne({
				where: {
					username,
				},
			});
			if (existAuthCred && existAuthCred.userId !== userId) {
				return res.sendHttpError(ResponseError.validationInput('Пользователь с таким логином уже существует'));
			}
			// проверяем поля
			const newUserData = {
				externalId,
				role,
				firstName,
				lastName,
				middleName,
				email,
				phone,
				skype,
				telegram,
				comment,
			};
			Object.keys(newUserData).forEach(key => {
				if (newUserData[key] === undefined) {
					delete newUserData[key];
				}
			});
			// save
			const currentUser = req.getAuthorizedUser();
			return await sequelize
				.transaction(async transaction => {
					const promises: any[] = [];
					if (Object.keys(newUserData).length) {
						promises.push(userService.updateOne(existUser, {
							...newUserData,
							moderatorId: currentUser.id,
						}, transaction));
					}
					if (username) {
						if (existAuthCred && existAuthCred.username !== username) {
							promises.push(existAuthCred.update({
								username,
								moderatorId: currentUser.id,
							}, transaction));
						} else if (!existAuthCred) {
							promises.push(authCredentialService
								.createOne({
									accountId: existUser.accountId,
									userId: existUser.id,
									username,
									key: '',
									ownerId: currentUser.id,
								}, transaction));
						}
					}
					return await Promise.all(promises);
				})
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Ошибка обновления пользователя]', error));
					if (error.constructor.name === 'UniqueConstraintError') {
						// ошибка уникальной записи
						res.sendHttpError(UNIQUE_CONSTRAIN_TERROR);
					} else {
						res.sendHttpError(UNHANDLED_EXCEPTION);
					}
				});
		},
	},
	{
		/**
		 * Обновление пароля пользователя
		 */
		putPath: '/api/user/:userId/password',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			// check
			const { userId } = req.params;
			if (!userId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const { key } = req.body;
			if (!key) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const currentUser = req.getAuthorizedUser();
			const user: any = await UserRepository.findOneByAccountIdAndId(currentUser.accountId, userId);
			if (!user) {
				return res.sendHttpError(USER_NOT_FOUND);
			}
			const authCredentialService = new AuthCredentialService(req);
			const existAuthCred = await authCredentialService.findOne({
				where: {
					userId: user.id,
				},
			});
			if (!existAuthCred) {
				return res.sendHttpError(USER_NOT_FOUND);
			}
			const logger = req.getLogger(moduleLogger);
			try {
				logger.http(util.format('Update password for user #%d with body "%s"', userId, key));
				// update
				await existAuthCred.update({ key, moderator: currentUser });
				res.sendOk(user.toDto());
			} catch (error: any) {
				logger.error(fillError(Error, `Failed update password for user #${userId}:`, error));
				res.sendHttpError(DATABASE_WRITE_ERROR);
			}
		},
	},
	{
		/**
		 * Удаление пользователя
		 * Удалять из базы нельзя из-за привязок, поэтому дизейблим
		 */
		deletePath: '/api/user/:userId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			// check
			const { userId } = req.params;
			if (!userId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const currentUser = req.getAuthorizedUser();
			const user: any = await UserRepository.findOneByAccountIdAndId(currentUser.accountId, userId);
			if (!user) {
				return res.sendHttpError(USER_NOT_FOUND);
			}

			const logger = req.getLogger(moduleLogger);
			try {
				logger.http(util.format('Disable user #%d with body %o', userId));
				await user.update({
					username: `DELETED-${Date.now()}`,
					isDeleted: true,
					moderatorId: currentUser.id,
				});
				res.sendOk(user.toDto());
			} catch (error: any) {
				logger.error(fillError(Error, `Failed disable user #${userId}:`, error));
				res.sendHttpError(DATABASE_WRITE_ERROR);
			}
		},
	},
];
