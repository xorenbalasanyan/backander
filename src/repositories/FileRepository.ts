import BaseRepository from './BaseRepository';
import { File } from '../stores/db/entity';

export default class FileRepository extends BaseRepository
{
	constructor(req: any) {
		super(req, File);
	}

	protected addWhereSearch(where: object, str: string) {
		throw new Error('Не реализовано');
	}
}
