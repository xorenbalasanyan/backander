import { Op } from 'sequelize';
import { UserSetting } from '../stores/db/entity';

export default class UserSettingRepository
{
	static KEY_FCM_TOKEN = 'FCM_TOKEN';

	/**
	 * Поиск токена FCM по пользователю
	 */
	static async findOneFcmTokenByUserId(userId) {
		return UserSetting.findOne({
			where: {
				userId,
				key: UserSettingRepository.KEY_FCM_TOKEN,
			},
		});
	}

	/**
	 * Поиск токенов FCM по пользователям
	 */
	static async findAllFcmTokensByUserIds(userIds) {
		return UserSetting.findAll({
			where: {
				key: UserSettingRepository.KEY_FCM_TOKEN,
				userId: {
					[Op.in]: userIds,
				},
			},
		});
	}
}
