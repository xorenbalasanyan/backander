import { Transaction } from 'sequelize';
import BaseRepository from './BaseRepository';
import { File, Log, StructureImport } from '../stores/db/entity';

export default class StructureImportRepository extends BaseRepository
{
	constructor(req: any) {
		super(req, StructureImport);
	}

	protected addWhereSearch(where: object, str: string) {
		throw new Error('Не реализовано');
	}

	findLast100WithLogAndFile = async (transaction?: Transaction) => this
		.findAll({
			where: {},
			include: [Log, File],
			order: [['createdAt', 'DESC']],
			limit: 100,
		}, transaction)
}
