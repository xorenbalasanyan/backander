import { Op, Transaction } from 'sequelize';
import { AppLogger } from '~lib/AppLogger';
import BaseStore from '../stores/BaseStore';

export default abstract class BaseRepository
{
	private declare _logger: AppLogger;

	protected constructor(private readonly _req: any, private readonly _store: BaseStore) {}

	get req(): any {
		return this._req;
	}

	get store(): BaseStore {
		return this._store;
	}

	get logger(): any {
		if (!this._logger) {
			this._logger = this.req.getLogger(new AppLogger(this.constructor.name));
		}
		return this._logger;
	}

	protected abstract addWhereSearch(where: object, str: string);

	//
	// Базовые методы поиска
	//

	/**
	 * Поиск одной записи по заявленным условиям.
	 * @param options
	 * @param {object} transaction
	 * @returns {Promise<any>}
	 */
	findOne = async (options: any, transaction?: Transaction): Promise<any> => {
		if (!options?.where) {
			throw new Error(`Для вызова ${this.constructor.name}.findOne нужно указать условие where`);
		}
		const currentUser = this.req.getAuthorizedUser();
		options.where.accountId = currentUser.accountId;
		return this.store.findOne(options, transaction);
	}

	/**
	 * Поиск одной записи по заявленным условиям, без поиска по аккаунту.
	 * @param options
	 * @param transaction
	 * @returns {Promise<any>}
	 */
	findOneWithoutAccountId = async (options: any, transaction?: Transaction): Promise<any> => {
		if (!options?.where) {
			throw new Error(`Для вызова ${this.constructor.name}.findOneWithoutAccountId нужно указать условие where`);
		}
		return this.store.findOne(options, transaction);
	}

	/**
	 * Поиск нескольких записей.
	 * @param options
	 * @param transaction
	 * @returns {Promise<*>}
	 */
	findAll = async (options: any, transaction?: Transaction): Promise<any[]> => {
		if (!options?.where) {
			// искуственно ограничиваем поиск всех элементов
			// чтобы выполнить поиск всех записей необходимо передать `{ where: {} }`
			throw new Error(`Для вызова ${this.constructor.name}.findAll нужно указать условие where`);
		}
		const currentUser = this.req.getAuthorizedUser();
		options.where.accountId = currentUser.accountId;
		const findOptions: any = { where: options.where }; // init where
		if (options.search) {
			this.addWhereSearch(findOptions.where, options.search); // add search
		}
		if (options.include) {
			findOptions.include = options.include;
		}
		return this.store.findAll(findOptions, transaction);
	}

	//
	// Расширенные методы поиска, которые не используют store напрямую
	//

	/**
	 * Поиск по id.
	 * @param {number} id
	 * @param transaction
	 * @returns {Promise<any>}
	 */
	findOneById = async (id: number, transaction?: Transaction): Promise<any> => this
		.findOne({ where: { id } }, transaction)

	/**
	 * Поиск по id без добавления фильтра по аккаунту.
	 * @param {number} id
	 * @param transaction
	 * @returns {Promise<any>}
	 */
	findOneByIdWithoutAccountId = async (id: number, transaction?: Transaction): Promise<any> => this
		.findOneWithoutAccountId({ where: { id } }, transaction)

	/**
	 * Поиск по id.
	 * @param {number[]} idList
	 * @param transaction
	 * @returns {Promise<any>}
	 */
	findAllByIds = async (idList: number[], transaction?: Transaction): Promise<any[]> => this
		.findAll({ where: { id: { [Op.in]: idList } } }, transaction)

	//
	// Манипуляции с записью
	//

	/**
	 * Создание одной записи.
	 * @param item
	 * @param transaction
	 * @returns {Promise<any>}
	 */
	createOne = async (item: any, transaction?: Transaction): Promise<any> => this
		.store.createOne(item, transaction)

	/**
	 * Обновление одной записи.
	 * @param item
	 * @param newData
	 * @param transaction
	 * @returns {Promise<any>>}
	 */
	updateOne = async (item: any, newData: any, transaction?: Transaction): Promise<any> => this
		.store.updateOne(item, newData, transaction)

	/**
	 * Удаление ожной записи.
	 * @param where
	 * @param transaction
	 * @returns {Promise<any>}
	 */
	deleteWhere = async (where: any, transaction?: Transaction): Promise<void> => this
		.store.deleteWhere(where, transaction)
}
