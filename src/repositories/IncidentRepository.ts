import { Op } from 'sequelize';
import { toSqlDate } from '~utils/date';
import { File, Good, Incident, IncidentImport, Log, Shop } from '../stores/db/entity';

export default class IncidentRepository
{
	static findAllByDateAndShopIds = (date, shopIds) => Incident.findAll({
		where: {
			date,
			shopId: {
				[Op.in]: shopIds,
			},
		},
	});

	/**
	 * Поиск инцидентов с товарами и магазинами, но без defaultScope, чтобы все значения для isReported попали.
	 * @param date
	 * @param incidentTypeId
	 * @param shopIds
	 * @returns {*}
	 */
	static findAllWithShopAndGoodByDateAndIncidentTypeIdAndShopIds = (date, incidentTypeId, shopIds) => Incident
		.unscoped()
		.findAll({
			where: {
				date,
				incidentTypeId,
				shopId: {
					[Op.in]: shopIds,
				},
			},
			include: [
				{ model: Shop, as: 'fk_shop' },
				{ model: Good, as: 'fk_good' },
			],
		})

	static findAllIncidentTypeByUserAndIds = (user, incidentTypeIds) => user.getFk_account()
		.then(acc => acc.getIncidentTypes({
			where: {
				id: {
					[Op.in]: incidentTypeIds,
				},
			},
		}))

	static findAllReportedWithShopAndGoodByDateAndIncidentTypeId = (date, incidentTypeId) => Incident.findAll({
		where: {
			isReported: true,
			date,
			incidentTypeId,
		},
		include: [
			{ model: Shop, as: 'fk_shop' },
			{ model: Good, as: 'fk_good' },
		],
	})

	/**
	 * Выбор записей об импорте инцидентов с логом и файлом
	 * @param accountId
	 * @param month Месяц в формате YYYY-MM
	 */
	static findAllIncidentImportsWithLogAndFileByAccountIdAndMonth = (accountId, month) => {
		const firstDay = `${month}-01`;
		const d = new Date(firstDay);
		d.setMonth(d.getMonth() + 1);
		const nextMonthDay = toSqlDate(d);
		return IncidentImport.findAll({
			where: {
				accountId,
				date: {
					[Op.gte]: firstDay,
					[Op.lt]: nextMonthDay,
				},
			},
			include: [Log, File],
		});
	}

	/**
	 * Выбор последних 100 записей об импорте инцидентов с логом и файлом
	 */
	static find100IncidentImportsWithLogAndFileByAccountId = accountId => IncidentImport.findAll({
		where: {
			accountId,
		},
		include: [Log, File],
		order: [['createdAt', 'DESC']],
		limit: 100,
	})
}
