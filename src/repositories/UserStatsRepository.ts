import { UserAuth } from '../stores/db/entity';

export default class UserStatsRepository
{
	/**
	 * Поиск последних N авторизаций для пользователя
	 */
	static async find100AuthsByUser(user) {
		return UserAuth.findAll({
			where: {
				userId: user.id,
			},
			order: [['createdAt', 'DESC']],
			limit: 100,
		});
	}
}
