import { Op } from 'sequelize';
import { Shop } from '../stores/db/entity';

export default class ShopRepository
{
	static findOneByAccountIdAndId = (accountId, id) => Shop.findOne({
		where: {
			accountId,
			id,
		},
	})

	static findAllByAccountId = accountId => Shop.findAll({
		where: {
			accountId,
		},
	})

	static findAllByAccountIdAndExternalIds = (accountId, externalIds) => Shop.findAll({
		where: {
			accountId,
			externalId: {
				[Op.in]: externalIds,
			},
		},
	})

	static findAllByAccountIdAndDfId = (accountId, dfId) => Shop.findAll({
		where: {
			accountId,
			dfId,
		},
	})

	static findAllByAccountIdAndTmId = (accountId, tmId) => Shop.findAll({
		where: {
			accountId,
			tmId,
		},
	})

	static findAllByAccountIdAndUpfId = (accountId, upfId) => Shop.findAll({
		where: {
			accountId,
			upfId,
		},
	})
}
