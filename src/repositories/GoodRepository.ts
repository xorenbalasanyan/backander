import { Op, Transaction } from 'sequelize';
import BaseRepository from './BaseRepository';
import { Good } from '../stores/db/entity';

export default class GoodRepository extends BaseRepository
{
	constructor(req: any) {
		super(req, Good);
	}

	protected addWhereSearch(where: object, str: string) {
		throw new Error('Не реализовано');
	}

	/**
	 * Поиск многих по полю externalId.
	 * @param {string[] | number[]} externalIds
	 * @param transaction
	 * @returns {Promise<any>}
	 */
	findAllByExternalIds = async (externalIds: string[] | number[], transaction?: Transaction) => {
		return this.findAll({ where: { externalId: { [Op.in]: externalIds } } }, transaction);
	}
}
