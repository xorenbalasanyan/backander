import { Op } from 'sequelize';
import { User } from '../stores/db/entity';

export default class UserRepository
{
	static findOneByAccountIdAndId = (accountId, id) => User.findOne({
		where: {
			accountId,
			id,
		},
	})

	static findAllByAccountId = accountId => User.findAll({
		where: {
			accountId,
		},
	})

	/*
	static findAllByAccountIdAndIds = (accountId, ids) => User.findAll({
		where: {
			accountId,
			id: {
				[Op.in]: ids,
			},
		},
	})
	 */

	/*
	static findOneByAccountIdAndIdAndRole = (accountId, id, role) => User.findOne({
		where: {
			accountId,
			id,
			role,
		},
	})
	 */

	/*
	static findAllByAccountIdAndRole = (accountId, role) => User.findAll({
		where: {
			accountId,
			role,
		},
	})
	 */

	/*
	static findAllByAccountIdAndRoleAndIds = (accountId, role, ids) => User.findAll({
		where: {
			accountId,
			id: {
				[Op.in]: ids,
			},
			role,
		},
	})
	 */

	/*
	static findAllDmAndZdmUsersByAccountIdAndShopId = (accountId, shopId) => User.findAll({
		where: {
			accountId,
			role: {
				[Op.or]: [USER_ROLE_DM, USER_ROLE_ZDM],
			},
			shopId,
		},
	})
	 */

	/*
	static findAllDmAndZdmUsersByAccountIdAndShopIds = (accountId, shopIds) => User.findAll({
		where: {
			accountId,
			role: {
				[Op.or]: [USER_ROLE_DM, USER_ROLE_ZDM],
			},
			shopId: {
				[Op.in]: shopIds,
			},
		},
	})
	 */

	/*
	static findAllByAccountIdAndDfId = (accountId, dfId) => User.findAll({
		where: {
			accountId,
			dfId,
		},
	})
	 */

	/*
	static findAllByAccountIdAndDfIdAndIds = (accountId, dfId, ids) => User.findAll({
		where: {
			accountId,
			dfId,
			id: {
				[Op.in]: ids,
			},
		},
	})
	*/

	/*
	static findAllByAccountIdAndTmId = (accountId, tmId) => User.findAll({
		where: {
			accountId,
			tmId,
		},
	})
	 */

	/*
	static findAllByAccountIdAndTmIdAndIds = (accountId, tmId, ids) => User.findAll({
		where: {
			accountId,
			tmId,
			id: {
				[Op.in]: ids,
			},
		},
	})
	*/

	/*
	static findAllByAccountIdAndUpfId = (accountId, upfId) => User.findAll({
		where: {
			accountId,
			upfId,
		},
	})
	 */

	/*
	static findAllByAccountIdAndUpfIdAndIds = (accountId, upfId, ids) => User.findAll({
		where: {
			accountId,
			upfId,
			id: {
				[Op.in]: ids,
			},
		},
	})
	*/
}
