import morgan from 'morgan';

/**
 * Инициализация morgan логгера
 */
export default function morganLogger(rootLogger) {
	return morgan((tokens, req, res) => {
		// не будем логировать запрос при ответе без ошибки
		if (req.doNotLogNormalStatus && res.statusCode < 400) {
			return;
		}

		// фильтруем запросы загрузки файлов
		// const reqUrl = req.url;
		// if (reqUrl.startsWith('/api/file/download/') && res.statusCode < 400) {
		// 	return;
		// }

		const reqContentLength = tokens.req(req, res, 'content-length');
		const reqLen = reqContentLength ? `in:${reqContentLength}b` : '-';
		const resContentLength = tokens.res(req, res, 'content-length');
		const resLen = resContentLength ? `out:${resContentLength}b` : '-';

		const logger = req.getLogger(rootLogger);

		const log = [
			req.headers['x-forwarded-for'] || req.headers['x-real-ip'] || req.ip || req.connection.remoteAddress,
			`HTTP/${req.httpVersion}`,
			tokens.method(req, res),
			tokens.url(req, res),
			reqLen,
			tokens['user-agent'](req, res),
			`code:${tokens.status(req, res)}`,
			resLen,
			`time:${tokens['response-time'](req, res)}ms`,
		].join(' ');

		// выбираем уровень лога по статусу
		if (res.statusCode < 400) {
			logger.http(log);
			// логируем запрос для уровня debug
			if (logger.level === 'debug') {
				logger.debug(`Request headers: ${JSON.stringify(req.headers)}`);
				logger.debug(`Request body: ${JSON.stringify(req.body) || 'empty'}`);
				logger.debug(`Response body: ${res.responseBody || 'empty'}`);
			}
		} else {
			logger.warn(log
				+ `\nRequest headers: ${JSON.stringify(req.headers)}`
				+ `\nRequest body: ${JSON.stringify(req.body) || 'empty'}`
				+ `\nResponse body: ${res.responseBody || 'empty'}`);
		}
	});
}
