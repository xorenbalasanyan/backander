const MAX_BUF_LENGTH = 1e6; // максимальная длина ответа в буфере, 1 Мб

/**
 * Будем запоминать тело Response.
 */
export default function responseWriter(req, res, next) {
	// будем запоминать ответ
	const oldWrite = res.write;
	const oldEnd = res.end;
	const chunks: Buffer[] = [];
	let length = 0;

	res.write = function resWrite(chunk) {
		if (length < MAX_BUF_LENGTH) {
			const buf = Buffer.from(chunk);
			length += buf.length;
			chunks.push(buf);
		}
		oldWrite.apply(res, arguments);
	};

	res.end = function resEnd(chunk) {
		if (chunk) {
			if (length >= MAX_BUF_LENGTH) {
				chunks.push(Buffer.from('...'));
			}
			chunks.push(Buffer.from(chunk));
		}
		res.responseBody = Buffer.concat(chunks).toString('utf8');
		oldEnd.apply(res, arguments);
	};

	next();
}
