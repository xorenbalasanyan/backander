import fs from 'fs';
import tmp from 'tmp';
import util from 'util';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('readRequestBodyAsCsvFile');

/**
 * Автоматически сохраняем файл из запроса с Content-Type 'text/csv' во временный файл и запоминает в req ссылку на файл.
 * Как это работает:
 * - запускается скачивание тела файла через `req('on', cb)`
 * - этот метод сохраняет чанки в буфер
 * - параллельно запускается метод, который сохраняет чанки в стрим
 * - когда тело из запроса прочиталось, отмеачется флаг uploadFinished
 * - когда все чанки сохранились, вызывается next
 */
export default (req, res, next) => {
	if (req.headers['content-type'] !== 'text/csv') {
		return next();
	}
	const logger = req.getLogger(moduleLogger);
	const tmpFile = tmp.fileSync();
	req.csvTmpFileName = tmpFile.name; // запоминаем имя файла
	const debugInfo: any = {
		contentLength: req.headers['content-length'],
		tmpFileName: tmpFile.name,
		startTime: Date.now(),
		chunkCount: 0,
		maxChunkArraySize: 0,
	};
	let uploadFinished = false;
	const chunks: any[] = [];
	const writeStream = fs.createWriteStream(tmpFile.name, { encoding: 'ascii' });
	req.on('data', chunk => {
		chunks.push(chunk);
		debugInfo.chunkCount++;
		debugInfo.maxChunkArraySize = Math.max(debugInfo.maxChunkArraySize, chunks.length);
	});
	req.on('end', () => {
		uploadFinished = true;
		debugInfo.uploadTimeLength = Date.now() - debugInfo.startTime;
	});
	// запись чанков в файл будет работать параллельно, чтобы не завис импорт в запросе
	const writeChunk = () => {
		if (!chunks.length && uploadFinished) {
			debugInfo.saveTimeLength = Date.now() - debugInfo.startTime;
			logger.debug(util.format(`Uploaded new '%s' file, %o`, req.headers['content-type'], debugInfo));
			return next();
		}
		if (chunks.length) {
			writeStream.write(chunks.shift());
		}
		setTimeout(writeChunk);
	};
	writeChunk();
};
