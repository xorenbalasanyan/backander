import { Request, Response } from 'express';
import HttpError from '~errors/HttpError';
import { ResponseErrorEnum } from '~enums/ResponseErrorEnum';
import jsoner from '../lib/jsoner';
import PageInfo from '~utils/PageInfo';

/**
 * Расширяем объект Response
 * @param req
 * @param res
 * @param next
 */
export default function injectResponseMethods(req: Request, res: Response, next) {
	Object.assign(res, {
		/**
		 * Ответ на запрос при успешной оработке данных
		 * @param value
		 * @param pageInfo
		 */
		sendOk: (value?: any, pageInfo?: PageInfo) => {
			if (value !== null && value !== undefined && !(value instanceof Object) && !(value instanceof Array)) {
				throw new Error('В методе sendOk аргумент data должен быть объектом, массивом, null или undefined');
			}
			// вырезаем дочерние entity из data и закидываем их в meta чтобы сэкономить память, ведь они
			// могут повторяться
			const [json, metaJson] = jsoner(value);
			res
				.status(200)
				.json({
					ok: true,
					timestamp: Date.now(),
					data: json,
					meta: Object.keys(metaJson).length ? metaJson : undefined,
					...(pageInfo && Object.keys(pageInfo).length && { pageInfo }),
				})
				.end();
		},
		/**
		 * Ответ на запрос с ошибкой
		 * @param error
		 * @param jsonFields
		 */
		sendHttpError: (error: Error, jsonFields?: any) => {
			if (error instanceof HttpError) {
				res
					.status(error.getStatus())
					.json({
						ok: false,
						timestamp: Date.now(),
						...error.getJson(),
						...jsonFields,
					})
					.end();
			} else {
				res
					.status(500)
					.json({
						ok: false,
						timestamp: Date.now(),
						error: error.message,
						error_code: ResponseErrorEnum.ERROR_CODE_SERVER_ERROR,
						...jsonFields,
					})
					.end();
			}
		},
	});

	next();
}
