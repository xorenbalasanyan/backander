import fs from 'fs';
import util from 'util';
import { AddressObject, simpleParser } from 'mailparser';
import FirebaseAdmin, { messaging } from 'firebase-admin';
import { ONE_DAY_MS } from '~enums/DateEnum';
import config from '~lib/config';
import { isFirebaseInitialized } from '~lib/initFirebaseAdmin';
import sequelize from '~lib/sequelize';
import { UserSettingRepository } from '~repositories';
import {
	FileService,
	ImapService,
	InfoMessageService,
	LogService,
	RegionService,
	ShopService,
	UserService,
} from '~services';
import { fileTypeStore } from '~stores';
import BaseBackgroundTask, {
	BackgroundTaskExecuteOptions,
	BackgroundTaskReturnType,
} from './BaseBackgroundTask';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import Message = messaging.Message;

const logger = new AppLogger('InfoMessageReaderBackgroundTask');

/**
 * Задача читает входящие информационые сообщения и добавляет их в базу
 */
export default class InfoMessageReaderBackgroundTask extends BaseBackgroundTask
{
	protected override async start(options?: BackgroundTaskExecuteOptions): Promise<BackgroundTaskReturnType> {
		return new Promise((resolve, reject) => {
			const req = {
				getAuthorizedUser: () => ({
					accountId: config.get('inforeader:0:accountId'),
					id: config.get('inforeader:0:adminUserId'),
				}),
				getLogger: childLogger => logger.child(childLogger),
			};

			const infoMessageService = new InfoMessageService(req);
			const userService = new UserService(req);
			const regionService = new RegionService(req);
			const shopService = new ShopService(req);
			const logService = new LogService(req);
			const fileService = new FileService(req);

			const imapServiceConfig = config.get('inforeader:0');
			const imapService = new ImapService(imapServiceConfig.transport);

			let openedBox;
			let messagesToReadCount; // сколько сообщений должно быть прочитано
			let moveToTrashMessageCount = 0; // сколько сообщений отправлено в корзину

			function close(error: Error | undefined) {
				imapService.end();
				if (error) {
					reject(error);
				} else {
					resolve(undefined);
				}
			}

			function action() {
				imapService.searchMessages(['ALL'])
					.then((results: number[]) => {
						if (!results.length) {
							return close(undefined);
						}
						// читаем ограниченное количество сообщений за один запуск
						messagesToReadCount = Math.min(imapServiceConfig.readQuantityAtOneTime, results.length);
						logger.info(util.format('Будем загружать %d сообщений из ящика', messagesToReadCount));
						imapService.fetchMessages(
							Array(messagesToReadCount)
								.fill(0)
								.map((_, i) => i + 1));
					});
			}

			function parseEmailBuffer(seqno, buffer) {
				return simpleParser(buffer)
					.then(async parsed => {
						const attachmentsCount = parsed.attachments.filter(at => at.contentDisposition !== 'inline').length;
						const externalMessageDate = parsed.date?.toISOString();
						const subject = parsed.subject;
						const externalMessageId = parsed.messageId;
						// исключаем повторы - проверка на существующее письмо
						let caughtError: any = false;
						try {
							const existInfoMessage = await infoMessageService.findOne({ where: { externalMessageId } })
								.catch(error => caughtError = error);
							if (caughtError) {
								throw new Error(util.format('[Ошибка поиска инфо сообщения по номеру]: %o', externalMessageId, caughtError));
							}
							if (existInfoMessage) {
								logger.info(util.format('Почтовое сообщение с темой "%s" и номером "%s" уже было загружено', subject, externalMessageId));
								return;
							}
							const rawHtml = parsed.html || parsed.text || '';
							let html = reformatMessageHtml(rawHtml);
							logger.info(util.format('Результаты сжатия HTML верстки письма с темой "%s" и номером "%s": %s Kb -> %s Kb = %s%',
								subject,
								externalMessageId,
								(rawHtml.length / 1e3).toFixed(1),
								(html.length / 1e3).toFixed(1),
								(100 - html.length / rawHtml.length * 100).toFixed(2)));
							if (attachmentsCount) {
								html += '<p>Информационное сообщение содержит файлы.<br/>Чтобы ознакомиться с файлами, проверьте почту.</p>';
							}
							const isImportant = (parsed.headers.get('priority') as string)?.toLowerCase() === 'high';
							// вытаскиваем адресатов
							const to = (parsed.to as AddressObject)?.value.map(addr => addr.address) || [];
							const cc = (parsed.cc as AddressObject)?.value.map(addr => addr.address) || [];
							const emails: string[] = [...to, ...cc].filter(notUndefined).map(s => s.toLowerCase());
							// найдено пользователей по email
							const userAddressees: any = await userService
								.findAllByEmails(emails)
								.catch(error => caughtError = error);
							if (caughtError) {
								throw new Error(util.format('[Ошибка поиска пользователей по email]: emails: %o, ошибка: %o', emails, caughtError));
							}
							const regionAddressees = await regionService
								.findAllByEmails(emails) // найдено регионов по email
								.catch(error => caughtError = error);
							if (caughtError) {
								throw new Error(util.format('[Ошибка поиска регионов по email]: emails: %o, ошибка: %o', emails, caughtError));
							}
							const existsEmails: string[] = Array.from(new Set([
								...userAddressees.map(u => u.email),
								...regionAddressees.map(u => u.email),
							]));
							const notExistsEmails = emails.filter(s => !existsEmails.includes(s)); // сколько email не  было найдено
							// по регионам определяем адресатов
							const shops = await shopService
								.findAllByRegionIds(regionAddressees.map(r => r.id))
								.catch(error => caughtError = error);
							if (caughtError) {
								throw new Error(util.format('[Ошибка поиска магазинов по регионам]: %o', caughtError));
							}
							/* TODO xxx
							const dms = await userService
								.findAllDmAndZdmByShopIds(shops.map(s => s.id))
								.catch(error => caughtError = error);
							 */
							const dms: any = [];
							if (caughtError) {
								throw new Error(util.format('[Ошибка поиска ДМ и ЗДМ по магазинам]: %o', caughtError));
							}
							return sequelize.transaction(async transaction => {
								// сохраним html тело в файл
								const {
									fileName: name,
									pathName,
									fullPath,
								} = FileService.generateNewFileName('emailmsg-', '.html');
								// TODO удалить файл в случае ошибки
								fs.writeFileSync(fullPath, html);
								const size = fs.statSync(fullPath).size;
								const htmlFileType = await fileTypeStore
									.findIdByExt('html')
									.catch(error => caughtError = error);
								if (caughtError) {
									throw new Error(util.format('[Ошибка поиска типа файла]: %o', caughtError));
								}
								const savedFile = await fileService
									.createOne({
										name,
										size,
										fileTypeId: htmlFileType.id,
										pathName,
										ownerId: req.getAuthorizedUser().id,
										accountId: req.getAuthorizedUser().accountId,
									}, transaction)
									.catch(error => caughtError = error);
								if (caughtError) {
									throw new Error(util.format('[Ошибка создания файла]: %o', caughtError));
								}

								const newInfoMessage = {
									subject,
									fileId: savedFile.id,
									isImportant,
									externalMessageId,
									externalMessageDate,
									ownerId: 1000,
									linkAddressees: [].concat(userAddressees.map(u => u.id), dms.map((u: any) => u.id)),
								};

								const infoMessage = newInfoMessage.linkAddressees.length
									? await infoMessageService
										.createOne(newInfoMessage, transaction)
										.catch(error => caughtError = error)
									: null;
								if (caughtError) {
									throw new Error(util.format('[Ошибка создания инфо сообщения]: %o', caughtError));
								}
								const logMessage = `Информационное сообщение ${externalMessageId} '${subject}'\n`
									+ (infoMessage
										? `InfoMessage.id = ${infoMessage.id}\n`
											+ `Найдено пользователей: ${userAddressees.length}\n`
											+ `Найдено регионов: ${regionAddressees.length}, магазинов: ${shops.length}, ДМ и ЗДМ: ${dms.length}\n`
										: ''
									)
									+ `Не найдено email: ${notExistsEmails.length}\n`
									+ (infoMessage
										? `Список пользователей без регионов (${userAddressees.length}):\n`
											+ `${userAddressees.map((u, index) => ` ${index + 1}. (${u.role}) ${u.fullName}, ${u.email}\n`).join('')}`
											+ `Список регионов (${regionAddressees.length}):\n`
											+ `${regionAddressees.map((u, index) => ` ${index + 1}. ${u.name}, ${u.email}\n`).join('')}`
										: ''
									)
									+ (notExistsEmails.length
										? `Не нашлось адресатов (${notExistsEmails.length}):\n`
											+ `${notExistsEmails.map((s, index) => ` ${index + 1}. ${s}`).join('\n')}`
										: ''
									);
								await logService
									.createOne({
										accountId: req.getAuthorizedUser().accountId,
										userId: req.getAuthorizedUser().id,
										type: 'READ_INFO_MAIL',
										message: logMessage,
										ownerId: req.getAuthorizedUser().id,
									}, transaction)
									.catch(error => caughtError = error);
								if (caughtError) {
									throw new Error(util.format('[Ошибка создания лога]: %o', caughtError));
								}

								// уведомляем пользователей на android
								const userIds = Array.from(new Set([
									...userAddressees.map(u => u.id),
									...dms.map(u => u.id),
								]));
								if (userIds.length > 0 && isFirebaseInitialized()) {
									const userFcmSettings = await UserSettingRepository
										.findAllFcmTokensByUserIds(userIds)
										.catch(error => caughtError = error);
									if (caughtError) {
										throw new Error(util.format('[Ошибка поиск FCM для уведомления о письме]: %o', caughtError));
									}
									// collect
									const notifications: Message[] = userFcmSettings
										.filter((userFcm: any) => !!userFcm.value)
										.map((userFcm: any) => ({
											token: userFcm.value,
											notification: {
												title: `✉️ Пришло новое письмо`,
												body: `${isImportant ? '️🚩 ' : '️'}Тема: ${subject}`,
											},
											android: {
												ttl: ONE_DAY_MS, // завтра уже не нужно отправлять
											},
										}));
									logger.debug(util.format('notifications = %o', notifications));
									// send notification messages
									if (notifications && notifications.length) {
										// firebase может отправлять за раз не более 500 уведомлений
										let part = 1;
										while (notifications.length > 0) {
											const cuttedNotifications = notifications.splice(0, 500);
											await FirebaseAdmin.messaging()
												.sendAll(cuttedNotifications)
												.then((response) => {
													logger.info(util.format(
														'[Уведомление о новом письме] Результат отправки (партия %d) в Firebase: %o',
														part++,
														response));
												})
												.catch(error => {
													logger.error(fillError(Error,
														'[Ошибка отправки уведомлений в Firebase]',
														error));
												});
										}
									}
								}
							});
						} catch (error) {
							throw new Error(util.format(
								'При чтении сообщения с темой "%s" и номером "%s" произошла ошибка: %o',
								subject,
								externalMessageId,
								caughtError));
						}
					});
			}

			imapService
				.once('ready', () => imapService.openInbox().then(box => {
					openedBox = box;
					logger.info(util.format('В ящике сейчас %o сообщений', openedBox.messages));
					// при подключении к серверу сразу читаем почту
					action();
				}))
				.on('mail', newMsgCount => {
					logger.info(util.format('Ура! Приехало %d новых сообщений', newMsgCount));
					if (openedBox) { // проверяем наличие ящика, потому что событие может сработать до конца ready
						action();
					}
				})
				.on(ImapService.Event.FetchMessage, (message, seqno) => {
					imapService.readMessageBuffer(message)
						.then(({ buffer, attrs: { uid } }) => parseEmailBuffer(seqno, buffer)
							.then(() => imapService.moveToTrash(uid))
							.then(() => {
								// сколько отправлено в корзину
								moveToTrashMessageCount++;
								if (moveToTrashMessageCount === messagesToReadCount) {
									// все прочитанные сообщения отправлены в корзину
									// значит можно завершить работу с сообщениями
									close(undefined);
								}
							}))
						.catch(error => {
							logger.error(fillError(Error, '[Ошибка при выполнении imapService.readMessageBuffer]', error));
							close(error);
						});
				})
				.on(ImapService.Event.Error, error => {
					logger.error(fillError(Error, '[Ошибка работы IMAP]', error));
					close(error);
				})
				.once('end', () => logger.debug('IMAP закрылся'))
				.connect();
		});
	}
}

const reformatMessageHtml = html => {
	// удаляем переводы строк
	html = html.replace(/\n/g, '');
	// оставляем только текст внутри тега BODY
	const bodyM = html.match(/<body[^>]*>/);
	if (bodyM) {
		html = html.substr(bodyM.index + bodyM[0].length);
		const endM = html.match(/<\/body[^>]*>/);
		if (endM) {
			html = html.substr(0, endM.index);
		}
	}
	// удаляем информационное предупреждение, которое идет после подписи
	const policyM = html.match('(<p>)?<br>-----');
	if (policyM) {
		html = html.substr(0, policyM.index);
	}
	// удаляем ms теги и классы
	html = html.replace(/(<\/?o:p>|class="MsoNormalTable"|class="MsoNormal"|class="WordSection1"|class="MsoTableGrid"|class="MsoListParagraphCxSpFirst"|class="MsoListParagraphCxSpLast"|class="MsoListParagraphCxSpMiddle"|class="MsoBodyText")/g, '');
	// удаляем стили аля "mso-..." из атрибута style
	html = html.replace(/style="[^"]*mso-[^;"]+?[;"]/g, found => found
		.replace(/mso-[^;]+;/g, '').replace(/mso-[^"]+"/, '"'));
	// удаляем пустые теги span, p, div, b, i
	let i = 0;
	while (/<(span|div|p|b|i|u)(?:\s[^>]*>|>)<\/\1[^>]*>/g.test(html) && i++ < 100) {
		html = html.replace(/<(span|div|p|b|i|u)(?:\s[^>]*>|>)<\/\1[^>]*>/g, '');
	}
	// частный реформат: удаление стиля шрифта с засечками
	html = html.replace(/font-family:&quot;Times New Roman&quot;,serif;?/g, '');
	// частный реформат: увеличение мелкого шрифта
	html = html.replace(/font-size:8(\.0)?pt/g, 'font-size: 9pt');
	// очистка пустых аргументов style
	html = html.replace(/style=""/g, '');
	// удаляем пробелы после открывающих тегов и перед закрывающими
	html = html.replace(/<\s+/g, '<').replace(/\s+>/g, '>');
	// чистим хвост
	html = html.replace(/(<p>(<span[^>]*>)?&nbsp;(<\/span>)?<\/p>)+<\/div>$/, '</div>');
	return html;
};

const notUndefined = <T>(x: T | undefined): x is T => x !== undefined;
