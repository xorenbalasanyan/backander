import moment from 'moment';
import util from 'util';
import { IncidentStateEnum } from '~enums/IncidentStateEnum';
import { arrayToMapById } from '~utils/list';
import { validateInteger } from '~utils/validate';
import sequelize, { insert, select, update } from '~lib/sequelize';
import BaseBackgroundTask, { BackgroundTaskReturnType } from './BaseBackgroundTask';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const logger = new AppLogger('IncidentStatsCuberBackgroundTask');

const CUBE_NAME1 = 'cube_incident_user_stats';
const CUBE_NAME2 = 'cube_incident_shop_stats';

/**
 * Задача чистит и заполняет куб статистики по инцидентам для менеджеров.
 */
export default class IncidentStatsCuberBackgroundTask extends BaseBackgroundTask
{
	private started = Date.now();

	protected override start(accountId: number): Promise<BackgroundTaskReturnType> {
		const date = moment().format('YYYY-MM-DD'); // TODO опредлелять сутки сети по аккаунту
		// обновляем куб для одного аккаунта
		const check = validateInteger(1, Number.MAX_SAFE_INTEGER)(accountId);
		if (check) {
			throw new Error(util.format(
				'При запуске задачи "IncidentStatsCuberBackgroundTask" передано значение accountId=%o, ошибка валидации: %s',
				accountId,
				check));
		}
		return generateCubesForAccountId(accountId, date)
			.then(async (queries: Queries) => {
				// определяем, нужно ли чистить кубы
				// для этого делаем выборку по дате
				const counts1 = await select(`SELECT date, count(*)
                                              FROM cube_incident_user_stats
                                              GROUP BY date`);
				const hasToday = counts1.find(i => i.date === date);
				// если статов за сегодня нет, но есть за другие дни, чистим кубы для аккаунта
				if (!hasToday && counts1.length) {
					return await clearCubeForAccount(accountId)
						.then(() => queries);
				} else {
					return queries;
				}
			})
			.then(async (queries: Queries) => {
				const { inserts, updates } = queries;
				for (const query of inserts) {
					await insert(query);
				}
				for (const query of updates) {
					await update(query);
				}
				const length = (Date.now() - this.started) / 1000;
				logger.info(`Задача работала ${length.toFixed(2)} секунд`);
			});
	}

	// перепишу метод чтобы ограничить входные данные
	override execute(accountId: number): Promise<BackgroundTaskReturnType> {
		return super.execute(accountId);
	}
}

/**
 * Чистка кубов для одного аккаунта
 * @param accountId
 */
function clearCubeForAccount(accountId: number): Promise<any> {
	logger.debug(util.format('Выполняется чистка кубов "%s" и "%s" для аккаунта #%d', CUBE_NAME1, CUBE_NAME2, accountId));
	return sequelize.query(`DELETE
                            FROM ${CUBE_NAME1}
                            WHERE accountId = ${accountId}`)
		.then(() => sequelize.query(`DELETE
                                     FROM ${CUBE_NAME2}
                                     WHERE accountId = ${accountId}`));
}

type Stat = {
	shopId: number,
	state: string,
	incidentTypeId: number,
	kount: number,
};

type LevelStat = {
	ALL: number,
	[IncidentStateEnum.TODO]?: number,
	[IncidentStateEnum.DONE]?: number,
};

type ShopStat = {
	managerId?: number,
	shopId: number,
	incidentTypeStats: Map<number, LevelStat>,
};

type ManagerStat = {
	managerId?: number,
	userId?: number,
	incidentTypeStats: Map<number, LevelStat>,
	users: ManagerStat[],
	shops: ShopStat[],
};

type Queries = {
	inserts: string[],
	updates: string[],
};

type ShopStats = Map<number, Map<number, LevelStat>>;

type Context = {
	shopManagers: any[],
	shopStats: ShopStats,
	roles: any[],
	roleLevels: number[],
};

// заполняет куб для одного аккаунта
async function generateCubesForAccountId(accountId: number, date: string): Promise<Queries> {
	logger.debug(util.format('Начато заполнение кубов "%s" и "%s" для аккаунта #%d', CUBE_NAME1, CUBE_NAME2, accountId));
	// получаем список ролей для аккаунта
	const shopManagerRoles: any[] = await select(`SELECT *
                                                  FROM shop_manager_role
                                                  WHERE accountId = ${accountId}`)
		.catch(error => {
			throw fillError(Error, '[Ошибка при получении списка ролей]', error);
		});
	const roleMap = arrayToMapById(shopManagerRoles);
	// загружаем связки менеджеров и магазинов
	const shopManagers: any[] = await select(`SELECT shopId, roleId, userId
                                              FROM shop_manager
                                              WHERE roleId IN (${Array.from(roleMap.keys()).join(',')})`)
		.catch(error => {
			throw fillError(Error, '[Ошибка при получении списка менеджеров]', error);
		});
	shopManagers.forEach(i => i.role = roleMap.get(i.roleId));
	const shopIds = Array.from(new Set(shopManagers.map(i => i.shopId)));
	// загружаем статы по инцидентам для магазинов аккаунта
	const incidentStats: Stat[] = await select(`SELECT shopId,
                                                       incidentTypeId,
                                                       state,
                                                       COUNT(*) AS kount
                                                FROM incident
                                                WHERE date = '${date}'
                                                  AND shopId IN (${shopIds.join(',')})
                                                GROUP BY shopId, incidentTypeId, state`)
		.catch(error => {
			throw fillError(Error, '[Ошибка при получении списка инцидентов]', error);
		});
	// также загружаем текущие кубы
	const existUserStats = await select(`SELECT id,
                                                managerId,
                                                userId,
                                                incidentTypeId,
                                                state,
                                                completed,
                                                allCount
                                         FROM ${CUBE_NAME1}
                                         WHERE date = '${date}'
                                           AND accountId = ${accountId}`)
		.catch(error => {
			throw fillError(Error, '[Ошибка при получении куба пользователей]', error);
		});
	const existShopStats = await select(`SELECT id,
                                                managerId,
                                                shopId,
                                                incidentTypeId,
                                                state,
                                                completed,
                                                allCount
                                         FROM ${CUBE_NAME2}
                                         WHERE date = '${date}'
                                           AND accountId = ${accountId}`)
		.catch(error => {
			throw fillError(Error, '[Ошибка при получении куба магазинов]', error);
		});
	// формируем карту статистики по статусам для каждого магазина
	const shopStats: ShopStats = new Map<number, Map<number, LevelStat>>();
	for (const stat of incidentStats) {
		if (!shopStats.has(stat.shopId)) {
			const map = new Map<number, LevelStat>();
			map.set(0, { ALL: 0 }); // статы по всем типам инцидентов
			shopStats.set(stat.shopId, map);
		}
		const shopStat = shopStats.get(stat.shopId)!;
		if (!shopStat.has(stat.incidentTypeId)) {
			shopStat.set(stat.incidentTypeId, { ALL: 0 });
		}
		const allIncidentTypeStat = shopStat.get(0)!;
		allIncidentTypeStat[stat.state] = (allIncidentTypeStat[stat.state] || 0) + stat.kount;
		allIncidentTypeStat.ALL += stat.kount;
		const shopIncidentTypeStat = shopStat.get(stat.incidentTypeId)!;
		shopIncidentTypeStat[stat.state] = stat.kount;
		shopIncidentTypeStat.ALL += stat.kount;
	}
	// формируем дерево статистики по менедежерам, начиная с уровня DF
	const context: Context = {
		shopManagers,
		shopStats,
		roles: shopManagerRoles,
		roleLevels: Array.from(new Set(shopManagerRoles.map(i => i.subLevel))),
	};
	const statsTree = fillManagerLevel(context, Number.MAX_SAFE_INTEGER);
	// для каждого уровня и пользователя записываем статы в куб
	const cubeStats: CubeStats = {
		shopInserts: [],
		shopUpdates: [],
		userInserts: [],
		userUpdates: [],
	};
	fillCubeData(cubeStats, existUserStats, existShopStats, accountId, date, statsTree);
	// формируем и выполняем запросы
	const inserts: string[] = [];
	const updates: string[] = [];
	if (cubeStats.userInserts.length) {
		const size = 100_000;
		const chunks = Math.floor(cubeStats.userInserts.length / size) + 1;
		for (let i = 0; i < chunks; i++) {
			const chunk = cubeStats.userInserts.slice(i * size, (i + 1) * size);
			if (chunk.length) {
				inserts.push(`INSERT INTO ${CUBE_NAME1} (accountId, managerId, userId, date,
                                                         incidentTypeId, state, completed,
                                                         allCount)
                              VALUES (${chunk.join('),(')})`);
			}
		}
	}
	cubeStats.userUpdates.forEach(setQuery => {
		updates.push(`UPDATE ${CUBE_NAME1}
                      SET ${setQuery}`);
	});
	if (cubeStats.shopInserts.length) {
		const size = 100_000;
		const chunks = Math.floor(cubeStats.shopInserts.length / size) + 1;
		for (let i = 0; i < chunks; i++) {
			const chunk = cubeStats.shopInserts.slice(i * size, (i + 1) * size);
			if (chunk.length) {
				inserts.push(`INSERT INTO ${CUBE_NAME2} (accountId, managerId, shopId, date,
                                                         incidentTypeId, state, completed,
                                                         allCount)
                              VALUES (${chunk.join('),(')})`);
			}
		}
	}
	cubeStats.shopUpdates.forEach(setQuery => {
		updates.push(`UPDATE ${CUBE_NAME2}
                      SET ${setQuery}`);
	});
	return {
		inserts: inserts.filter(Boolean),
		updates: updates.filter(Boolean),
	};
}

// метод заполняет статистикой дерево менеджеров
function fillManagerLevel(context: Context, upperRoleLevel: number, managerId?: number): ManagerStat {
	const { shopManagers, shopStats, roleLevels } = context;
	const incidentTypeStats = new Map<number, LevelStat>();
	incidentTypeStats.set(0, { ALL: 0 });
	const levelStats: ManagerStat = { incidentTypeStats, users: [], shops: [] };
	const shopIdsByManager = managerId
		? shopManagers.filter((sm: any) => sm.role.subLevel === upperRoleLevel && sm.userId === managerId)
			.map(i => i.shopId)
		: [];
	const topRoleLevel = roleLevels.filter(i => i < upperRoleLevel).sort((a, b) => a - b).pop() || 0; // выбираем старшую роль
	const levelShopManagers = managerId
		? shopManagers.filter((sm: any) => sm.role.subLevel === topRoleLevel && !sm.role.isShopUser && shopIdsByManager.includes(sm.shopId))
		: shopManagers.filter((sm: any) => sm.role.subLevel === topRoleLevel && !sm.role.isShopUser);

	// строим дерево подчиненных сотрудников и собираем с них статистику
	if (levelShopManagers?.length) {
		const userIds = Array.from(new Set(levelShopManagers.map(i => i.userId)));
		userIds.forEach(levelManagerId => {
			// сначала заполняем статы для подчиненных уровней
			const userStat = fillManagerLevel(context, topRoleLevel, levelManagerId);
			userStat.userId = levelManagerId;
			// магазины под управлением подчиненных менеджеров
			const subLsmShopIds = Array.from(new Set(shopManagers
				.filter((sm: any) => sm.role.subLevel < topRoleLevel && !sm.role.isShopUser)
				.map(i => i.shopId)));
			// собираем статистику с подчиненных магазинов
			const lsmByShop = shopManagers.filter((sm: any) => sm.userId === levelManagerId && !subLsmShopIds.includes(sm.shopId));
			lsmByShop?.forEach((sm: any) => {
				const shopStat = shopStats.get(sm.shopId);
				if (shopStat) {
					// инкрементим счетчики статистики
					for (const [incidentTypeId, shopIncidentTypeStat] of shopStat) {
						for (const statKey in shopIncidentTypeStat) {
							// check userStat
							if (!userStat.incidentTypeStats.has(incidentTypeId)) {
								userStat.incidentTypeStats.set(incidentTypeId, { ALL: 0 });
							}
							const userIncidentTypeStat = userStat.incidentTypeStats.get(incidentTypeId)!;
							userIncidentTypeStat[statKey] = (userIncidentTypeStat[statKey] || 0) + shopIncidentTypeStat[statKey];
						}
					}
					userStat.shops.push({
						managerId: levelManagerId,
						shopId: sm.shopId,
						incidentTypeStats: shopStat,
					});
				}
			});
			if (userStat.incidentTypeStats.get(0)!.ALL) {
				// добавляем в статистику только если есть что добавить
				// инкрементим счетчики статистики
				for (const [incidentTypeId, userIncidentTypeStat] of userStat.incidentTypeStats) {
					for (const statKey in userIncidentTypeStat) {
						// check levelStats
						if (!levelStats.incidentTypeStats.has(incidentTypeId)) {
							levelStats.incidentTypeStats.set(incidentTypeId, { ALL: 0 });
						}
						const levelIncidentTypeStat = levelStats.incidentTypeStats.get(incidentTypeId)!;
						levelIncidentTypeStat[statKey] = (levelIncidentTypeStat[statKey] || 0) + userIncidentTypeStat[statKey];
					}
				}
				userStat.managerId = managerId;
				levelStats.users.push(userStat);
			}
		});
	}
	return levelStats;
}

type CubeStats = {
	userInserts: string[],
	userUpdates: string[],
	shopInserts: string[],
	shopUpdates: string[],
};

// заполняет данные для куба
function fillCubeData(cubeStats: CubeStats, cubeUserStats: any[], cubeShopStats: any[], accountId: number, date: string, stat: ManagerStat): void {
	if (stat.userId) {
		// заполняем статы по подчиненным пользователям
		const managerId = stat.managerId || null;
		const first = `${accountId},${managerId},${stat.userId},'${date}'`;
		for (const [incidentTypeIdV, incidentTypeStat] of stat.incidentTypeStats) {
			const incidentTypeId = incidentTypeIdV || null;
			if (incidentTypeStat.TODO) {
				const exStat = cubeUserStats.find(i => i.managerId === managerId
					&& i.userId === stat.userId
					&& i.incidentTypeId === incidentTypeId
					&& i.state === 'TODO');
				if (exStat) {
					if (exStat.completed !== 0 || exStat.allCount !== incidentTypeStat.TODO) {
						cubeStats.userUpdates.push(`completed=0, allCount=${incidentTypeStat.TODO} WHERE id=${exStat.id}`);
					}
				} else {
					cubeStats.userInserts.push(`${first},${incidentTypeId},'TODO',0,${incidentTypeStat.TODO}`);
				}
			}
			if (incidentTypeStat.DONE) {
				const exStat = cubeUserStats.find(i => i.managerId === managerId
					&& i.userId === stat.userId
					&& i.incidentTypeId === incidentTypeId
					&& i.state === 'DONE');
				if (exStat) {
					if (exStat.completed !== incidentTypeStat.DONE || exStat.allCount !== incidentTypeStat.DONE) {
						cubeStats.userUpdates.push(`completed=${incidentTypeStat.DONE}, allCount=${incidentTypeStat.DONE} WHERE id=${exStat.id}`);
					}
				} else {
					cubeStats.userInserts.push(`${first},${incidentTypeId},'DONE',${incidentTypeStat.DONE},${incidentTypeStat.DONE}`);
				}
			}
			const completed = incidentTypeStat.DONE || 0;
			const exStat1 = cubeUserStats.find(i => i.managerId === managerId
				&& i.userId === stat.userId
				&& i.incidentTypeId === incidentTypeId
				&& i.state === null);
			if (exStat1) {
				if (exStat1.completed !== completed || exStat1.allCount !== incidentTypeStat.ALL) {
					cubeStats.userUpdates.push(`completed=${completed}, allCount=${incidentTypeStat.ALL} WHERE id=${exStat1.id}`);
				}
			} else {
				cubeStats.userInserts.push(`${first},${incidentTypeId},NULL,${completed},${incidentTypeStat.ALL}`);
			}
		}
	}
	stat.shops.forEach(shop => {
		const managerId = shop.managerId || null;
		// заполняем статы по подчиненным магазинам
		const first = `${accountId},${managerId},${shop.shopId},'${date}'`;
		for (const [incidentTypeIdV, incidentTypeStat] of shop.incidentTypeStats) {
			const incidentTypeId = incidentTypeIdV || null;
			if (incidentTypeStat.TODO) {
				const exStat = cubeShopStats.find(i => i.managerId === managerId
					&& i.shopId === shop.shopId
					&& i.incidentTypeId === incidentTypeId
					&& i.state === 'TODO');
				if (exStat) {
					if (exStat.completed !== 0 || exStat.allCount !== incidentTypeStat.TODO) {
						cubeStats.shopUpdates.push(`completed=0, allCount=${incidentTypeStat.TODO} WHERE id=${exStat.id}`);
					}
				} else {
					cubeStats.shopInserts.push(`${first},${incidentTypeId},'TODO',0,${incidentTypeStat.TODO}`);
				}
			}
			if (incidentTypeStat.DONE) {
				const exStat = cubeShopStats.find(i => i.managerId === managerId
					&& i.shopId === shop.shopId
					&& i.incidentTypeId === incidentTypeId
					&& i.state === 'DONE');
				if (exStat) {
					if (exStat.completed !== incidentTypeStat.DONE || exStat.allCount !== incidentTypeStat.DONE) {
						cubeStats.shopUpdates.push(`completed=${incidentTypeStat.DONE}, allCount=${incidentTypeStat.DONE} WHERE id=${exStat.id}`);
					}
				} else {
					cubeStats.shopInserts.push(`${first},${incidentTypeId},'DONE',${incidentTypeStat.DONE},${incidentTypeStat.DONE}`);
				}
			}
			const completed = incidentTypeStat.DONE || 0;
			const exStat1 = cubeShopStats.find(i => i.managerId === managerId
				&& i.shopId === shop.shopId
				&& i.incidentTypeId === incidentTypeId
				&& i.state === null);
			if (exStat1) {
				if (exStat1.completed !== completed || exStat1.allCount !== incidentTypeStat.ALL) {
					cubeStats.shopUpdates.push(`completed=${completed}, allCount=${incidentTypeStat.ALL} WHERE id=${exStat1.id}`);
				}
			} else {
				cubeStats.shopInserts.push(`${first},${incidentTypeId},NULL,${incidentTypeStat.DONE || 0},${incidentTypeStat.ALL}`);
			}
		}
	});
	stat.users.forEach(u => fillCubeData(cubeStats, cubeUserStats, cubeShopStats, accountId, date, u));
}
