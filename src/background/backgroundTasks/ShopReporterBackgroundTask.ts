import fs from 'fs';
import moment from 'moment';
import path from 'path';
import ExcelJS, { Alignment } from 'exceljs';
import { AccountSettingKeyEnum } from '~enums/AccountSettingKeyEnum';
import { ShopReportingType } from '~enums/ShopReportingType';
import { UserRoles } from '~enums/UserRoleEnum';
import {
	arrayToMapByField,
	arrayToMapById,
	distinctListByField,
	mappingArrayByFields,
} from '~utils/list';
import config from '~lib/config';
import sequelize from '~lib/sequelize';
import {
	accountSettingStore,
	accountStore,
	fileStore,
	fileTypeStore,
	regionStore,
	shopReportingStore,
	shopStore,
	userStore,
} from '~stores';
import BaseBackgroundTask, {
	BackgroundTaskExecuteOptions,
	BackgroundTaskReturnType,
} from './BaseBackgroundTask';
import { FileService, MailService } from '~services';
import { MailMessage } from '../../stores/db/entity';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const logger = new AppLogger('ShopReporterBackgroundTask');

/**
 * Логика формирования отчетов по магазинам.
 * На сегодня формируется только один отчет по типу EMPLOYEE_COUNT.
 * Этот отчет нужно формировать после 10:00 и после 12:00 - два отчета за сутки.
 */
export default class ShopReporterBackgroundTask extends BaseBackgroundTask
{
	protected override async start(options?: BackgroundTaskExecuteOptions): Promise<BackgroundTaskReturnType> {
		const accounts: any[] = await accountStore
			.findAll({
				where: {},
				attributes: ['id'],
				raw: true,
			})
			.catch(error => {
				throw fillError(Error, '[Ошибка поиска аккаунтов]', error);
			});
		const accountIds: number[] = accounts.map(i => i.id);
		for (const accountId of accountIds) {
			// проверяем настройки
			const lastSentSetting: any | null = await accountSettingStore
				.findOne({
					where: {
						accountId,
						key: AccountSettingKeyEnum.SHOP_REPORTER_LAST_SENT,
					},
				})
				.catch(error => {
					throw fillError(Error, `[Ошибка поиска настроек аккаунтов]: ${JSON.stringify({ accountId })}`, error);
				});
			const lastSentTime: number | null = lastSentSetting ? Number(lastSentSetting.value) : null;
			// проверяем текущее время и дату последнего отчета
			// отправлять можно только дважды в сутки после 10:00 и после 12:00 по Уралу
			if (lastSentTime) {
				const now = moment().utcOffset(300);
				const nowHour = now.hour();
				if (nowHour < 10) {
					// время для первого отчета еще не наступило
					continue;
				}
				const last = moment(lastSentTime || Date.now()).utcOffset(300);
				if (now.format('YYYY.MM.DD') === last.format('YYYY.MM.DD')) {
					const lastHour = last.hour();
					if (nowHour < 12) {
						// время для второго отчета еще не наступило
						continue;
					} else if (lastHour >= 12) {
						// второй отчет уже был отправлен, сегодня отчетов больше не нужно
						continue;
					}
				}
			}
			// выполняем отчет на текущее время
			await createReportForAccountId(accountId, lastSentSetting)
				.catch(error => {
					throw fillError(Error, `[Ошибка создания отчета по магазинам]: ${JSON.stringify({ accountId, lastSentSetting })}`, error);
				});
		} // accounts
	}
}

async function createReportForAccountId(accountId: number, lastSentSetting: any | null) {
	let caughtError: any = false;
	// файл и отчет создает один из админов аккаунта
	const currentUser = await userStore
		.findOne({ where: { accountId, role: UserRoles.ADMIN } })
		.catch(error => caughtError = error);
	if (caughtError) throw caughtError;
	// кому будем отправлять очтет
	const addresseesSetting = await accountSettingStore
		.findOneEmployeeCountReportAddresseesByAccountId(accountId)
		.catch(error => caughtError = error);
	if (caughtError) throw caughtError;
	const toEmails = addresseesSetting?.value.split(',').map(s => s.trim()).filter(s => !!s);
	if (!toEmails?.length) {
		// не кому отправлять отчет, значит нет смысла его формировать
		return false;
	}
	// собираем отчеты
	const today = moment().format('YYYY-MM-DD');
	const todayHuman = moment().format('DD.MM.YYYY');
	const shopReports = await shopReportingStore
		.findAll({
			where: { accountId, date: today, reportType: ShopReportingType.EMPLOYEE_COUNT },
			attributes: ['shopId', 'value'],
			raw: true,
		})
		.catch(error => caughtError = error);
	if (caughtError) throw caughtError;
	const reportsMap = mappingArrayByFields(shopReports, 'shopId', 'value');
	// список всех магазинов
	const shops = await shopStore.findAll({ where: {} })
		.catch(error => caughtError = error);
	if (caughtError) throw caughtError;
	const shopMap = arrayToMapById(shops);
	const shopIds: number[] = Array.from(shopMap.keys());
	// регионы магазинов
	const regionIds: number[] = distinctListByField(shops, 'regionId');
	const regions = await regionStore.findAllByIds(regionIds)
		.catch(error => caughtError = error);
	if (caughtError) throw caughtError;
	const regionMap = arrayToMapById(regions);
	// список пользователей, причастных к магазинам
	const dmUsers = await userStore.findAllByShopIds(shopIds)
		.catch(error => caughtError = error);
	if (caughtError) throw caughtError;
	const userMapByShop = arrayToMapByField(dmUsers, 'shopId');
	const managerUserIds: number[] = Array.from(shops.reduce((set, shop) => {
		if (shop.dfId) set.add(shop.dfId);
		if (shop.tmId) set.add(shop.tmId);
		if (shop.upfId) set.add(shop.upfId);
		return set;
	}, new Set()));
	const managerUsers: any[] = await userStore.findAllByIds(managerUserIds)
		.catch(error => caughtError = error);
	if (caughtError) throw caughtError;
	const managerUserMap = arrayToMapById(managerUsers);
	// формируем excel файл для отчета
	const wb = new ExcelJS.Workbook();
	wb.creator = 'Mon-check.ru';
	wb.created = new Date();
	const ws = wb.addWorksheet(`Учет сотрудников - ${todayHuman}`, {
		views: [
			{ state: 'frozen', ySplit: 1 },
		],
	});
	const alignment: Partial<Alignment> = {
		horizontal: 'center',
		vertical: 'middle',
	};
	ws.columns = [
		{ header: 'ID магазина', width: 12 },
		{ header: 'Адрес', width: 30 },
		{ header: 'Город', width: 20 },
		{ header: 'Регион', width: 15 },
		{ header: 'ДФ', width: 35 },
		{ header: 'ТМ', width: 35 },
		{ header: 'УПФ', width: 35 },
		{ header: 'ID ДМ', width: 10 },
		{ header: 'ДМ', width: 35 },
		{ header: 'Вышло сотрудников', width: 15, style: { alignment } },
	];
	ws.getRow(1).height = 25;
	ws.getRow(1).alignment = { ...alignment, wrapText: true };
	// наполняем табличку
	shops.forEach((shop, index) => {
		const row = ws.getRow(index + 2);
		const _value = reportsMap.get(shop.id);
		const reportValue = (_value === undefined) ? '(Нет отчета)' : Number(_value);
		const region = regionMap.get(shop.regionId) as any;
		const dmUser = userMapByShop.get(shop.id) as any;
		row.values = [
			shop.externalId,
			shop.address,
			shop.city,
			region?.name,
			!shop.dfId ? '-' : (managerUserMap.has(shop.dfId) ? managerUserMap.get(shop.dfId).fullName || '(Без имени)' : '(Ошибка ссылки)'),
			!shop.tmId ? '-' : (managerUserMap.has(shop.tmId) ? managerUserMap.get(shop.tmId).fullName || '(Без имени)' : '(Ошибка ссылки)'),
			!shop.upfId ? '-' : (managerUserMap.has(shop.upfId) ? managerUserMap.get(shop.upfId).fullName || '(Без имени)' : '(Ошибка ссылки)'),
			dmUser?.externalId || '-',
			dmUser ? (dmUser.fullName || '(Без имени)') : '(Нет ДМ)', // Если ДМ нет, то как он сохранил отчет по учету сотрудников?
			// Может этого ДМ удалили после сохранения отчета?
			reportValue,
		];
	});
	ws.autoFilter = { from: 'A1', to: `J${shops.length + 1}` };
	// сохраняем файл
	const { pathName, fullPath } = FileService.generateNewFileName('', '.xlsx');
	await wb.xlsx.writeFile(fullPath)
		.catch(error => caughtError = error);
	if (caughtError) throw caughtError;
	const size = fs.statSync(fullPath).size;
	const xlsxFileType = await fileTypeStore.findIdByExt('xlsx')
		.catch(error => caughtError = error);
	if (caughtError) throw caughtError;
	return sequelize.transaction(async transaction => {
		const file = await fileStore.createOne({
			accountId,
			name: `pajero_${moment().format('YYYYMMDD')}.xlsx`,
			size,
			fileTypeId: xlsxFileType.id,
			pathName,
			ownerId: currentUser.id,
		}, transaction)
			.catch(error => caughtError = error);
		if (caughtError) throw caughtError;
		// формируем e-mail сообщение
		const mailMessageData = MailService.buildEmployeeCountReportMessage(
			null,
			toEmails,
			todayHuman,
			[file],
			currentUser,
		);
		return MailMessage.create(mailMessageData, { transaction })
			.then(() => {
				// запоминаем дату последнего отчета
				if (lastSentSetting) {
					return accountSettingStore.updateOne(lastSentSetting, {
						value: Date.now(),
					}, transaction);
				} else {
					return accountSettingStore.createOne({
						accountId,
						key: AccountSettingKeyEnum.SHOP_REPORTER_LAST_SENT,
						value: Date.now(),
						ownerId: currentUser.id,
					}, transaction);
				}
			});
	})
		.catch(async (err) => {
			logger.error(fillError(Error, '[Cannot commit transaction]', err));
			fs.unlinkSync(fullPath);
		})
		.then(() => {
			if (fs.existsSync(fullPath)) {
				// для выгрузки ftp копируем этот файл в папку откуда через ftp будут забирать отчет
				const ftpPath = config.get('shopReporter:employeeCountReport:ftpPath');
				return fs.promises.copyFile(fullPath, path.resolve(ftpPath, moment().format('YYYYMMDD_HH') + '.xlsx'));
			}
		});
}
