import { MailService } from '~services';
import BaseBackgroundTask, { BackgroundTaskReturnType } from './BaseBackgroundTask';
import { MailMessage } from '../../stores/db/entity';

/**
 * Задача проверяет исходящие и отправляет письма
 */
export default class MailerBackgroundTask extends BaseBackgroundTask
{
	protected override start(): Promise<BackgroundTaskReturnType> {
		const mailService = new MailService();
		return MailMessage.findAll()
			.then(async (mailMessages: any[]) => {
				for (const mailMessage of mailMessages) {
					await mailService.sendMailMessage(mailMessage);
				}
				return undefined;
			});
	}
}
