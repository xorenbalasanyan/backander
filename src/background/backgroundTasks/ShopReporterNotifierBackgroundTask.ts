import moment from 'moment';
import util from 'util';
import FirebaseAdmin from 'firebase-admin';
import { ONE_DAY_MS } from '~enums/DateEnum';
import { ShopReportingType } from '~enums/ShopReportingType';
import { arrayToSetByField } from '~utils/list';
import { isFirebaseInitialized } from '~lib/initFirebaseAdmin';
import { UserSettingRepository } from '~repositories';
import {
	accountStore,
	shopManagerStore,
	shopReportingStore,
	shopStore,
	userSettingStore,
} from '~stores';
import BaseBackgroundTask, {
	BackgroundTaskExecuteOptions,
	BackgroundTaskReturnType,
} from './BaseBackgroundTask';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const logger = new AppLogger('ShopReporterNotifierBackgroundTask');

/**
 * Логика формирования отчетов по магазинам.
 * На сегодня формируется только один отчет по типу EMPLOYEE_COUNT.
 * Этот отчет нужно формировать после 10:00 и после 12:00 - два отчета за сутки.
 */
export default class ShopReporterNotifierBackgroundTask extends BaseBackgroundTask
{
	protected override async start(options?: BackgroundTaskExecuteOptions): Promise<BackgroundTaskReturnType> {
		// FIXME проверка времени, нотификацию отправляем только в определенный час
		const now = moment().utcOffset(300);
		if (now.hour() !== 11 || now.minute() < 40) return;
		const accounts: any[] = await accountStore
			.findAll({
				where: {},
				attributes: ['id'],
				raw: true,
			})
			.catch(error => {
				throw fillError(Error, `[Ошибка получения списка аккаунтов]`, error);
			});
		for (const account of accounts) {
			await sendReportPushNotificationsForAccount(account)
				.catch(error => {
					throw fillError(Error, `[Ошибка отправки уведомлений]: ${JSON.stringify({ accountId: account.id })}`, error);
				});
		}
	}
}

async function sendReportPushNotificationsForAccount(account) {
	// кому будем отправлять пуши
	const shopIds = (await shopStore.findAll({
		where: { accountId: account.id },
		attributes: ['id'],
		raw: true,
	}))
		.map(i => i.id);
	const shopsWithReport = await shopReportingStore.findAll({
		where: {
			shopId: shopIds,
			reportType: ShopReportingType.EMPLOYEE_COUNT,
			date: moment().format('YYYY-MM-DD'),
		},
		attributes: ['id', 'date', 'shopId'],
		raw: true,
	});
	const shopIdsWithReportSet = arrayToSetByField(shopsWithReport, 'shopId');
	const shopIdsWithoutReport = shopIds.filter(id => !shopIdsWithReportSet.has(id));
	// список ДМ и ЗДМ и магазинов без отчетов
	const dmUserIds = (await shopManagerStore.findAllWithShopUserRole({
		where: {
			shopId: shopIdsWithoutReport,
		},
		attributes: ['id'],
		raw: true,
	}))
		.map(i => i.id);
	const userFcmSettings = (await userSettingStore.findAll({
		where: {
			key: UserSettingRepository.KEY_FCM_TOKEN,
			userId: dmUserIds,
		},
		attributes: ['value'],
		raw: true,
	}))
		.map(i => i.value).filter(s => !!s);
	// рассылаем пуши
	const notifications = userFcmSettings.map(token => ({
		token,
		notification: {
			title: `⏰ Заполните отчет по сотрудникам`,
		},
		android: {
			ttl: ONE_DAY_MS, // завтра уже не нужно отправлять
		},
	}));
	logger.debug(util.format('notifications = %o', notifications));
	// send notification messages
	if (notifications && notifications.length && isFirebaseInitialized()) {
		// firebase может отправлять за раз не более 500 уведомлений
		let part = 1;
		while (notifications.length > 0) {
			const cuttedNotifications = notifications.splice(0, 500);
			await FirebaseAdmin.messaging()
				.sendAll(cuttedNotifications)
				.then(response => {
					logger.info(util.format(
						'[Уведомление о незаполненных отчета по сотрудникам] Результат отправки (партия %d) в Firebase: %o',
						part++,
						response));
				})
				.catch(error => {
					logger.error(fillError(Error, '[Ошибка отправки уведомлений в Firebase]', error));
				});
		}
	}
}
