import util from 'util';
import { validateInteger } from '~utils/validate';
import BaseBackgroundTask, { BackgroundTaskReturnType } from './BaseBackgroundTask';
import { accountSettingStore, shopStore, userStore } from '~stores';
import StructureAnalisysStats from './StructureAnalysisStats';
import { UserRoles } from '~enums/UserRoleEnum';
import { arrayToMapById } from '~utils/list';
import { MailService } from '~services';
import { MailMessage } from '../../stores/db/entity';
import { fillError } from '~utils/errors';
import User from '../../stores/db/entity/User';

/**
 * Задача проверяет структуру на предмет соответствия привязок и ролей.
 */
export default class StructureAnalysisBackgroundTask extends BaseBackgroundTask
{
	protected override start(accountId: number): Promise<BackgroundTaskReturnType> {
		return new Promise((resolve, reject) => {
			// обновляем куб для одного аккаунта
			const check = validateInteger(1, Number.MAX_SAFE_INTEGER)(accountId);
			if (check) {
				throw new Error(util.format(
					'При запуске задачи "StructureAnalysisBackgroundTask" передано значение accountId=%o, ошибка валидации: %s',
					accountId,
					check));
			}
			checkStructureForAccountId(accountId)
				.then(() => resolve(undefined))
				.catch(reject);
		});
	}

	// перепишу метод чтобы ограничить входные данные
	override execute(accountId: number): Promise<BackgroundTaskReturnType> {
		return super.execute(accountId);
	}
}

async function checkStructureForAccountId(accountId: number) {
	// загружаем весь список магазинов и пользователей
	const shops = await shopStore
		.findAll({
			where: { accountId },
			raw: true,
		})
		.catch(error => {
			throw fillError(Error, `[Ошибка поиска магазинов аккаунта]: ${JSON.stringify({ accountId })}`, error);
		});
	const users = await userStore
		.findAll({
			where: { accountId },
			raw: true,
		})
		.catch(error => {
			throw fillError(Error, `[Ошибка поиска пользователей аккаунта]: ${JSON.stringify({ accountId })}`, error);
		});
	// проверяем поля магазинов и пользователей
	const stats = new StructureAnalisysStats();
	checkShops(shops, stats);
	checkUsers(users, stats);
	// генерим мапы
	const shopMap = arrayToMapById(shops);
	const userMap = arrayToMapById(users);
	// проверяем связки
	checkUserLinks(users, userMap, shopMap, stats);
	checkShopsManagers(shops, userMap, stats);
	// создаем письмо с отчетом
	return createStatsMessage(stats, accountId);
}

// проверка заполенных полей магазинов
function checkShops(shops: any[], stats: StructureAnalisysStats): void {
	const emptyExternalId: number[] = [];
	const emptyCity: number[] = [];
	const emptyRegion: number[] = [];
	const emptyAddress: number[] = [];
	const emptyTimeOffset: number[] = [];
	const emptyPhone: number[] = [];
	const emptyEmail: number[] = [];
	const emptyDf: number[] = [];
	const emptyTm: number[] = [];
	const emptyUpf: number[] = [];
	const timeOffsets: any = {};
	shops.forEach(shop => {
		if (!shop.externalId) emptyExternalId.push(shop.id);
		if (!shop.city) emptyCity.push(shop.id);
		if (!shop.regionId) emptyRegion.push(shop.id);
		if (!shop.address) emptyAddress.push(shop.id);
		if (!shop.phone) emptyPhone.push(shop.id);
		if (!shop.email) emptyEmail.push(shop.id);
		if (!shop.dfId) emptyDf.push(shop.id);
		if (!shop.tmId) emptyTm.push(shop.id);
		if (!shop.upfId) emptyUpf.push(shop.id);
		if (!shop.timeOffset) {
			emptyTimeOffset.push(shop.id);
		} else {
			timeOffsets[shop.timeOffset] = (timeOffsets[shop.timeOffset] || 0) + 1;
		}
	});
	if (emptyExternalId.length) stats.addError('У следующих магазинов не указан ВНЕШНИЙ ИДЕНТИФИКАТОР', emptyExternalId);
	if (emptyCity.length) stats.addError('У следующих магазинов не указан ГОРОД', emptyCity);
	if (emptyRegion.length) stats.addError('У следующих магазинов не указан РЕГИОН', emptyRegion);
	if (emptyAddress.length) stats.addError('У следующих магазинов не указан АДРЕС', emptyAddress);
	if (emptyTimeOffset.length) stats.addError('У магазинов не указан ЧАСОВОЙ ПОЯС', emptyTimeOffset);
	if (emptyDf.length) stats.addError('У следующих магазинов не указан ДФ', emptyDf);
	if (emptyTm.length) stats.addWarn('У следующих магазинов не указан ТМ', emptyTm);
	if (emptyUpf.length) stats.addWarn('У следующих магазинов не указан УПФ', emptyUpf);
	if (emptyPhone.length) stats.addWarn('У следующих магазинов не указан ТЕЛЕФОН', emptyPhone);
	if (emptyEmail.length) stats.addWarn('У следующих магазинов не указан EMAIL', emptyEmail);
	const tos = Object.keys(timeOffsets).map(key => `${key} мин = ${timeOffsets[key]} шт.`).join(', ');
	stats.addInfo('Часовые пояса в магазинах: ' + tos);
}

// проверка заполненных полей пользователей
function checkUsers(users: typeof User[], stats: StructureAnalisysStats): void {
	const emptyExternalId: number[] = [];
	const emptyShop: number[] = [];
	const emptyLastName: number[] = [];
	const emptyFirstName: number[] = [];
	const emptyEmail: number[] = [];
	const managerEmptyPhone: number[] = [];
	const chiefEmptyPhone: number[] = [];
	const rolesCount: any = {};
	users.forEach(user => {
		if (!user.externalId) emptyExternalId.push(user.id);
		if (!user.lastName) emptyLastName.push(user.id);
		if (!user.firstName) emptyFirstName.push(user.id);
		if (!user.email) emptyEmail.push(user.id);
		if (!user.phone && user.role === UserRoles.MANAGER) managerEmptyPhone.push(user.id);
		if (!user.phone && user.role === UserRoles.CHIEF) chiefEmptyPhone.push(user.id);
		rolesCount[user.role] = (rolesCount[user.role] || 0) + 1;
	});
	if (emptyExternalId.length) stats.addWarn('У следующих пользователей не указан ВНЕШНИЙ ИДЕНТИФИКАТОР', emptyExternalId);
	if (emptyShop.length) stats.addError('У следующих пользователей с ролью ДМ или ЗДМ не указан МАГАЗИН', emptyShop);
	if (emptyLastName.length) stats.addError('У следующих пользователей не указана ФАМИЛИЯ', emptyLastName);
	if (emptyFirstName.length) stats.addWarn('У следующих пользователей не указано ИМЯ', emptyFirstName);
	if (emptyEmail.length) stats.addWarn('У следующих пользователей не указан EMAIL', emptyEmail);
	if (managerEmptyPhone.length) stats.addWarn('У следующих сотрудников (директор магазина) не указан ТЕЛЕФОН', managerEmptyPhone);
	if (chiefEmptyPhone.length) stats.addWarn('У следующих сотрудников (менеджер торговой сети) не указан ТЕЛЕФОН', chiefEmptyPhone);
	const tos = Object.keys(rolesCount).map(key => `${key} = ${rolesCount[key]} шт.`).join(', ');
	stats.addInfo('Назначено ролей пользователям: ' + tos);
}

// проверяем, все ли менеджеры пользователей на своих ролях
function checkUserLinks(users: any[], userMap: Map<number, any>, shopMap: Map<number, any>, stats: StructureAnalisysStats): void {
	users.forEach(user => {
		// проверка привязки к магазину
		if (user.shopId) {
			if (user.role !== UserRoles.MANAGER) {
				stats.addWarn(`У пользователя #${user.id} указан магазин #${user.shopId}, хотя у этого пользователя роль '${user.role}'`);
			}
			const shop = shopMap.get(user.shopId);
			if (!shop) {
				stats.addError(`У пользователя #${user.id} указан несуществующий МАГАЗИН #${user.shopId}`);
			} else {
				// проверка совпадения менеджеров
				if (shop.dfId !== user.dfId) {
					stats.addError(`У пользователя #${user.id} указан ДФ #${user.dfId}, а у его МАГАЗИНА #${user.shopId} указан ДФ #${shop.dfId}`);
				}
				if (shop.tmId !== user.tmId) {
					stats.addError(`У пользователя #${user.id} указан ТМ #${user.tmId}, а у его МАГАЗИНА #${user.shopId} указан ТМ #${shop.tmId}`);
				}
				if (shop.upfId !== user.upfId) {
					stats.addError(`У пользователя #${user.id} указан УПФ #${user.upfId}, а у его МАГАЗИНА #${user.shopId} указан УПФ #${shop.upfId}`);
				}
			}
		}
	});
}

// проверяем, все ли менеджеры магазинов на своих ролях
function checkShopsManagers(shops: any[], userMap: Map<number, any>, stats: StructureAnalisysStats): void {
	shops.forEach(shop => {
		// проверка менеджеров
		if (shop.dfId) {
			const df = userMap.get(shop.dfId);
			if (!df) {
				stats.addError(`У магазина #${shop.id} указан несуществующий ДФ #${shop.dfId}`);
			}
			/* TODO xxx
			} else if (df.role !== USER_ROLE_DF) {
				stats.addError(`У магазина #${shop.id} указан ДФ #${shop.dfId}, у которого роль '${df.role}'`);
			}
			 */
		}
		if (shop.tmId) {
			const tm = userMap.get(shop.tmId);
			if (!tm) {
				stats.addError(`У магазина #${shop.id} указан несуществующий ТМ #${shop.tmId}`);
			}
			/* TODO xxx
			} else if (tm.role !== USER_ROLE_TM) {
				stats.addError(`У магазина #${shop.id} указан ТМ #${shop.tmId}, у которого роль '${tm.role}'`);
			}
			 */
		}
		if (shop.upfId) {
			const upf = userMap.get(shop.upfId);
			if (!upf) {
				stats.addError(`У магазина #${shop.id} указан несуществующий УПФ #${shop.upfId}`);
			}
			/* TODO xxx
			} else if (upf.role !== USER_ROLE_UPF) {
				stats.addError(`У магазина #${shop.id} указан УПФ #${shop.upfId}, у которого роль '${upf.role}'`);
			}
			 */
		}
	});
}

// создаем информационное письмо с результатами анализа
async function createStatsMessage(stats: StructureAnalisysStats, accountId: number) {
	let caughtError: any = false;
	// файл и отчет создает один из админов аккаунта
	const currentUser = await userStore
		.findOne({ where: { accountId, role: UserRoles.ADMIN } })
		.catch(error => caughtError = error);
	if (caughtError) throw caughtError;
	// кому будем отправлять очтет
	const addresseesSetting = await accountSettingStore
		.findOneStructureAnalysisReportAddresseesByAccountId(accountId)
		.catch(error => caughtError = error);
	if (caughtError) throw caughtError;
	const toEmails = addresseesSetting?.value.split(',').map(s => s.trim()).filter(s => !!s);
	if (!toEmails?.length) {
		// не кому отправлять отчет, значит нет смысла его формировать
		return false;
	}
	const mailMessageData = MailService.buildStructureAnalysisStatsMessage(stats, null, toEmails, currentUser);
	return MailMessage.create(mailMessageData);
}
