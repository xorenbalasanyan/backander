export default class StructureAnalisysStats
{
	errors: string[] = [];
	errorIdCount = 0;
	warns: string[] = [];
	warnIdCount = 0;
	infos: string[] = [];
	infoIdCount = 0;

	addError(msg: string, ids?: number[]) {
		const MAX = 100;
		if (ids?.length) {
			this.errorIdCount += ids.length;
			this.errors.push(`${msg} (${ids.length}):${ids.length > MAX ? ` (выведено только ${MAX})` : ''} ${ids.sort().slice(0, MAX).join(', ')}`);
		} else {
			this.errorIdCount++;
			this.errors.push(msg);
		}
	}

	addWarn(msg: string, ids?: number[]) {
		const MAX = 50;
		if (ids?.length) {
			this.warnIdCount += ids.length;
			this.warns.push(`${msg} (${ids.length}):${ids.length > MAX ? ` (выведено только ${MAX})` : ''} ${ids.sort().slice(0, MAX).join(', ')}`);
		} else {
			this.warnIdCount++;
			this.warns.push(msg);
		}
	}

	addInfo(msg: string, ids?: number[]) {
		if (ids?.length) {
			this.infoIdCount += ids.length;
			this.infos.push(`${msg} (${ids.length}): ${ids.sort().join(', ')}`);
		} else {
			this.infoIdCount++;
			this.infos.push(msg);
		}
	}
}
