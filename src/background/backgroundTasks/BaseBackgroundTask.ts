type ValueType = string | number | boolean | ValueType[] | Map<string, ValueType>;

// тип опция для запуска задачи
export type BackgroundTaskExecuteOptions = ValueType | undefined;

// результат промиса
export type BackgroundTaskReturnType = ValueType | undefined | void;

/**
 * Класс описывает фоновую задачу.
 * Задача может быть запущена одновременно только один раз.
 * При выполнении метода execute запоминается промис запущенной задачи.
 */
export default abstract class BaseBackgroundTask
{
	private static readonly stack: Map<string, Promise<BackgroundTaskReturnType>> = new Map();

	// если задача уже запущена, вернет промис из стека
	// если задача не запущена, запускает и запоминает ее в стеке
	async execute(options?: BackgroundTaskExecuteOptions): Promise<BackgroundTaskReturnType> {
		const className = this.constructor.name;
		const existPromise = BaseBackgroundTask.stack.get(className);
		if (existPromise) {
			return existPromise;
		}
		const taskPromise = this.start(options)
			.then((result: BackgroundTaskReturnType) => {
				// при завершении работы удаляем задачу из стека
				BaseBackgroundTask.stack.delete(className);
				return result;
			})
			.catch((error: any) => {
				BaseBackgroundTask.stack.delete(className);
				return Promise.reject(error);
			});
		// запоминаем задачу в стеке
		BaseBackgroundTask.stack.set(className, taskPromise);
		return taskPromise;
	}

	// стартует работу
	protected abstract start(options?: BackgroundTaskExecuteOptions): Promise<BackgroundTaskReturnType>;

	// завершает работу
	// можно использовать как предупреждение перед завершением приложения
	protected stop() {
		// по умолчанию завершение не требуется
	}
}
