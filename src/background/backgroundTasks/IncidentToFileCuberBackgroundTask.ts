import moment from 'moment';
import util from 'util';
import { IncidentStateEnum } from '~enums/IncidentStateEnum';
import { validateInteger } from '~utils/validate';
import { insert, select } from '~lib/sequelize';
import BaseBackgroundTask, { BackgroundTaskReturnType } from './BaseBackgroundTask';
import { toSqlDateTime } from '~utils/date';
import { fillError } from '~utils/errors';
import { AppLogger } from '~lib/AppLogger';

const logger = new AppLogger('IncidentToFileCuberBackgroundTask');

const CUBE_NAME = 'cube_incident_to_file';

/**
 * Задача чистит и заполняет куб привязки инцидентов к товарам и файлам.
 * Куб заполняется только для тех инцидентов, которые выполнены и имеют в своем отчете файлы.
 * Идея куба в том, чтобы быстро получить связку "incident - good - file in report".
 */
export default class IncidentToFileCuberBackgroundTask extends BaseBackgroundTask
{
	protected override async start(accountId: number): Promise<BackgroundTaskReturnType> {
		return new Promise((resolve, reject) => {
			// обновляем куб для одного аккаунта
			const check = validateInteger(1, Number.MAX_SAFE_INTEGER)(accountId);
			if (check) {
				throw new Error(util.format(
					'При запуске задачи "IncidentToFileCuberBackgroundTask" передано значение accountId=%o, ошибка валидации: %s',
					accountId,
					check));
			}
			appendCubeForAccountId(accountId)
				.then(() => resolve(undefined))
				.catch(reject);
		});
	}

	// перепишу метод чтобы ограничить входные данные
	override execute(accountId: number): Promise<BackgroundTaskReturnType> {
		return super.execute(accountId);
	}
}

// дозаполняет куб для одного аккаунта
// нет смысла перезатирать куб и заново заполнять связки, будет просто дополнять те, которые отсутствуют
async function appendCubeForAccountId(accountId: number): Promise<void> {
	let insertedCount = 0;
	logger.debug(util.format('Начато дозаполнение куба "%s" для аккаунта #%d', CUBE_NAME, accountId));
	// сначала узнаем последний incidentId в кубе за прошлый день
	const predate = moment().add(-1, 'days').format('YYYY-MM-DD');
	const row = await select(`SELECT MAX(incidentId) as maxid FROM ${CUBE_NAME} WHERE accountId = ${accountId} AND createdAt < '${predate}'`)
		.catch(error => {
			throw fillError(Error, '[Ошибка получения последнего индекса incidentId] на дату ' + predate, error);
		});

	let lastId = row?.[0]?.maxid || 0;
	const SIZE = 10_000; // размер страницы, количество элементов в одном запросе `INSERT INTO`
	let allReturnedCount = 0;
	let count = 0;
	const MAX_COUNT = 1_000; // максимальное количество запросов чтобы не уйти в безлимит
	while (count++ < MAX_COUNT) {
		logger.info(`lastId: ${lastId}, count: ${count}`);
		// получим инциденты, их товары, привязку к аккаунту
		let returnedCount = 0;
		await select(
			`SELECT i.id     AS incidentId,
					i.createdAt AS createdAt,
                    g.id     AS goodId,
                    i.report AS report
             FROM incident i
                  INNER JOIN good g ON i.goodId = g.id
             WHERE g.accountId = ${accountId}
               AND i.id > ${lastId}
               AND i.state = '${IncidentStateEnum.DONE}'
               AND i.report LIKE '%"fileId":%'
             ORDER BY i.id
             LIMIT 0, ${SIZE}`)
			.then(async (incidents: any[]) => {
				returnedCount = incidents.length;
				if (!returnedCount) {
					return;
				}
				// проверяем, есть ли такие файлы в кубе
				const checks = incidents.reduce((acc, i) => {
					// сразу запомним последний индекс
					lastId = Math.max(lastId, i.incidentId);
					acc.incidentIds.add(i.incidentId);
					const regExp = /"fileId":(\d+)/g;
					for (let match = regExp.exec(i.report); match; match = regExp.exec(i.report)) {
						const fileId = Number(match[1]);
						if (fileId > 0) {
							acc.fileIds.add(fileId);
						}
					}
					return acc;
				}, { incidentIds: new Set<number>(), fileIds: new Set<number>() });
				const exist = await select(`SELECT incidentId, fileId
                                            FROM ${CUBE_NAME}
                                            WHERE accountId = ${accountId}
                                              AND incidentId IN (${Array.from(checks.incidentIds).join(',')})
                                              AND fileId IN (${Array.from(checks.fileIds).join(',')})`)
					.catch(error => {
						throw fillError(Error, '[Ошибка проверки существующих данных в кубе]', error);
					});
				// фильтруем инциденты
				const existSet = new Set();
				exist.forEach(i => existSet.add(`${i.incidentId}_${i.fileId}`));
				incidents = incidents.filter(i => {
					const regExp = /"fileId":(\d+)/g;
					for (let match = regExp.exec(i.report); match; match = regExp.exec(i.report)) {
						const fileId = match[1];
						return !existSet.has(`${i.incidentId}_${fileId}`);
					}
					return;
				});
				// добавляем данные, которых еще нет в кубе
				const cubeData: any[] = [];
				for (const { incidentId, goodId, report, createdAt } of incidents) {
					const regExp = /"fileId":(\d+)/g;
					for (let match = regExp.exec(report); match; match = regExp.exec(report)) {
						const fileId = match[1];
						cubeData.push(`${accountId},${incidentId},${goodId},${fileId},'${toSqlDateTime(createdAt)}'`);
					}
				}
				if (cubeData.length) {
					const query = `INSERT INTO ${CUBE_NAME} (accountId, incidentId, goodId, fileId, createdAt)
                                   VALUES (${cubeData.join('),(')})`;
					return insert(query)
						.then(() => insertedCount += incidents.length);
				}
				// ничего не добавлялось
				return 0;
			})
			.catch(error => {
				throw fillError(Error, '[Ошибка получения инцидентов в связке с товарами]', error);
			});
		allReturnedCount += returnedCount;
		if (returnedCount < SIZE) {
			break; // конец достигнут, завершаем цикл
		}
	}
	logger.info([
		'Завершено нормально.',
		`Итого: lastId: ${lastId}, циклов: ${count}, проверено строк: ${allReturnedCount}, добавлено строк в куб: ${insertedCount}`,
	].join(' '));
}
