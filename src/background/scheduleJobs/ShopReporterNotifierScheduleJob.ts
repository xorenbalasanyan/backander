import BaseScheduleJob from './BaseScheduleJob';
import ShopReporterNotifierBackgroundTask from '../backgroundTasks/ShopReporterNotifierBackgroundTask';

export default class ShopReporterNotifierScheduleJob extends BaseScheduleJob
{
	async execute() {
		return new ShopReporterNotifierBackgroundTask().execute();
	}
}
