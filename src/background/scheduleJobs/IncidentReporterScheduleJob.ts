import BaseScheduleJob from './BaseScheduleJob';
import IncidentReporterBackgroundTask from '../backgroundTasks/IncidentReporterBackgroundTask';

export default class IncidentReporterScheduleJob extends BaseScheduleJob
{
	async execute() {
		return new IncidentReporterBackgroundTask().execute();
	}
}
