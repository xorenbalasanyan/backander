import BaseScheduleJob from './BaseScheduleJob';
import ShopReporterBackgroundTask from '../backgroundTasks/ShopReporterBackgroundTask';

export default class ShopReporterScheduleJob extends BaseScheduleJob
{
	async execute() {
		return new ShopReporterBackgroundTask().execute();
	}
}
