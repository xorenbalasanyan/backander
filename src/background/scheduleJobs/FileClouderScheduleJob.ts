import BaseScheduleJob from './BaseScheduleJob';
import FileClouderBackgroundTask from '../backgroundTasks/FileClouderBackgroundTask';

export default class FileClouderScheduleJob extends BaseScheduleJob
{
	async execute() {
		return new FileClouderBackgroundTask().execute(this.options);
	}
}
