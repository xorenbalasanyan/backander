import { ScheduleJobUnitEnum } from '~enums/ScheduleJobUnitEnum';
import { BackgroundTaskExecuteOptions } from '../backgroundTasks/BaseBackgroundTask';

export type JobTimetable = {
	every?: JobTimetableEvery
	launch?: JobTimetableLaunchTime
	executeOnStart: boolean
};

type JobTimetableEvery = {
	value: number
	unit: ScheduleJobUnitEnum
};

type JobTimetableLaunchTime = {
	hour: number
	minute: number
};

/**
 * Класс описывает джобу, которая выполняет определенное задание в фоне с определенным расписанием.
 * Расписание настраивается в файле конфигурации.
 */
export default abstract class BaseScheduleJob
{
	// расписание джобы
	private readonly _schedule: JobTimetable;
	private readonly _options: BackgroundTaskExecuteOptions;

	protected constructor(schedule: JobTimetable, options: BackgroundTaskExecuteOptions) {
		this._schedule = schedule;
		this._options = options;
	}

	get schedule(): JobTimetable {
		return this._schedule;
	}

	get options(): BackgroundTaskExecuteOptions {
		return this._options;
	}

	// имя требуется для хранения данных о запуске в базе
	get jobName(): string {
		return this.constructor.name;
	}

	// метод вызывает шедулер при срабатывании расписания
	// метод может быть async, в этом случае шедулер будет в фоне дожидаться завершения
	abstract execute();

	// метод вызывает шедулер, когда завершает свою работу
	stop() {
		// по умолчанию завершение не требуется
	}
}
