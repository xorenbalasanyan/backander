import BaseScheduleJob from './BaseScheduleJob';
import IncidentStatsCuberBackgroundTask from '../backgroundTasks/IncidentStatsCuberBackgroundTask';

export default class IncidentStatsCuberScheduleJob extends BaseScheduleJob
{
	async execute() {
		// TODO пока только один аккаунт, поэтому обновляем куб для первого аккаунта
		return new IncidentStatsCuberBackgroundTask().execute(100);
	}
}
