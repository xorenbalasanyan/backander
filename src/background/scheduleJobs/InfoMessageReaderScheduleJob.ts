import BaseScheduleJob from './BaseScheduleJob';
import InfoMessageReaderBackgroundTask from '../backgroundTasks/InfoMessageReaderBackgroundTask';

export default class InfoMessageReaderScheduleJob extends BaseScheduleJob
{
	async execute() {
		return new InfoMessageReaderBackgroundTask().execute();
	}
}
