import BaseScheduleJob from './BaseScheduleJob';
import OldDataCleanBackgroundTask from '../backgroundTasks/OldDataCleanBackgroundTask';

export default class OldDataCleanScheduleJob extends BaseScheduleJob
{
	async execute() {
		return new OldDataCleanBackgroundTask().execute(this.options);
	}
}
