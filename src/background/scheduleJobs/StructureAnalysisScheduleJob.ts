import BaseScheduleJob from './BaseScheduleJob';
import StructureAnalysisBackgroundTask from '../backgroundTasks/StructureAnalysisBackgroundTask';

export default class StructureAnalysisScheduleJob extends BaseScheduleJob
{
	async execute() {
		// TODO пока только один аккаунт, поэтому обновляем куб для первого аккаунта
		return new StructureAnalysisBackgroundTask().execute(100);
	}
}
