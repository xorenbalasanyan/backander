import BaseStore from './BaseStore';
import { AccountSettingKeyEnum } from '~enums/AccountSettingKeyEnum';
import AccountSettingDbStore from './db/AccountSettingDbStore';

export default class AccountSettingStore extends BaseStore
{
	constructor() {
		super(AccountSettingDbStore);
	}

	/**
	 * Ищет адресатов для отправки очтета по учету сотрудников.
	 * @param accountId
	 */
	findOneEmployeeCountReportAddresseesByAccountId = async accountId => this.findOne({
		where: {
			accountId,
			key: AccountSettingKeyEnum.EMPLOYEE_COUNT_REPORT_ADDRESSEES,
		},
	})

	/**
	 * Ищет адресатов для отправки очтета по учету температуры.
	 * @param accountId
	 */
	findOneTemperatureCheckReportAddresseesByAccountId = async accountId => this.findOne({
		where: {
			accountId,
			key: AccountSettingKeyEnum.TEMPERATURE_CHECK_REPORT_ADDRESSEES,
		},
	})

	/**
	 * Ищет адресатов для отправки очтета по учету проблем.
	 * @param accountId
	 */
	findOneProblemCheckReportAddresseesByAccountId = async accountId => this.findOne({
		where: {
			accountId,
			key: AccountSettingKeyEnum.PROBLEM_CHECK_REPORT_ADDRESSEES,
		},
	})

	/**
	 * Ищет адресатов для отправки очтета по учету сотрудников.
	 * @param accountId
	 */
	findOnePersonnelCheckReportAddresseesByAccountId = async accountId => this.findOne({
		where: {
			accountId,
			key: AccountSettingKeyEnum.PERSONNEL_CHECK_REPORT_ADDRESSEES,
		},
	})

	/**
	 * Ищет адресатов для отправки очтета по камерам.
	 * @param accountId
	 */
	findOneCameraCheckReportAddresseesByAccountId = async accountId => this.findOne({
		where: {
			accountId,
			key: AccountSettingKeyEnum.CAMERA_CHECK_REPORT_ADDRESSEES,
		},
	})

	/**
	 * Ищет адресатов для отправки очтета анализа структуры.
	 * @param accountId
	 */
	findOneStructureAnalysisReportAddresseesByAccountId = async accountId => this.findOne({
		where: {
			accountId,
			key: AccountSettingKeyEnum.STRUCTURE_ANALISYS_REPORT_ADDRESSEES,
		},
	})
}
