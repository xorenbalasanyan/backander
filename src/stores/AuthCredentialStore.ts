import BaseStore from './BaseStore';
import AuthCredentialDbStore from './db/AuthCredentialDbStore';

export default class AuthCredentialStore extends BaseStore
{
	constructor() {
		super(AuthCredentialDbStore);
	}
}
