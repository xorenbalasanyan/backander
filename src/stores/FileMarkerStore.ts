import BaseStore from './BaseStore';
import FileMarkerDbStore from './db/FileMarkerDbStore';

export default class FileMarkerStore extends BaseStore
{
	constructor() {
		super(FileMarkerDbStore);
	}
}
