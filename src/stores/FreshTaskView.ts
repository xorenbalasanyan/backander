import BaseView from './BaseView';
import FreshTaskDbView from './db/FreshTaskDbView';

export default class FreshTaskView extends BaseView
{
	constructor() {
		super(FreshTaskDbView);
	}
}
