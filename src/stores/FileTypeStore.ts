import BaseStore from './BaseStore';
import FileTypeDbStore from './db/FileTypeDbStore';
import { Transaction } from 'sequelize';

export default class FileTypeStore extends BaseStore
{
	constructor() {
		super(FileTypeDbStore);
	}

	findIdByExt = async ext => this.findOne({
		where: { ext },
		attributes: ['id'],
		raw: true,
	})

	findIdByMimeType = async (mimeType, transaction?: Transaction) => this.findOne({
		where: { mimeType },
		attributes: ['id'],
		raw: true,
	}, transaction)
}
