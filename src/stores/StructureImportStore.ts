import BaseStore from './BaseStore';
import StructureImportDbStore from './db/StructureImportDbStore';

export default class StructureImportStore extends BaseStore
{
	constructor() {
		super(StructureImportDbStore);
	}
}
