import BaseStore from './BaseStore';
import ViolationTaskDbStore from './db/ViolationTaskDbStore';

export default class ViolationTaskStore extends BaseStore
{
	constructor() {
		super(ViolationTaskDbStore);
	}
}
