import merge from 'deepmerge';
import BaseStore from './BaseStore';
import ShopManagerDbStore from './db/ShopManagerDbStore';

export default class ShopManagerStore extends BaseStore
{
	constructor() {
		super(ShopManagerDbStore);
	}

	findAllWithShopUserRole = async (options: any): Promise<any[]> => {
		// find shop user roles
		const roles = await this.findAll({ where: { isShopUser: true } });
		return await this.findAll(merge({
			where: {
				roleId: roles.map(r => r.id),
			},
		},
		options));
	}
}
