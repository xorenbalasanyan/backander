import BaseStore from './BaseStore';
import ViolationTaskToShopDbStore from './db/ViolationTaskToShopDbStore';

export default class ViolationTaskToShopStore extends BaseStore
{
	constructor() {
		super(ViolationTaskToShopDbStore);
	}
}
