import BaseStore from './BaseStore';
import TodoItemDbStore from './db/TodoItemDbStore';

export default class TodoItemStore extends BaseStore
{
	constructor() {
		super(TodoItemDbStore);
	}
}
