import BaseStore from './BaseStore';
import RegionDbStore from './db/RegionDbStore';

export default class RegionStore extends BaseStore
{
	constructor() {
		super(RegionDbStore);
	}
}
