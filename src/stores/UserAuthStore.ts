import BaseStore from './BaseStore';
import UserAuthDbStore from './db/UserAuthDbStore';

export default class UserAuthStore extends BaseStore
{
	constructor() {
		super(UserAuthDbStore);
	}
}
