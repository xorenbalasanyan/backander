import AccountSettingStore from './AccountSettingStore';
import AccountStore from './AccountStore';
import FileStore from './FileStore';
import FileMarkerStore from './FileMarkerStore';
import FileTypeStore from './FileTypeStore';
import GoodStore from './GoodStore';
import IncidentImportStore from './IncidentImportStore';
import IncidentStore from './IncidentStore';
import IncidentTypeStore from './IncidentTypeStore';
import InfoMessageStore from './InfoMessageStore';
import InfoMessageToAddresseeStore from './InfoMessageToAddresseeStore';
import LogStore from './LogStore';
import RegionStore from './RegionStore';
import ScheduleJobSettingStore from './ScheduleJobSettingStore';
import ShopReportingStore from './ShopReportingStore';
import ShopStore from './ShopStore';
import ShopDiscountStore from './ShopDiscountStore';
import TaskStore from './TaskStore';
import TaskToShopStore from './TaskToShopStore';
import TodoItemStore from './TodoItemStore';
import UserAuthStore from './UserAuthStore';
import UserSettingStore from './UserSettingStore';
import UserStore from './UserStore';
import StructureImportStore from './StructureImportStore';
import IncidentDaylyReportStore from './IncidentDaylyReportStore';
import ViolationTaskStore from './ViolationTaskStore';
import ViolationTaskToShopStore from './ViolationTaskToShopStore';
import FreshTaskView from './FreshTaskView';
import FreshViolationTaskView from './FreshViolationTaskView';
import AuthCredentialStore from './AuthCredentialStore';
import ShopManagerStore from './ShopManagerStore';
import ShopManagerRoleStore from './ShopManagerRoleStore';

const accountStore = new AccountStore();
const accountSettingStore = new AccountSettingStore();
const authCredentialStore = new AuthCredentialStore();
const fileStore = new FileStore();
const fileMarkerStore = new FileMarkerStore();
const fileTypeStore = new FileTypeStore();
const goodStore = new GoodStore();
const incidentDaylyReportStore = new IncidentDaylyReportStore();
const incidentStore = new IncidentStore();
const incidentTypeStore = new IncidentTypeStore();
const incidentImportStore = new IncidentImportStore();
const infoMessageStore = new InfoMessageStore();
const infoMessageToAddresseeStore = new InfoMessageToAddresseeStore();
const logStore = new LogStore();
const regionStore = new RegionStore();
const scheduleJobSettingStore = new ScheduleJobSettingStore();
const shopStore = new ShopStore();
const shopManagerStore = new ShopManagerStore();
const shopManagerRoleStore = new ShopManagerRoleStore();
const shopDiscountStore = new ShopDiscountStore();
const shopReportingStore = new ShopReportingStore();
const structureImportStore = new StructureImportStore();
const taskStore = new TaskStore();
const taskToShopStore = new TaskToShopStore();
const todoItemStore = new TodoItemStore();
const userStore = new UserStore();
const userAuthStore = new UserAuthStore();
const userSettingStore = new UserSettingStore();
const violationTaskStore = new ViolationTaskStore();
const violationTaskToShopStore = new ViolationTaskToShopStore();
const freshTaskView = new FreshTaskView();
const freshViolationTaskView = new FreshViolationTaskView();

export {
	accountStore,
	accountSettingStore,
	authCredentialStore,
	fileStore,
	fileMarkerStore,
	fileTypeStore,
	goodStore,
	incidentDaylyReportStore,
	incidentStore,
	incidentTypeStore,
	incidentImportStore,
	infoMessageStore,
	infoMessageToAddresseeStore,
	logStore,
	regionStore,
	scheduleJobSettingStore,
	shopStore,
	shopManagerStore,
	shopManagerRoleStore,
	shopDiscountStore,
	shopReportingStore,
	structureImportStore,
	taskStore,
	taskToShopStore,
	todoItemStore,
	userStore,
	userAuthStore,
	userSettingStore,
	violationTaskStore,
	violationTaskToShopStore,
	freshTaskView,
	freshViolationTaskView,
};
