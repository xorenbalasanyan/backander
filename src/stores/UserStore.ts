import { Op, Transaction } from 'sequelize';
import BaseStore from './BaseStore';
import UserDbStore from './db/UserDbStore';

export default class UserStore extends BaseStore
{
	constructor() {
		super(UserDbStore);
	}

	findAllByShopIds = async (ids: number[], transaction?: Transaction): Promise<any[]> => this
		.findAll({ where: { shopId: { [Op.in]: ids } } }, transaction)
}
