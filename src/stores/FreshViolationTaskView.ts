import BaseView from './BaseView';
import FreshViolationTaskDbView from './db/FreshViolationTaskDbView';

export default class FreshViolationTaskView extends BaseView
{
	constructor() {
		super(FreshViolationTaskDbView);
	}
}
