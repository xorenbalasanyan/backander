import BaseStore from './BaseStore';
import AccountDbStore from './db/AccountDbStore';

export default class AccountStore extends BaseStore
{
	constructor() {
		super(AccountDbStore);
	}
}
