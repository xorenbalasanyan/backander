import BaseStore from './BaseStore';
import IncidentImportDbStore from './db/IncidentImportDbStore';

export default class IncidentImportStore extends BaseStore
{
	constructor() {
		super(IncidentImportDbStore);
	}
}
