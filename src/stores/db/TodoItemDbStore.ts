import BaseDbStore from './BaseDbStore';
import {TodoItem} from './entity';

export default class TodoItemDbStore extends BaseDbStore
{
	constructor() {
		super(TodoItem);
	}
}
