import BaseDbStore from './BaseDbStore';
import { ShopReporting } from './entity';

export default class ShopReportingDbStore extends BaseDbStore
{
	constructor() {
		super(ShopReporting);
	}
}
