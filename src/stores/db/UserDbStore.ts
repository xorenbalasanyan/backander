import BaseDbStore from './BaseDbStore';
import { User } from './entity';

export default class UserDbStore extends BaseDbStore
{
	constructor() {
		super(User);
	}
}
