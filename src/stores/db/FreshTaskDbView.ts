import { FreshTask } from './views';
import BaseDbView from './BaseDbView';

export default class FreshTaskDbView extends BaseDbView
{
	constructor() {
		super(FreshTask);
	}
}
