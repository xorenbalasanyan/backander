import Sequelize from 'sequelize';
import { DefineLoggedEntity } from '../entity/tools';

const FreshViolationTask = DefineLoggedEntity(
	'violation_task',
	'fresh_violation_task_view',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		title: {
			type: Sequelize.STRING(200),
			allowNull: false,
		},
		description: {
			type: Sequelize.TEXT,
		},
		executionPeriodDays: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
	});

FreshViolationTask.prototype.toDto = function toDto() {
	return {
		id: this.id,
		title: this.title,
		description: this.description,
		executionPeriodDays: this.executionPeriodDays,
		ownerId: this.ownerId,
	};
};

export default FreshViolationTask;
