import BaseDbStore from './BaseDbStore';
import { ViolationTaskToShop } from './entity';

export default class ViolationTaskToShopDbStore extends BaseDbStore
{
	constructor() {
		super(ViolationTaskToShop);
	}
}
