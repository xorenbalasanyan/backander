import BaseDbStore from './BaseDbStore';
import { Region } from './entity';

export default class RegionDbStore extends BaseDbStore
{
	constructor() {
		super(Region);
	}
}
