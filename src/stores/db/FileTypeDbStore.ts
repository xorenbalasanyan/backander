import BaseDbStore from './BaseDbStore';
import { FileType } from './entity';

export default class FileTypeDbStore extends BaseDbStore
{
	constructor() {
		super(FileType);
	}
}
