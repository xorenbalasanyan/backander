import { Incident } from '../../stores/db/entity';
import BaseDbStore from './BaseDbStore';

export default class IncidentDbStore extends BaseDbStore
{
	constructor() {
		super(Incident);
	}
}
