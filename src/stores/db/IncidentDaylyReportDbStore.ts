import {IncidentDaylyReport} from './entity';
import BaseDbStore from './BaseDbStore';

export default class IncidentDaylyReportDbStore extends BaseDbStore
{
	constructor() {
		super(IncidentDaylyReport);
	}
}
