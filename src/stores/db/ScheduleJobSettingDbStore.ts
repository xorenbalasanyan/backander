import BaseDbStore from './BaseDbStore';
import { ScheduleJobSetting } from './entity';

export default class ScheduleJobSettingDbStore extends BaseDbStore
{
	constructor() {
		super(ScheduleJobSetting);
	}
}
