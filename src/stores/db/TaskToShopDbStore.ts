import BaseDbStore from './BaseDbStore';
import { TaskToShop } from './entity';

export default class TaskToShopDbStore extends BaseDbStore
{
	constructor() {
		super(TaskToShop);
	}
}
