import { IncidentType } from '../../stores/db/entity';
import BaseDbStore from './BaseDbStore';

export default class IncidentTypeDbStore extends BaseDbStore
{
	constructor() {
		super(IncidentType);
	}
}
