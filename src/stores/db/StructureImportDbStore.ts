import {StructureImport} from '../../stores/db/entity';
import BaseDbStore from './BaseDbStore';

export default class StructureImportDbStore extends BaseDbStore
{
	constructor() {
		super(StructureImport);
	}
}
