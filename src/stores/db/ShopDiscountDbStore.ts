import BaseDbStore from './BaseDbStore';
import { ShopDiscount } from './entity';

export default class ShopDiscountDbStore extends BaseDbStore
{
	constructor() {
		super(ShopDiscount);
	}
}
