import BaseDbStore from './BaseDbStore';
import { ViolationTask } from './entity';

export default class ViolationTaskDbStore extends BaseDbStore
{
	constructor() {
		super(ViolationTask);
	}
}
