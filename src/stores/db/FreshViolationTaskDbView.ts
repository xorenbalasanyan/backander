import { FreshViolationTask } from './views';
import BaseDbView from './BaseDbView';

export default class FreshViolationTaskDbView extends BaseDbView
{
	constructor() {
		super(FreshViolationTask);
	}
}
