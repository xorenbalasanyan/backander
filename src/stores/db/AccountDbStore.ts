import BaseDbStore from './BaseDbStore';
import { Account } from './entity';

export default class AccountDbStore extends BaseDbStore
{
	constructor() {
		super(Account);
	}
}
