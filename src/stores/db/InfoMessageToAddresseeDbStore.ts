import BaseDbStore from './BaseDbStore';
import { InfoMessageToAddressee } from './entity';

export default class InfoMessageToAddresseeDbStore extends BaseDbStore
{
	constructor() {
		super(InfoMessageToAddressee);
	}

	override count = async (options, transaction) => {
		return this.entityType.count({
			...options,
			group: [`infoMessageToAddressee.infoMessageId`], // model name here
		}, transaction);
	}
}
