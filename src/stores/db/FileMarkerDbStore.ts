import BaseDbStore from './BaseDbStore';
import { FileMarker } from './entity';

export default class FileMarkerDbStore extends BaseDbStore
{
	constructor() {
		super(FileMarker);
	}
}
