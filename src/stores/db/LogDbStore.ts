import BaseDbStore from './BaseDbStore';
import { Log } from './entity';

export default class LogDbStore extends BaseDbStore
{
	constructor() {
		super(Log);
	}
}
