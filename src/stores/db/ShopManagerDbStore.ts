import BaseDbStore from './BaseDbStore';
import { ShopManager } from './entity';

export default class ShopManagerDbStore extends BaseDbStore
{
	constructor() {
		super(ShopManager);
	}
}
