import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const IncidentDaylyReport = DefineLoggedEntity(
	'incidentDaylyReport',
	'incident_dayly_report',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		fileId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		date: {
			type: Sequelize.DATEONLY,
			allowNull: false,
		},
	});

IncidentDaylyReport.prototype.toDto = function toDto() {
	return {
		id: this.id,
		date: this.date,
		fileId: this.fileId,
	};
};

export default IncidentDaylyReport;
