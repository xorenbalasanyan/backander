import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const TodoItem = DefineLoggedEntity(
	'todoItem',
	'todo_item',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		externalId: {
			type: Sequelize.STRING(100),
			allowNull: true,
		},
		date: {
			type: Sequelize.DATEONLY,
			allowNull: false,
		},
		addresseeId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		status: {
			type: Sequelize.STRING(50),
			allowNull: false,
		},
		title: {
			type: Sequelize.STRING(2000),
			allowNull: false,
		},
		description: {
			type: Sequelize.TEXT,
			allowNull: true,
		},
	});

TodoItem.prototype.toDto = function toDto() {
	return {
		id: this.id,
		externalId: this.externalId,
		date: this.date,
		status: this.status,
		title: this.title,
		description: this.description,
		createdAt: this.createdAt,
		updatedAt: this.updatedAt,
	};
};

export default TodoItem;
