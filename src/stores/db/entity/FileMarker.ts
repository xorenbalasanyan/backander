import Sequelize from 'sequelize';
import { FileMarkerType } from '~enums/FileMarkerType';
import sequelize from '../../../lib/sequelize';

const FileMarker = sequelize.define(
	'FileMarker',
	{
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			allowNull: false,
			autoIncrement: true,
		},
		fileId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		marker: {
			type: Sequelize.ENUM(FileMarkerType.BAD, FileMarkerType.MATCH, FileMarkerType.LATER),
			allowNull: true,
		},
		createdAt: {
			type: Sequelize.DATE,
			allowNull: false,
		},
		updatedAt: {
			type: Sequelize.DATE,
			allowNull: false,
		},
	},
	{
		tableName: 'file_marker',
		createdAt: true,
		updatedAt: true,
	},
);

FileMarker.prototype.toDto = function toDto() {
	return {
		id: this.id,
		fileId: this.fileId,
		marker: this.marker,
	};
};

export default FileMarker;
