import Sequelize from 'sequelize';
import {DefineLoggedEntity} from './tools';

const Shop = DefineLoggedEntity(
	'shop',
	'shop',
	{
		externalId: {
			type: Sequelize.STRING(100),
		},
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		name: {
			type: Sequelize.STRING(100),
			allowNull: false,
		},
		regionId: {
			type: Sequelize.INTEGER,
			allowNull: true,
		},
		city: {
			type: Sequelize.STRING(100),
		},
		address: {
			type: Sequelize.STRING(100),
		},
		phone: {
			type: Sequelize.STRING(100),
		},
		email: {
			type: Sequelize.STRING(100),
		},
		description: Sequelize.TEXT,
		timeOffset: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		isDeleted: {
			type: Sequelize.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},
	},
	{
		defaultScope: {
			where: {
				isDeleted: false,
			},
		},
	});

Shop.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		id: this.id,
		externalId: this.externalId || undefined,
		name: this.name,
		regionId: this.regionId || undefined,
		city: this.city || undefined,
		address: this.address || undefined,
		phone: this.phone || undefined,
		email: this.email || undefined,
		timeOffset: this.timeOffset,
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default Shop;
