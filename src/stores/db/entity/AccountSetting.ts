import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const AccountSetting = DefineLoggedEntity(
	'accountSetting',
	'account_setting',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		key: {
			type: Sequelize.STRING(50),
			allowNull: false,
		},
		value: {
			type: Sequelize.TEXT,
			allowNull: false,
		},
	},
	{
		setterMethods: {
			account(value) {
				if (!value || !value.id) {
					throw new Error('Account.id must be not null when set account in AccountSetting entity');
				}
				// @ts-ignore
				this.setDataValue('accountId', value.id);
			},
		},
	});

AccountSetting.prototype.toDto = function toDto() {
	throw new Error('Hey buddy! Method AccountSetting.toDto not released');
};

export default AccountSetting;
