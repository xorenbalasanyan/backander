import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const Account = DefineLoggedEntity(
	'account',
	'account',
	{
		title: {
			type: Sequelize.STRING(100),
			allowNull: false,
			unique: true,
		},
		contacts: Sequelize.TEXT,
		description: Sequelize.TEXT,
		email: {
			type: Sequelize.STRING(100),
			unique: true,
		},
		phone: {
			type: Sequelize.STRING(100),
			unique: true,
		},
	},
);

Account.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		id: this.id,
		title: this.title,
		contacts: this.contacts || undefined,
		email: this.email || undefined,
		phone: this.phone || undefined,
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default Account;
