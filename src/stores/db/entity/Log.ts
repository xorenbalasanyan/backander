import Sequelize from 'sequelize';
import { DefineNotUpdatedEntity } from './tools';

const Log = DefineNotUpdatedEntity(
	'log',
	'log',
	{
		accountId: Sequelize.INTEGER,
		userId: Sequelize.INTEGER,
		shopId: Sequelize.INTEGER,
		incidentId: Sequelize.INTEGER,
		type: {
			type: Sequelize.STRING(30),
		},
		message: Sequelize.TEXT,
		reason: {
			type: Sequelize.STRING(200),
		},
	},
	{
		updatedAt: false,
	});

Log.prototype.toDto = function toDto() {
	return {
		id: this.id,
		message: this.message || undefined,
	};
};

export default Log;
