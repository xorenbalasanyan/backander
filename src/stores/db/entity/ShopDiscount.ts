import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const ShopDiscount = DefineLoggedEntity(
	'shopDiscount',
	'shop_discount',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		shopId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		name: {
			type: Sequelize.STRING(100),
			allowNull: false,
		},
	});

ShopDiscount.prototype.toDto = function () {
	return {
		id: this.id,
	};
};

export default ShopDiscount;
