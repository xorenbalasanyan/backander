import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const ShopManagerRole = DefineLoggedEntity(
	'shopManagerRole',
	'shop_manager_role',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		title: {
			type: Sequelize.STRING(100),
			allowNull: false,
		},
		shortTitle: {
			type: Sequelize.STRING(10),
			allowNull: false,
		},
		engShortTitle: {
			type: Sequelize.STRING(10),
			allowNull: false,
		},
		subLevel: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		isShopUser: {
			type: Sequelize.BOOLEAN,
			allowNull: false,
		},
	});

ShopManagerRole.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		id: this.id,
		title: this.title,
		shortTitle: this.shortTitle,
		engShortTitle: this.engShortTitle,
		subLevel: this.subLevel,
		isShopUser: this.isShopUser,
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default ShopManagerRole;
