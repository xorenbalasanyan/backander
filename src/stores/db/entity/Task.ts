import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const Task = DefineLoggedEntity(
	'task',
	'task',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		title: {
			type: Sequelize.STRING(200),
			allowNull: false,
		},
		description: {
			type: Sequelize.TEXT,
			allowNull: true,
		},
		executionPeriodDays: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
	});

Task.prototype.toDto = function toDto() {
	return {
		id: this.id,
		title: this.title,
		description: this.description || undefined,
		executionPeriodDays: this.executionPeriodDays,
		ownerId: this.ownerId,
	};
};

export default Task;
