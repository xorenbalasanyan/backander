import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const InfoMessage = DefineLoggedEntity(
	'infoMessage',
	'info_message',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		subject: {
			type: Sequelize.STRING(300),
			allowNull: false,
		},
		fileId: Sequelize.INTEGER,
		isImportant: {
			type: Sequelize.BOOLEAN,
			allowNull: false,
		},
		externalMessageId: Sequelize.STRING,
		externalMessageDate: Sequelize.DATE,
	},
	null);

InfoMessage.prototype.toDto = function toDto() {
	return {
		id: this.id,
		subject: this.subject,
		fileId: this.fileId,
		isImportant: this.isImportant,
		externalMessageId: this.externalMessageId,
		externalMessageDate: this.externalMessageDate,
		stats: this.stats || undefined,
		ownerId: this.ownerId,
		createdAt: this.createdAt,
	};
};

export default InfoMessage;
