import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';
import { IncidentStateEnum } from '~enums/IncidentStateEnum';

const Incident = DefineLoggedEntity(
	'incident',
	'incident',
	{
		externalId: {
			type: Sequelize.STRING(100),
		},
		shopId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		shopDiscountId: {
			type: Sequelize.INTEGER,
		},
		incidentTypeId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		incidentTypeVersion: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		goodId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		date: {
			type: Sequelize.DATEONLY,
			allowNull: false,
		},
		state: {
			type: Sequelize.ENUM(
				IncidentStateEnum.TODO,
				IncidentStateEnum.DONE,
			),
			allowNull: false,
			defaultValue: IncidentStateEnum.TODO,
		},
		inputData: Sequelize.TEXT,
		report: Sequelize.TEXT,
		isReported: {
			type: Sequelize.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},
	},
	{
		defaultScope: {
			where: {
				isReported: false,
			},
		},
	});

Incident.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		id: this.id,
		shopId: this.shopId,
		shopDiscountId: this.shopDiscountId,
		incidentTypeId: this.incidentTypeId,
		goodId: this.goodId,
		state: this.state,
		inputData: this.inputData,
		report: this.report,
		isReported: this.isReported,
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default Incident;
