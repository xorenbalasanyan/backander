import Sequelize from 'sequelize';
import { InfoMessageToAddresseeEnum } from '~enums/InfoMessageToAddresseeEnum';
import sequelize from '../../../lib/sequelize'; // FIXME импорт по алиасу

const InfoMessageToAddressee = sequelize.define(
	'infoMessageToAddressee',
	{
		infoMessageId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		addresseeUserId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		status: {
			type: Sequelize.ENUM(
				InfoMessageToAddresseeEnum.CREATED,
				InfoMessageToAddresseeEnum.VIEWED,
			),
			allowNull: false,
		},
		updatedAt: {
			type: Sequelize.DATE,
			allowNull: true,
		},
		moderatorId: {
			type: Sequelize.INTEGER,
			allowNull: true,
		},
	},
	{
		tableName: 'info_message_to_addressee',
		setterMethods: {
			moderator(value) {
				// @ts-ignore
				this.setDataValue('moderatorId', value === null ? null : value.id);
			},
		},
		createdAt: false,
	},
);

InfoMessageToAddressee.prototype.toDto = function toDto() {
	return {
		infoMessageId: this.infoMessageId,
		addresseeUserId: this.addresseeUserId,
		status: this.status,
		updatedAt: this.updatedAt,
	};
};

export default InfoMessageToAddressee;
