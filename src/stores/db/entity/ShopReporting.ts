import Sequelize from 'sequelize';
import { ShopReportingType } from '~enums/ShopReportingType';
import { DefineLoggedEntity } from './tools';

const ShopReporting = DefineLoggedEntity(
	'shopReporting',
	'shop_reporting',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		shopId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		reportType: {
			type: Sequelize.STRING(50),
			allowNull: false,
		},
		date: {
			type: Sequelize.DATEONLY,
			allowNull: false,
		},
		value: Sequelize.TEXT,
		isReported: {
			type: Sequelize.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},
	},
	{
		setterMethods: {
			account(value) {
				if (!value || !value.id) {
					throw new Error('Account.id must be not null when set account in Shopreporting entity');
				}
				// @ts-ignore
				this.setDataValue('accountId', value.id);
			},
			shop(value) {
				if (!value || !value.id) {
					throw new Error('Shop.id must be not null when set shop in Shopreporting entity');
				}
				// @ts-ignore
				this.setDataValue('shopId', value.id);
			},
		},
	});

ShopReporting.prototype.toDto = function toDto() {
	return {
		id: this.id,
		shopId: this.shopId,
		reportType: this.reportType,
		date: this.date,
		value: this.value,
		isReported: this.isReported,
	};
};

export default ShopReporting;
