import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const UserAuth = DefineLoggedEntity(
	'userAuth',
	'user_auth',
	{
		userId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		authToken: {
			type: Sequelize.STRING(100),
			allowNull: false,
		},
		refreshToken: {
			type: Sequelize.STRING(100),
			allowNull: false,
		},
		ipAddress: {
			type: Sequelize.STRING(15),
			allowNull: false,
		},
		secondaryUserId: {
			type: Sequelize.INTEGER,
			allowNull: true,
		},
		lastUrl: {
			type: Sequelize.STRING(200),
			allowNull: true,
		},
	});

UserAuth.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		ipAddress: this.ipAddress || undefined,
		createdAt: this.createdAt,
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default UserAuth;
