import Sequelize from 'sequelize';
import sequelize from '../../../lib/sequelize';

const FileType = sequelize.define(
	'FileType',
	{
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			allowNull: false,
			autoIncrement: true,
		},
		ext: {
			type: Sequelize.STRING(10),
			allowNull: false,
		},
		mimeType: {
			type: Sequelize.STRING(200),
			allowNull: false,
		},
		createdAt: {
			type: Sequelize.DATE,
			allowNull: false,
		},
		updatedAt: {
			type: Sequelize.DATE,
			allowNull: false,
		},
	},
	{
		tableName: 'file_type',
		createdAt: true,
		updatedAt: true,
	},
);

FileType.prototype.toDto = function toDto() {
	return {
		id: this.id,
		ext: this.ext,
		type: this.type,
	};
};

export default FileType;
