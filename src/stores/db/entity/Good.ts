import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const Good = DefineLoggedEntity(
	'good',
	'good',
	{
		externalId: {
			type: Sequelize.STRING(100),
		},
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		name: {
			type: Sequelize.STRING(500),
			allowNull: false,
		},
	});

Good.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		id: this.id,
		externalId: this.externalId,
		name: this.name,
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default Good;
