import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const ShopManager = DefineLoggedEntity(
	'shopManager',
	'shop_manager',
	{
		shopId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		roleId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		userId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
	});

ShopManager.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		id: this.id,
		shopId: this.shopId,
		roleId: this.roleId,
		userId: this.userId,
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default ShopManager;
