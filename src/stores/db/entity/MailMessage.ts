import Sequelize, { Op } from 'sequelize';
import { DefineLoggedEntity } from './tools';
import { MailMessageStatusEnum } from '~enums/MailMessageStatusEnum';

const MailMessage = DefineLoggedEntity(
	'mailMessage',
	'mail_message',
	{
		from: {
			type: Sequelize.STRING(300),
		},
		to: {
			type: Sequelize.STRING(300),
			allowNull: false,
		},
		cc: {
			type: Sequelize.STRING(300),
		},
		bcc: {
			type: Sequelize.STRING(300),
		},
		subject: {
			type: Sequelize.STRING(300),
			allowNull: false,
		},
		text: {
			type: Sequelize.TEXT,
			allowNull: false,
		},
		fileIds: {
			type: Sequelize.STRING(300),
		},
		status: {
			type: Sequelize.ENUM(
				MailMessageStatusEnum.CREATED,
				MailMessageStatusEnum.SENDED,
				MailMessageStatusEnum.ERROR,
			),
			allowNull: false,
			defaultValue: MailMessageStatusEnum.CREATED,
		},
		errorCount: {
			type: Sequelize.INTEGER,
			defaultValue: 0,
			allowNull: false,
		},
	},
	{
		defaultScope: {
			where: {
				[Op.or]: [
					{
						status: MailMessageStatusEnum.CREATED,
					},
					{
						[Op.and]: [
							{
								status: MailMessageStatusEnum.ERROR,
							},
							{
								errorCount: {
									[Op.lt]: 5,
								},
							},
						],
					},
				],
			},
		},
	});

MailMessage.prototype.toDto = function toDto() {
	return { id: this.id };
};

export default MailMessage;
