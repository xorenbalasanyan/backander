import Sequelize from 'sequelize';
import { DefineNotUpdatedEntity } from './tools';

const ScheduleJobSetting = DefineNotUpdatedEntity(
	'scheduleJobSetting',
	'schedule_job_settings',
	{
		jobName: {
			type: Sequelize.STRING(100),
			allowNull: false,
		},
		key: {
			type: Sequelize.STRING(100),
			allowNull: false,
		},
		value: {
			type: Sequelize.TEXT,
			allowNull: false,
		},
	},
	{
		createdAt: true,
		updatedAt: true,
	});

ScheduleJobSetting.prototype.toDto = function toDto() {
	throw new Error('Метод не реализован');
};

export default ScheduleJobSetting;
