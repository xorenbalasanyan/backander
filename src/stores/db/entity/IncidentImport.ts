import Sequelize from 'sequelize';
import { DefineNotUpdatedEntity } from './tools';
import { IncidentImportStateEnum } from '~enums/IncidentImportStateEnum';

const IncidentImport = DefineNotUpdatedEntity(
	'incidentImport',
	'incident_import',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		fileId: Sequelize.INTEGER,
		logId: Sequelize.INTEGER,
		date: {
			type: Sequelize.DATEONLY,
			allowNull: false,
		},
		incidentTypeId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		state: {
			type: Sequelize.ENUM(
				IncidentImportStateEnum.OK,
				IncidentImportStateEnum.WARN,
				IncidentImportStateEnum.ERROR,
			),
			allowNull: false,
			defaultValue: IncidentImportStateEnum.OK,
		},
		addedCount: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		warnCount: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
	});

IncidentImport.prototype.toDto = function toDto() {
	return {
		id: this.id,
		fileId: this.fileId || undefined,
		logId: this.logId || undefined,
		date: this.date,
		incidentTypeId: this.incidentTypeId,
		state: this.state,
		addedCount: this.addedCount,
		warnCount: this.warnCount,
		ownerId: this.ownerId,
		createdAt: this.createdAt,
	};
};

export default IncidentImport;
