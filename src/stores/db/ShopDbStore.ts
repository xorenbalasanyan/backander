import BaseDbStore from './BaseDbStore';
import { Shop } from './entity';

export default class ShopDbStore extends BaseDbStore
{
	constructor() {
		super(Shop);
	}
}
