import { IncidentImport } from '../../stores/db/entity';
import BaseDbStore from './BaseDbStore';

export default class IncidentImportDbStore extends BaseDbStore
{
	constructor() {
		super(IncidentImport);
	}
}
