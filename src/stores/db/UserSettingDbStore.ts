import BaseDbStore from './BaseDbStore';
import { UserSetting } from './entity';

export default class UserSettingDbStore extends BaseDbStore
{
	constructor() {
		super(UserSetting);
	}
}
