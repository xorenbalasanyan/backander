import BaseStore from './BaseStore';
import IncidentTypeDbStore from './db/IncidentTypeDbStore';

export default class IncidentStore extends BaseStore
{
	constructor() {
		super(IncidentTypeDbStore);
	}
}
