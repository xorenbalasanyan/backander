import BaseStore from './BaseStore';
import ShopDiscountDbStore from './db/ShopDiscountDbStore';

export default class ShopDiscountStore extends BaseStore
{
	constructor() {
		super(ShopDiscountDbStore);
	}
}
