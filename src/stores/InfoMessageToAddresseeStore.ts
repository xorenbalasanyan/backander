import BaseStore from './BaseStore';
import InfoMessageToAddresseeDbStore from './db/InfoMessageToAddresseeDbStore';

export default class InfoMessageToAddresseeStore extends BaseStore
{
	constructor() {
		super(InfoMessageToAddresseeDbStore);
	}
}
