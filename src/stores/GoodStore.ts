import BaseStore from './BaseStore';
import GoodDbStore from './db/GoodDbStore';

export default class GoodStore extends BaseStore
{
	constructor() {
		super(GoodDbStore);
	}
}
