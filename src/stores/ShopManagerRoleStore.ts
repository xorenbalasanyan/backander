import BaseStore from './BaseStore';
import ShopManagerRoleDbStore from './db/ShopManagerRoleDbStore';

export default class ShopManagerRoleStore extends BaseStore
{
	constructor() {
		super(ShopManagerRoleDbStore);
	}
}
