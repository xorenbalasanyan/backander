import BaseStore from './BaseStore';
import InfoMessageDbStore from './db/InfoMessageDbStore';

export default class InfoMessageStore extends BaseStore
{
	constructor() {
		super(InfoMessageDbStore);
	}
}
