import BaseStore from './BaseStore';
import LogDbStore from './db/LogDbStore';

export default class LogStore extends BaseStore
{
	constructor() {
		super(LogDbStore);
	}
}
