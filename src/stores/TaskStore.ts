import BaseStore from './BaseStore';
import TaskDbStore from './db/TaskDbStore';

export default class TaskStore extends BaseStore
{
	constructor() {
		super(TaskDbStore);
	}
}
