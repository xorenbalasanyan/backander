import BaseStore from './BaseStore';
import IncidentDbStore from './db/IncidentDbStore';

export default class IncidentStore extends BaseStore
{
	constructor() {
		super(IncidentDbStore);
	}
}
