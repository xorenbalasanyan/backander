import BaseStore from './BaseStore';
import IncidentDaylyReportDbStore from './db/IncidentDaylyReportDbStore';

export default class IncidentDaylyReportStore extends BaseStore
{
	constructor() {
		super(IncidentDaylyReportDbStore);
	}
}
