import BaseStore from './BaseStore';
import ShopReportingDbStore from './db/ShopReportingDbStore';

export default class ShopReportingStore extends BaseStore
{
	constructor() {
		super(ShopReportingDbStore);
	}
}
