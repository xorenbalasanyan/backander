import { shopManagerStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

export default class ShopManagerService extends BaseEntityService
{
	constructor(req) {
		super(req, new AppLogger(ShopManagerService.name), shopManagerStore);
	}
}
