import { Transaction } from 'sequelize';
import { ViolationTaskStatusEnum } from '~enums/ViolationTaskStatusEnum';
import ResponseError from '~errors/ResponseError';
import { dateAddDays, dateSetTimeForTimeZone } from '~utils/date';
import { validateInteger, validateIsPositiveNumber, validateString } from '~utils/validate';
import sequelize from '~lib/sequelize';
import { ShopService } from '~services';
import { freshViolationTaskView, violationTaskStore, violationTaskToShopStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('ViolationTaskService');

function validateOneTaskData(violationTaskData) {
	const {
		title,
		description,
		executionPeriodDays,
		subordinateShopIds,
		...otherArgs
	} = violationTaskData;
	if (Object.keys(otherArgs).length) {
		throw ResponseError.validationInput(`Поле "${Object.keys(otherArgs)[0]}" не поддерживается`);
	}
	if (validateString(5, 200)(title)) {
		throw ResponseError.validationInput('Поле title должно быть строкой длиной от 5 до 200');
	}
	if (description != null && validateString(0, 2000)(description)) {
		throw ResponseError.validationInput('Поле description может быть null или должно быть строкой длиной от 0 до 2000');
	}
	if (validateInteger(1, 15)(executionPeriodDays)) {
		throw ResponseError.validationInput('Поле executionPeriodDays должно быть целым числом от 1 до 15');
	}
	if (!subordinateShopIds || subordinateShopIds.constructor.name !== 'Array') {
		throw ResponseError.validationInput('Поле subordinateShopIds должно быть массивом');
	} else {
		const isPositive = validateIsPositiveNumber();
		if (subordinateShopIds.some(isPositive)) {
			throw ResponseError.validationInput('Поле subordinateShopIds должно быть массивом с целыми числами');
		}
	}
}

export default class ViolationTaskService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, violationTaskStore);
	}

	private async createOneWithTransaction(violationTask: any, shopService: ShopService, transaction: Transaction): Promise<any> {
		const currentUser = this.req.getAuthorizedUser();
		violationTask = {
			...violationTask,
			accountId: currentUser.accountId,
			ownerId: currentUser.id,
			title: violationTask.title.trim(),
			description: violationTask.description?.trim() || null,
		};
		const { subordinateShopIds } = violationTask;
		delete violationTask.subordinateShopIds;
		// create ViolationTask
		const savedItem = await this.store.createOne(violationTask, transaction);
		// проверяем магазины и получаем часовой пояс магазина, чтобы рассчитать dueDate
		const shops = await shopService.findAllByIds(subordinateShopIds, transaction);
		if (shops.length !== subordinateShopIds.length) {
			throw ResponseError.validationInput('Один из магазинов не найден');
		}
		const newSubs = shops.map(shop => ({
			violationTaskId: savedItem.id,
			shopId: shop.id,
			status: ViolationTaskStatusEnum.IN_PROGRESS,
			dueDate: dateAddDays(dateSetTimeForTimeZone(new Date(), shop.timeOffset, 0, 0, 0, 0), violationTask.executionPeriodDays),
		}));
		await violationTaskToShopStore.createAll(newSubs, transaction);
		return savedItem;
	}

	/**
	 * Создание списка ViolationTask
	 * @param violationTasks
	 */
	async createItems(violationTasks) {
		violationTasks.forEach(validateOneTaskData);
		return sequelize.transaction(async transaction => {
			const result: any[] = [];
			const shopService = new ShopService(this.req);
			for (const violationTask of violationTasks) {
				result.push(await this.createOneWithTransaction(violationTask, shopService, transaction));
			}
			return result;
		});
	}

	/**
	 * Создание ViolationTask
	 * @param violationTask
	 */
	createOne = async violationTask => {
		validateOneTaskData(violationTask);
		return sequelize.transaction(async transaction => {
			return this.createOneWithTransaction(violationTask, new ShopService(this.req), transaction);
		});
	}

	/**
	 * Селектим только не протухшие нарушения (прошло не более 30 дней с даты протухания).
	 * @param options
	 * @param transaction
	 * @returns {Promise<any>}
	 */
	findAllFreshViolationTasks = async (options, transaction?: Transaction): Promise<any[]> => freshViolationTaskView
		.findAll(this._fillOptions(options), transaction);
}
