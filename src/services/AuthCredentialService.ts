import { authCredentialStore } from '~stores';
import { AppLogger } from '~lib/AppLogger';
import BaseEntityService from './BaseEntityService';

export default class AuthCredentialService extends BaseEntityService
{
	constructor(req) {
		super(req, new AppLogger(AuthCredentialService.name), authCredentialStore);
	}
}
