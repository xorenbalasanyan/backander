import { fileTypeStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('FileTypeService');

export default class FileTypeService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, fileTypeStore);
	}
}
