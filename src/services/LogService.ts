import { logStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('LogService');

export default class LogService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, logStore);
	}
}
