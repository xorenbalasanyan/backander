import { userSettingStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('UserSettingService');

export default class UserSettingService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, userSettingStore);
	}
}
