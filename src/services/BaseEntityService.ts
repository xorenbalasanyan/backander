import { Transaction } from 'sequelize';
import PageInfo from '~utils/PageInfo';
import BaseService from './BaseService';
import BaseStore from '../stores/BaseStore';
import { AppLogger } from '~lib/AppLogger';

/**
 * Базовый сервис для сервисов моделей.
 */
export default class BaseEntityService extends BaseService
{
	constructor(req: any, moduleLogger: AppLogger, private readonly _store: BaseStore) {
		super(req, moduleLogger);
	}

	get store() {
		return this._store;
	}

	addWhereSearch(_options, _pageInfo) {
		throw new Error(`Метод ${this.constructor.name}.addWhereSearch не реализован.`);
	}

	addMeta(_options, _pageInfo) {
		throw new Error(`Метод ${this.constructor.name}.addMeta не реализован.`);
	}

	/**
	 * Поиск одной записи по заявленным условиям.
	 * @param options
	 * @param transaction
	 * @returns {Promise<*>}
	 */
	findOne = (options, transaction?: Transaction): Promise<any> => {
		if (!options?.where) {
			throw new Error(`Для вызова ${this.constructor.name}.findOne нужно указать условие where`);
		}
		const currentUser = this.req.getAuthorizedUser();
		options.where.accountId = currentUser.accountId;
		return this.store.findOne(options, transaction);
	}

	/**
	 * Поиск нескольких записей по аккаунту
	 * @param options
	 * @param transaction
	 * @returns {Promise<*>}
	 */
	findAllWithAccountId = (options, transaction?: Transaction): Promise<any[]> => {
		if (!options?.where) {
			throw new Error(`Для вызова ${this.constructor.name}.findAllWithAccountId нужно указать условие where`);
		}
		const currentUser = this.req.getAuthorizedUser();
		options.where.accountId = currentUser.accountId;
		return this.store.findAll(options, transaction);
	}

	/**
	 * Поиск нескольких записей
	 * @param options
	 * @param transaction
	 * @returns {Promise<*>}
	 */
	findAll = (options, transaction?: Transaction): Promise<any[]> => this
		.store.findAll(this._fillOptions(options, false), transaction)

	findAllUnscoped = (options, transaction?: Transaction): Promise<any[]> => this
		.store.findAll(this._fillOptions(options), transaction)

	findAllByIds = (idList, transaction?: Transaction): Promise<any[]> => this
		.findAll({ where: { id: idList } }, transaction)

	findOneById = (id: number, transaction?: Transaction): Promise<any> => this
		.findOne({ where: { id } }, transaction)

	findOneByExternalId = (externalId, transaction?: Transaction): Promise<any> => this
		.findOne({ where: { externalId } }, transaction)

	findAllByExternalId = (externalId: string | number, transaction?: Transaction): Promise<any[]> => this
		.findAll({ where: { externalId } }, transaction)

	findOneByIdWithoutAccountId = (id: number): Promise<any> => this
		.findOneWithoutAccountId({ where: { id } })

	/**
	 * Поиск одной записи по заявленным условиям, без поиска по аккаунту.
	 * @param options
	 * @returns {Promise<*>}
	 */
	findOneWithoutAccountId = (options: any, transaction?: Transaction): Promise<any> => {
		if (!options?.where) {
			throw new Error(`Для вызова ${this.constructor.name}.findOneWithoutAccountId нужно указать условие where`);
		}
		return this.store.findOne(options, transaction);
	}

	/**
	 * Поиск нескольких записей по условиям и настройкам страницы.
	 * @param options
	 * @param pageInfo
	 * @returns {Promise<*[]|*>}
	 */
	async findAllWithPageInfo(options: any, pageInfo: PageInfo): Promise<any[]> {
		let caughtError: any = false;
		if (!options?.where) {
			// искуственно ограничиваем поиск всех элементов
			// чтобы выполнить поиск всех записей необходимо передать `{ where: {} }`
			throw new Error(`Для вызова ${this.constructor.name}.findAllWithPageInfo нужно указать условие where`);
		}
		const currentUser = this.req.getAuthorizedUser();
		// добавлять проверку для аккаунта нужно не всегда
		if (!['IncidentStore'].includes(this.store.constructor.name)) {
			options.where.accountId = currentUser.accountId;
		}
		const findOptions: any = { where: options.where }; // init where
		if (options.include) {
			findOptions.include = options.include;
		}
		if (pageInfo.meta?.size) {
			this.addMeta(findOptions, pageInfo); // add include by meta
		}
		if (pageInfo.search) {
			this.addWhereSearch(findOptions, pageInfo); // add search
		}
		let count = 0;
		if (pageInfo.pageIndex !== undefined && pageInfo.pageIndex >= 0 && pageInfo.pageSize !== undefined && pageInfo.pageSize > 0) {
			count = await this.store
				.count(findOptions) // общее количество записей с фильтром where
				.catch(error => caughtError = error);
			if (caughtError) {
				throw caughtError;
			}
			findOptions.offset = pageInfo.pageIndex * pageInfo.pageSize;
			findOptions.limit = pageInfo.pageSize;
		}
		if (pageInfo.orderColumn) {
			findOptions.order = [[pageInfo.orderColumn, pageInfo.orderDirection || 'asc']];
		}
		return this.store.findAll(findOptions)
			.then(items => [items, {
				pageIndex: pageInfo.pageIndex,
				pageSize: pageInfo.pageSize,
				pageCount: Math.floor((count - 1) / (pageInfo.pageSize || 0) + 1),
				allCount: count,
				orderColumn: pageInfo.orderColumn,
				orderDirection: pageInfo.orderDirection,
				search: pageInfo.search || undefined,
			}]);
	}

	createOne = async (item, transaction?: Transaction): Promise<any> => {
		const currentUser = this.req.getAuthorizedUser();
		item.accountId = currentUser.accountId;
		item.ownerId = currentUser.id;
		return this.store.createOne(item, transaction);
	}

	createAll = async (items, transaction?: Transaction): Promise<any> => {
		const currentUser = this.req.getAuthorizedUser();
		items.forEach(i => {
			i.accountId = currentUser.accountId;
			i.ownerId = currentUser.id;
		});
		return this.store.createAll(items, transaction);
	}

	updateOne = async (item: any, newData: any, transaction?: Transaction): Promise<any> => {
		const currentUser = this.req.getAuthorizedUser();
		item.moderatorId = currentUser.id;
		return this.store.updateOne(item, newData, transaction);
	}

	updateAll = async (items: any[], newData: any, transaction?: Transaction): Promise<any> => {
		const currentUser = this.req.getAuthorizedUser();
		items.forEach(i => i.moderatorId = currentUser.id);
		return this.store.updateAll(items, newData, transaction);
	}

	deleteWhere = async (where, transaction?: Transaction): Promise<void> => this.store
		.deleteWhere(where, transaction)

	/**
	 * Метод проверят и обогащает опции поиска.
	 * @param options
	 * @param addAccountId
	 * @returns {{where}}
	 * @private
	 */
	protected _fillOptions = (options, addAccountId = true) => {
		if (!options?.where) {
			// искуственно ограничиваем поиск всех элементов
			// чтобы выполнить поиск всех записей необходимо передать `{ where: {} }`
			throw new Error(`Для вызова ${this.constructor.name}.findAll нужно указать условие where`);
		}
		if (addAccountId) {
			const currentUser = this.req.getAuthorizedUser();
			options.where.accountId = currentUser.accountId;
		}
		const findOptions: any = {
			where: options.where,
			attributes: options.attributes, // набор аттрибутов
			offset: options.offset, // отступ
			limit: options.limit, // количество строк в результате
			raw: options.raw, // сырые данные, не нужно маппить в модель
		}; // init options
		if (options.search) {
			this.addWhereSearch(findOptions, { search: options.search }); // add search
		}
		findOptions.include = options.include || undefined; // include
		return findOptions;
	}
}
