import { shopManagerRoleStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';
import ShopManagerRole from '../stores/db/entity/ShopManagerRole';

export default class ShopManagerRoleService extends BaseEntityService
{
	constructor(req) {
		super(req, new AppLogger(ShopManagerRoleService.name), shopManagerRoleStore);
	}

	getShopUserRoles = async (): Promise<typeof ShopManagerRole[]> => {
		return await this.findAll({
			where: {
				accountId: this.req.getAuthorizedUser().accountId,
				isShopUser: true,
			},
		});
	}
}
