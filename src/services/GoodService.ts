import { Op } from 'sequelize';
import { goodStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('GoodService');

export default class GoodService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, goodStore);
	}

	findAllByExternalIds = async externalIds => this.findAll({ where: { externalId: { [Op.in]: externalIds } } });
}
