import fs from 'fs';
import util from 'util';
import { Op } from 'sequelize';
import { TaskStatusEnum } from '~enums/TaskStatusEnum';
import ResponseError, { NOT_ALL_REQUIRED_FIELDS } from '~errors/ResponseError';
import { copyFields } from '~utils/copyFields';
import { validateIsPositiveNumber, validateString } from '~utils/validate';
import sequelize from '~lib/sequelize';
import { FileService } from '~services';
import { fileTypeStore, taskToShopStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { File } from '../stores/db/entity';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('TaskToShopService');

const validateOneTaskToShopExecuteData = taskToShopData => {
	const availableKeys = ['executionComment', 'executionPhotos'];
	const data = copyFields(taskToShopData, {}, availableKeys);
	if (!data) {
		throw NOT_ALL_REQUIRED_FIELDS;
	}
	if (validateString(5, 2000)(data.executionComment)) {
		throw ResponseError.validationInput('Поле executionComment должно быть строкой длиной от 5 до 2000');
	}
	if (!data.executionPhotos?.length) {
		throw ResponseError.validationInput('Поле executionPhotos должно быть массивом и содержать значения');
	}
	data.executionPhotos.forEach(({ isNew, data: photoData, id, isDeleted }) => {
		if (isDeleted && isNew) {
			throw ResponseError.validationInput('Информация о фотографии не может быть new и deleted одновременно');
		}
		if (isDeleted) {
			if (!id) {
				throw ResponseError.validationInput('Если указан флаг isDeleted, то должен быть указан значение для id');
			}
			if (validateIsPositiveNumber()(id)) {
				throw ResponseError.validationInput('Поле id для удаления фотографии должно быть положительным числом');
			}
		}
		if (isNew) {
			if (!/^data:image\/(png|jpg|jpeg|gif);base64,/.test(photoData)) {
				throw ResponseError.validationInput('Поле data фотографии должно содержать данные картинки в кодировке base64. '
					+ 'Поддерживаемые mime-типы: image/png, image/jpg, image/jpeg, image/gif.');
			}
		}
	});
};

const validateOneTaskToShopRejectData = taskToShopData => {
	const availableKeys = ['rejectionComment'];
	const data = copyFields(taskToShopData, {}, availableKeys);
	if (!data) {
		throw NOT_ALL_REQUIRED_FIELDS;
	}
	if (validateString(5, 2000)(data.rejectionComment)) {
		throw ResponseError.validationInput('Поле rejectionComment должно быть строкой длиной от 5 до 2000');
	}
};

export default class TaskToShopService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, taskToShopStore);
	}

	executeOne = async (taskToShopId, taskToShopData) => {
		validateOneTaskToShopExecuteData(taskToShopData);
		this.logger.http(util.format('Сохранение решения TaskToShop с данными: %o', taskToShopData));
		const oldTaskToShop = await this.findOneByIdWithoutAccountId(taskToShopId);

		const currentUser = this.req.getAuthorizedUser();
		return sequelize.transaction(async transaction => {
			const { photos = [] } = JSON.parse(oldTaskToShop.executionInfo || '{}');
			const filesToDelete: number[] = [];
			// сначала проверяем и удаляем старые фото
			taskToShopData.executionPhotos.filter(p => p.isDeleted).forEach(({ id: photoFileId }) => {
				const index = photos.indexOf(photoFileId);
				if (index === -1) {
					throw ResponseError.validationInput('Фотография %d не существует в задаче %d', photoFileId, taskToShopId);
				}
				filesToDelete.push(photoFileId);
				photos.splice(index, 1);
			});
			// удаляем ссылки на файлы
			if (filesToDelete.length) {
				await File.destroy({ where: { id: { [Op.in]: filesToDelete } } }, { transaction });
			}
			// теперь добавляем новые фото
			await Promise.all(taskToShopData.executionPhotos.filter(p => p.isNew).map(async ({ data: photoData }) => {
				const [found, mimeType, imageFileExt] = photoData.match('^data:(image/(png|jpg|jpeg|gif));base64,');
				const data = Buffer.from(photoData.substr(found.length), 'base64');
				const imageFileType = await fileTypeStore.findIdByMimeType(mimeType, transaction);
				// сохраняем картинку в файл
				const {
					fileName: name,
					pathName,
					fullPath,
				} = FileService.generateNewFileName('', `.${imageFileExt}`);
				fs.writeFileSync(fullPath, data);
				const size = fs.statSync(fullPath).size;
				const file: any = await File.create({
					name,
					size,
					fileTypeId: imageFileType.id,
					pathName,
					ownerId: currentUser.id,
					accountId: currentUser.accountId,
				}, { transaction });
				photos.push(file.id);
			}));
			// сохраняем изменения
			return this.store.updateOne(oldTaskToShop, {
				status: TaskStatusEnum.DONE,
				executor: currentUser,
				executionComment: taskToShopData.executionComment.trim(),
				executionInfo: JSON.stringify({ photos }),
				executedAt: new Date(),
			}, transaction);
		});
	}

	rejectOne = async (taskToShopId, taskToShopData) => {
		validateOneTaskToShopRejectData(taskToShopData);
		this.logger.http(util.format('Отклонение TaskToShop с данными: %o', taskToShopData));
		const oldTaskToShop = await this.findOneByIdWithoutAccountId(taskToShopId);
		const currentUser = this.req.getAuthorizedUser();
		// сохраняем изменения
		return sequelize.transaction(async transaction => this.store.updateOne(oldTaskToShop, {
			status: TaskStatusEnum.REJECTED,
			rejector: currentUser,
			rejectionComment: taskToShopData.rejectionComment.trim(),
			rejectedAt: new Date(),
		}, transaction));
	}
}
