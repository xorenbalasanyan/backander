import { Op, Transaction } from 'sequelize';
import { regionStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('RegionService');

export default class RegionService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, regionStore);
	}

	findAllByEmails = async (emails, transaction?: Transaction) => this.store.findAll({ where: { email: { [Op.in]: emails } } }, transaction)

	findOneByName = async (name, transaction?: Transaction) => this.store.findOne({ where: { name } }, transaction)
}
