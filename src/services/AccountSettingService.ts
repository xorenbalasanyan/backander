import { accountSettingStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('AccountSettingService');

export default class AccountSettingService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, accountSettingStore);
	}
}
