import { Op } from 'sequelize';
import { shopDiscountStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('ShopDiscountService');

export default class ShopDiscountService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, shopDiscountStore);
	}

	findAllByNames = async names => this.findAll({ where: { name: { [Op.in]: names } } });
}
