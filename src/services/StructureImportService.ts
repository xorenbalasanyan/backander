import { Op } from 'sequelize';
import { structureImportStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { File, Log, User } from '../stores/db/entity';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('StructureImportService');

export default class StructureImportService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, structureImportStore);
	}

	addMeta = async (options, pageInfo) => {
		// нужно ли прикрепить пользователей к выгрузке
		if (pageInfo.meta.has('users')) {
			if (!options.include) options.include = [];
			// required позволяет делать LEFT JOIN вместо INNER JOIN
			options.include.push({ model: User, as: 'fk_owner', required: false });
		}
		// нужно ли прикрепить логи к выгрузке
		if (pageInfo.meta.has('logs')) {
			if (!options.include) options.include = [];
			// required позволяет делать LEFT JOIN вместо INNER JOIN
			options.include.push({
				model: Log,
				as: 'fk_log',
				attributes: ['id', 'message'],
				required: false,
			});
		}
		// нужно ли прикрепить файлы к выгрузке
		if (pageInfo.meta.has('files')) {
			if (!options.include) options.include = [];
			// required позволяет делать LEFT JOIN вместо INNER JOIN
			options.include.push({ model: File, as: 'fk_file', required: false });
		}
	}

	addWhereSearch = (options, { searchParts }) => {
		if (!searchParts?.length) return;
		const { where } = options;
		if (!where[Op.or]) where[Op.or] = [];
		// избавляемся от подзапросов, иначе не получится сделать поиск по полям
		options.subQuery = false;
		// для каждого поля добавляем поиск
		searchParts?.forEach(str => {
			where[Op.or].push({ id: { [Op.substring]: str } });
			where[Op.or].push({ '$fk_owner.lastName$': { [Op.substring]: str } });
			where[Op.or].push({ '$fk_owner.firstName$': { [Op.substring]: str } });
			where[Op.or].push({ '$fk_owner.middleName$': { [Op.substring]: str } });
		});
	}
}
