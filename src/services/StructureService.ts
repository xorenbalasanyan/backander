import util from 'util';
import fs from 'fs';
import { Transaction } from 'sequelize';
import { StructureImportStateEnum } from '~enums/StructureImportStatusEnum';
import ResponseError from '~errors/ResponseError';
import ValidationError from '~errors/ValidationError';
import { randomEasyPassword } from '~utils/random';
import { checkExcessKeys, validateIsPositiveNumber, validateString } from '~utils/validate';
import sequelize from '~lib/sequelize';
import {
	AuthCredentialService,
	FileService,
	RegionService,
	ShopManagerRoleService,
	ShopManagerService,
	ShopService,
	UserService,
} from '~services';
import { fileTypeStore } from '~stores';
import { File, Log, Shop, ShopManagerRole, StructureImport, User } from '../stores/db/entity';
import BaseService from './BaseService';
import { AppLogger } from '~lib/AppLogger';
import { UserRoles } from '~enums/UserRoleEnum';
import { fillError } from '~utils/errors';

const checkPN = validateIsPositiveNumber();
const checkIdString = validateString(1, 100);
const checkId = v => checkPN(v) && checkIdString(v);
const checkUsername = validateString(5, 100);
const checkNamePart = validateString(1, 100);

const USER_AVAILABLE_KEYS = ['user_id', 'role', 'username', 'first_name', 'last_name',
	'middle_name', 'phone', 'email'];
const SHOP_AVAILABLE_KEYS = ['shop_id', 'df_user_id', 'tm_user_id', 'upf_user_id', 'dm_user_id',
	'zdm_user_id', 'name', 'region', 'city', 'address', 'phones', 'emails'];

const moduleLogger = new AppLogger('StructureService');

export default class StructureService extends BaseService
{
	constructor(req) {
		super(req, moduleLogger);
	}

	importUsersAndShops = async ({ users, shops }) => {
		const currentUser = this.req.getAuthorizedUser();
		const log: any = {
			accountId: currentUser.accountId,
			userId: currentUser.id,
			type: 'STRUCTURE_IMPORT',
			ownerId: currentUser.id,
		};
		const structureImport: any = {
			accountId: currentUser.accountId,
			ownerId: currentUser.id,
		};
		const stats: any = { // будем собирать статистику
			createdUsers: [],
			updatedUsers: [],
			deletedUsers: [],
			createdShops: [],
			updatedShops: [],
			deletedShops: [],
			warns: [],
		};
		if (!users?.length && !shops?.length) {
			throw new ValidationError('В теле запроса должно быть заполнено хотя бы одно поле: users или shops');
		}
		// validate users
		if (users?.length) {
			users.forEach(u => {
				const excessKeys: any[] | false = checkExcessKeys(u, USER_AVAILABLE_KEYS);
				if (excessKeys && excessKeys.length) {
					throw new ValidationError(util.format(
						'У пользователя заданы избыточные поля %o в объекте %o',
						excessKeys,
						u));
				}
				if (checkId(u.user_id)) {
					throw new ValidationError(util.format(
						'У пользователя не указано или ошибочно заполнено поле user_id: %o',
						u.user_id));
				}
				if (u.role !== undefined) {
					if (!['DF', 'TM', 'UPF', 'DM', 'ZDM'].includes(u.role)) {
						throw new ValidationError(util.format(
							'У пользователя %o ошибочно заполнено поле role: %o',
							u.user_id,
							u.role));
					} else {
						u.role = UserRoles.MANAGER;
					}
				}
				if (u.username !== undefined) {
					if (checkUsername(u.username)) {
						throw new ValidationError(util.format(
							'У пользователя %o ошибочно заполнено поле username: %o',
							u.user_id,
							u.username));
					}
				}
				if (u.first_name !== undefined) {
					if (checkNamePart(u.first_name)) {
						throw new ValidationError(util.format(
							'У пользователя %o ошибочно заполнено поле first_name: %o',
							u.user_id,
							u.first_name));
					}
				}
				if (u.last_name !== undefined) {
					if (checkNamePart(u.last_name)) {
						throw new ValidationError(util.format(
							'У пользователя %o ошибочно заполнено поле last_name: %o',
							u.user_id,
							u.last_name));
					}
				}
				if (u.middle_name !== undefined) {
					if (checkNamePart(u.middle_name)) {
						throw new ValidationError(util.format(
							'У пользователя %o ошибочно заполнено поле middle_name: %o',
							u.user_id,
							u.middle_name));
					}
				}
				if (u.phone !== undefined) {
					if (checkNamePart(u.phone)) {
						throw new ValidationError(util.format(
							'У пользователя %o ошибочно заполнено поле phone: %o',
							u.user_id,
							u.phone));
					}
				}
				if (u.email !== undefined) {
					if (checkNamePart(u.email)) {
						throw new ValidationError(util.format(
							'У пользователя %o ошибочно заполнено поле email: %o',
							u.user_id,
							u.email));
					}
				}
			});
		}

		// validate shops
		if (shops?.length) {
			shops.forEach((s: any) => {
				const excessKeys: any[] | false = checkExcessKeys(s, SHOP_AVAILABLE_KEYS);
				if (excessKeys && excessKeys.length) {
					throw new ValidationError(util.format('У магазина заданы избыточные поля %o в объекте %o', excessKeys, s));
				}
				if (checkId(s.shop_id)) {
					throw new ValidationError(util.format('У магазина не указано или ошибочно заполнено поле shop_id: %o', s.shop_id));
				}
				if (s.df_user_id !== undefined) {
					if (checkId(s.df_user_id)) {
						throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле df_user_id: %o', s.shop_id, s.df_user_id));
					}
				}
				if (s.tm_user_id !== undefined) {
					if (checkId(s.tm_user_id)) {
						throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле tm_user_id: %o', s.shop_id, s.tm_user_id));
					}
				}
				if (s.upf_user_id !== undefined) {
					if (checkId(s.upf_user_id)) {
						throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле upf_user_id: %o', s.shop_id, s.upf_user_id));
					}
				}
				if (s.dm_user_id !== undefined) {
					if (checkId(s.dm_user_id)) {
						throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле dm_user_id: %o', s.shop_id, s.dm_user_id));
					}
				}
				if (s.zdm_user_id !== undefined) {
					if (checkId(s.zdm_user_id)) {
						throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле zdm_user_id: %o', s.shop_id, s.zdm_user_id));
					}
				}
				if (s.name !== undefined) {
					if (checkNamePart(s.name)) {
						throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле name: %o', s.shop_id, s.name));
					}
				}
				if (s.region !== undefined) {
					if (checkNamePart(s.region)) {
						throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле region: %o', s.shop_id, s.region));
					}
				}
				if (s.city !== undefined) {
					if (checkNamePart(s.city)) {
						throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле city: %o', s.shop_id, s.city));
					}
				}
				if (s.address !== undefined) {
					if (checkNamePart(s.address)) {
						throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле address: %o', s.shop_id, s.address));
					}
				}
				if (s.phones !== undefined) {
					if (s.phones.length) {
						s.phones = s.phones.filter(s1 => !!s1);
					}
					if (s.phones.length) {
						const bad = s.phones.find(i => checkNamePart(i));
						if (bad) {
							throw new ValidationError(util.format('Телефон для магазина %o ошибочно заполнен: %o', s.shop_id, bad));
						}
					}
				}
				if (s.emails !== undefined) {
					if (s.emails.length) {
						s.emails = s.emails.filter(s1 => !!s1);
					}
					if (s.emails.length) {
						const bad = s.emails.find(i => checkNamePart(i));
						if (bad) {
							throw new ValidationError(util.format('Email для магазина %o ошибочно заполнен: %o', s.shop_id, bad));
						}
					}
				}
			});
		}
		// сохраним входной файл и поместим его в архив
		const {
			fileName: rawFileName,
			fullPath: rawFilePath,
		} = FileService.generateNewFileName('', '.json');
		fs.writeFileSync(rawFilePath, JSON.stringify({ users, shops }));
		// zip
		const {
			fileName: name,
			pathName,
			fullPath,
		} = FileService.generateNewFileName('', '.zip');
		await FileService.zipOneFile(rawFileName, rawFilePath, fullPath);
		const size = fs.statSync(fullPath).size;
		// save file entity
		const zipFileType = await fileTypeStore.findIdByExt('zip');
		const savedFile: any = await File.create({
			name,
			size,
			fileTypeId: zipFileType.id,
			pathName,
			ownerId: this.req.getAuthorizedUser().id,
			accountId: this.req.getAuthorizedUser().accountId,
		});
		fs.unlinkSync(rawFilePath);
		structureImport.fileId = savedFile.id;

		return sequelize.transaction(async transaction => {
			// выполняем загрузку пользователей
			const userService = new UserService(this.req);
			const shopService = new ShopService(this.req);
			const authCredentialService = new AuthCredentialService(this.req);
			if (users?.length) {
				for (const user of users) {
					let existUser = await userService.findOneByExternalId(user.user_id, transaction);
					if (!existUser && user.username) {
						const existAuthCred = await authCredentialService
							.findOne({ where: { username: user.username } });
						if (existAuthCred) {
							existUser = await userService.findOneById(existAuthCred.userId);
						}
					}
					const update: any = {
						externalId: user.user_id,
					};
					// если пользователя еще нет, то проверяем обязательные поля
					if (!existUser) {
						['role', 'first_name', 'last_name']
							.forEach(key => {
								if (!user[key]) {
									throw ResponseError.validationInput(util.format(
										'Пользователь #%s не найден в базе. Чтобы создать нового пользователя, необходимо заполнить поле %o. '
										+ 'Транзакция не сохранена, измените и повторите запрос.',
										user.user_id,
										key,
									));
								}
							});
					}
					if (user.role) {
						const newRole = user.role.trim();
						if (existUser && existUser.role !== newRole) {
							// роль не может стать ниже чем была
							/*
							TODO xxx что тут происходит?
							if ((newRole === USER_ROLE_DM || newRole === USER_ROLE_ZDM) && SUPERVISOR_ROLES.includes(savedUser.role)) {
								// nothing
							} else if (newRole === USER_ROLE_UPF && (savedUser.role === USER_ROLE_TM || savedUser.role === USER_ROLE_DF)) {
								// nothing
							} else if (newRole === USER_ROLE_TM && savedUser.role === USER_ROLE_DF) {
								// nothing
							} else {
								update.role = newRole;
							}
							 */
							// если роль все-таки меняется, то для всех подчиненных пользователей и магазинов
							// нужно сменить привязки
							if (update.role) {
								let where;
								const newset: any = {};
								// if (savedUser.role === USER_ROLE_UPF) { TODO xxx
								if (existUser.role === UserRoles.MANAGER) {
									where = { upfId: existUser.id };
									newset.upfId = null;
									// } else if (savedUser.role === USER_ROLE_TM) { TODO xxx
								} else if (existUser.role === UserRoles.SUPPORT) {
									where = { tmId: existUser.id };
									newset.tmId = null;
								}
								// if (newRole === USER_ROLE_UPF) { TODO xxx
								if (newRole === UserRoles.MANAGER) {
									update.upfId = null;
									// } else if (newRole === USER_ROLE_TM) { TODO xxx
								} else if (newRole === UserRoles.SUPPORT) {
									update.upfId = null;
									update.tmId = null;
									newset.tmId = existUser.id;
								// } else if (newRole === USER_ROLE_DF) { TODO xxx
								} else if (newRole === UserRoles.CHIEF) {
									update.upfId = null;
									update.tmId = null;
									update.dfId = null;
									newset.dfId = existUser.id;
								}
								if (where) {
									// ищем и обновляем все починенные магазины
									const shops1 = await shopService.findAll({ where }, transaction);
									await Promise.all(shops1.map(shop => shopService.updateOne(shop, newset, transaction)));
									// ищем и обновляем всех подчиненных пользвоателей
									const subusers = await userService.findAll({ where }, transaction);
									await Promise.all(subusers.map(subuser => userService.updateOne(subuser, newset, transaction)));
								}
							}
						} else {
							update.role = newRole;
						}
					}
					if (user.first_name) update.firstName = user.first_name.trim();
					if (user.last_name) update.lastName = user.last_name.trim();
					if (user.middle_name) update.middleName = user.middle_name.trim() || null;
					if (user.username) {
						// это должен быть уникальный username
						const newUsername = user.username.trim();
						const existAuthCred = await authCredentialService
							.findOne({ where: { username: user.username } }, transaction);
						// логин не должен повторяться
						// поэтому нужно выполнить несколько проверок на уже созданного пользователя с таким
						// же логином
						const isBad = existAuthCred && (!existUser || existUser.id !== existAuthCred.userId);
						if (isBad) {
							throw ResponseError.validationInput(util.format(
								'Пользователь с логином \'%s\' уже есть в базе и это другой пользователь текущего аккаунта.'
								+ ' Транзакция не сохранена, измените и повторите запрос.',
								user.username,
							));
						}
						update.username = newUsername;
					}
					if (user.phone) update.phone = user.phone.trim() || null;
					if (user.email) update.email = user.email.trim() || null;
					if (existUser) {
						await userService.updateOne(existUser, {
							...update,
							moderatorId: currentUser.id,
						}, transaction);
						// update username
						if (update.username) {
							const existAuthCred = await authCredentialService.findOne({ where: { userId: existUser.id } }, transaction);
							if (existAuthCred) {
								// update
								await authCredentialService.updateOne(existAuthCred, {
									username: update.username,
									moderatorId: currentUser.id,
								}, transaction);
							} else {
								// create
								await authCredentialService.createOne({
									accountId: currentUser.accountId,
									userId: user.id,
									username: update.username,
									ownerId: currentUser.id,
								}, transaction);
							}
						}
						stats.updatedUsers.push(existUser.id);
					} else {
						const newUser = await userService.createOne({
							...update,
							accountId: currentUser.accountId,
							ownerId: currentUser.id,
							key: randomEasyPassword(8, 10),
						}, transaction);
						// create auth cred
						await authCredentialService.createOne({
							accountId: currentUser.accountId,
							userId: user.id,
							username: update.username,
							ownerId: currentUser.id,
						}, transaction);
						stats.createdUsers.push(newUser.id);
					}
				}
			}
			// выполняем загрузку магазинов
			const regionService = new RegionService(this.req);
			const shopManagerService = new ShopManagerService(this.req);
			const shopManagerRoleService = new ShopManagerRoleService(this.req);
			const roles = await shopManagerRoleService.findAllWithAccountId({ where: {} }, transaction);
			if (shops?.length) {
				for (const shop of shops) {
					let savedShop = (await Shop.unscoped().findAll({
						where: {
							accountId: currentUser.accountId,
							externalId: shop.shop_id,
						},
						transaction,
					}))[0];
					const update: any = {
						externalId: shop.shop_id,
					};
					// если магазина еще нет, то проверяем обязательные поля
					if (!savedShop) {
						['name', 'region', 'city', 'address']
							.forEach(key => {
								if (!shop[key]) {
									throw ResponseError.validationInput(util.format(
										'Магазин #%s не найден в базе. Чтобы новый магазин появился в базе,'
										+ ' необходимо заполнить поле %o. Транзакция не сохранена, измените и повторите запрос.',
										shop.shop_id,
										key,
									));
								}
							});
						// для нового магазина должно быть указано одно из следующих полей
						const keys = ['upf_user_id', 'tm_user_id', 'df_user_id'];
						const count = keys.map(key => shop[key]).filter(i => !!i).length;
						if (count !== 1) {
							throw ResponseError.validationInput(util.format(
								'Магазин #%s не найден в базе. Чтобы новый магазин появился в базе,'
								+ ' необходимо заполнить только одно из полей %s. Транзакция не сохранена, измените и повторите запрос.',
								shop.shop_id,
								`'${keys.join('\', \'')}'`,
							));
						}
					}
					// проверка ДФ
					if (shop.df_user_id) {
						const dfUser = await userService.findOneByExternalId(shop.df_user_id, transaction);
						if (!dfUser || dfUser.role !== UserRoles.MANAGER) {
							throw ResponseError.notFound(util.format(
								'Магазин #%s не удалось обновить, потому что ДФ #%s не найден в базе. '
								+ 'Транзакция не сохранена, измените и повторите запрос.',
								shop.shop_id,
								shop.df_user_id,
							));
						}
						update.dfId = dfUser.id;
					}
					// проверка ТМ
					if (shop.tm_user_id) {
						const tmUser = await userService.findOneByExternalId(shop.tm_user_id, transaction);
						if (!tmUser || tmUser.role !== UserRoles.MANAGER) {
							throw ResponseError.notFound(util.format(
								'Магазин #%s не удалось обновить, потому что ТМ #%s не найден в базе. '
								+ 'Транзакция не сохранена, измените и повторите запрос.',
								shop.shop_id,
								shop.tm_user_id,
							));
						}
						update.tmId = tmUser.id;
					}
					// проверка УПФ
					if (shop.upf_user_id) {
						const upfUser = await userService.findOneByExternalId(shop.upf_user_id, transaction);
						if (!upfUser || upfUser.role !== UserRoles.MANAGER) {
							throw ResponseError.notFound(util.format(
								'Магазин #%s не удалось обновить, потому что УПФ #%s не найден в базе. '
								+ 'Транзакция не сохранена, измените и повторите запрос.',
								shop.shop_id,
								shop.upf_user_id,
							));
						}
						update.upfId = upfUser.id;
					}
					// проверка ДМ
					if (shop.dm_user_id) {
						const dmUser = await userService.findOneByExternalId(shop.dm_user_id, transaction);
						if (!dmUser || dmUser.role !== UserRoles.MANAGER) {
							throw ResponseError.notFound(util.format(
								'Магазин #%s не удалось обновить, потому что ДМ #%s не найден в базе. '
								+ 'Транзакция не сохранена, измените и повторите запрос.',
								shop.shop_id,
								shop.dm_user_id,
							));
						}
						update.dmId = dmUser.id;
					}
					// проверка ЗДМ
					if (shop.zdm_user_id) {
						const zdmUser = await userService.findOneByExternalId(shop.zdm_user_id, transaction);
						if (!zdmUser || zdmUser.role !== UserRoles.MANAGER) {
							throw ResponseError.notFound(util.format(
								'Магазин #%s не удалось обновить, потому что ЗДМ #%s не найден в базе. '
								+ 'Транзакция не сохранена, измените и повторите запрос.',
								shop.shop_id,
								shop.zdm_user_id,
							));
						}
						update.zdmId = zdmUser.id;
					}
					if (shop.name) update.name = shop.name.trim();
					if (shop.region) {
						let savedRegion = await regionService.findOneByName(shop.region.trim(), transaction);
						if (!savedRegion) {
							// создаем новый
							savedRegion = await regionService.createOne({
								accountId: currentUser.accountId,
								name: shop.region.trim(),
								ownerId: currentUser.id,
							}, transaction);
						}
						update.regionId = savedRegion.id;
					}
					if (shop.city) update.city = shop.city.trim();
					if (shop.address) update.address = shop.address.trim();
					if (shop.phones?.length) update.phone = shop.phones.map(s => s?.trim()).filter(s => !!s).join(', ');
					if (shop.emails?.length) update.email = shop.emails.map(s => s?.trim()).filter(s => !!s).join(', ');
					if (savedShop) {
						// update shop
						await shopService.updateOne(savedShop, {
							...update,
							moderatorId: currentUser.id,
						}, transaction);
						stats.updatedShops.push(savedShop.id);
					} else {
						// create shop
						savedShop = await shopService.createOne({
							...update,
							accountId: currentUser.accountId,
							ownerId: currentUser.id,
							timeOffset: 300,
						}, transaction);
						stats.createdShops.push(savedShop.id);
					}
					// update shop managers
					if (update.dfId) {
						const role = roles.find(i => i.engShortTitle === 'DF');
						await checkAndUpdateShopManagerRole(shopManagerService, savedShop, transaction, role, currentUser, update.dfId);
					}
					if (update.tmId) {
						const role = roles.find(i => i.engShortTitle === 'TM');
						await checkAndUpdateShopManagerRole(shopManagerService, savedShop, transaction, role, currentUser, update.tmId);
					}
					if (update.upfId) {
						const role = roles.find(i => i.engShortTitle === 'UPF');
						await checkAndUpdateShopManagerRole(shopManagerService, savedShop, transaction, role, currentUser, update.upfId);
					}
					if (update.dmId) {
						const role = roles.find(i => i.engShortTitle === 'DM');
						await checkAndUpdateShopManagerRole(shopManagerService, savedShop, transaction, role, currentUser, update.dmId);
					}
					if (update.zdmId) {
						const role = roles.find(i => i.engShortTitle === 'ZDM');
						await checkAndUpdateShopManagerRole(shopManagerService, savedShop, transaction, role, currentUser, update.zdmId);
					}
				}
			}
			// запоминаем статистику
			Object.assign(log, {
				message: 'Обновление структуры выполнено',
				reason: 'OK',
			});
			structureImport.status = StructureImportStateEnum.OK;
		}) // transaction
			.catch(error => {
				this.logger.error(fillError(Error, '[Ошибка импорта структуры]', error));
				Object.assign(log, {
					message: String(error),
					reason: 'ERROR',
				});
				structureImport.status = StructureImportStateEnum.ERROR;
			})
			.finally(async () => {
				// если есть текст в объекте лога, то сохраняем полную связку
				if (log.message) {
					const newLog: any = await Log.create(log);
					Object.assign(structureImport, {
						logId: newLog.id,
						createdUsersCount: stats.createdUsers.length,
						updatedUsersCount: stats.updatedUsers.length,
						deletedUsersCount: stats.deletedUsers.length,
						createdShopsCount: stats.createdShops.length,
						updatedShopsCount: stats.updatedShops.length,
						deletedShopsCount: stats.deletedShops.length,
						warnCount: stats.warns.length,
					});
					await StructureImport.create(structureImport);
				}
				if (log.reason === 'ERROR') {
					throw ResponseError.validationInput(log.message);
				}
			});
	}
}

/**
 * Проверяет и обновляет менеджера в магазине
 * @param shopManagerService
 * @param savedShop
 * @param transaction
 * @param role
 * @param currentUser
 * @param managerUserId
 */
async function checkAndUpdateShopManagerRole(
	shopManagerService: ShopManagerService,
	savedShop: typeof Shop,
	transaction: Transaction,
	role: typeof ShopManagerRole,
	currentUser: typeof User,
	managerUserId: number,
) {
	const sm = await shopManagerService.findOneWithoutAccountId({
		where: {
			shopId: savedShop.id,
			userId: managerUserId,
		},
	}, transaction);
	if (sm && sm.roleId !== role.id) {
		await shopManagerService.updateOne(sm, {
			roleId: role.id,
			moderatorId: currentUser.id,
		}, transaction);
	} else if (!sm) {
		await shopManagerService.createOne({
			shopId: savedShop.id,
			userId: managerUserId,
			roleId: role.id,
			ownerId: currentUser.id,
		}, transaction);
	}
}
