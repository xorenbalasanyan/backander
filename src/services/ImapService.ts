import Imap, { ImapMessageAttributes } from 'imap';

type MessageBufferInfo = { buffer: string, attrs: ImapMessageAttributes };

export class ImapService extends Imap
{
	constructor(connectOptions) {
		super(connectOptions);
	}

	private handle1 = (resolve, reject) => error => {
		if (error) {
			reject(error);
		} else {
			resolve(undefined);
		}
	}

	private handle2 = (resolve, reject) => (error, results) => {
		if (error) {
			reject(error);
		} else {
			resolve(results);
		}
	}

	openInbox() {
		return new Promise((resolve, reject) => {
			this.openBox('INBOX', false, this.handle2(resolve, reject));
		});
	}

	/**
	 * Поиск сообщений в текущем открытом ящике.
	 * @param {any[]} criteria
	 * @returns {Promise<number[]>}
	 */
	searchMessages(criteria: any[]): Promise<number[]> {
		return new Promise((resolve, reject) => {
			this.search(criteria, this.handle2(resolve, reject));
		});
	}

	fetchMessages = (source: string | string[] | number[], bodies: string = '') => {
		const f = this.seq.fetch(source, { bodies });
		f.on('message', (...args) => {
			this.emit(ImapService.Event.FetchMessage, ...args);
		});
		f.once('error', error => {
			this.emit(ImapService.Event.FetchError, error);
			this.emit(ImapService.Event.Error, error);
		});
		f.once('end', () => {
			this.emit(ImapService.Event.FetchEnd);
		});
	}

	readMessageBuffer(messageEvent): Promise<MessageBufferInfo> {
		return new Promise(resolve => {
			let buffer = '';
			let attrs;
			messageEvent.on('body', stream => stream.on('data', chunk => buffer += chunk.toString('utf8')));
			messageEvent.on('attributes', attrs1 => attrs = attrs1);
			messageEvent.once('end', () => resolve({ buffer, attrs }));
		});
	}

	moveToBox(muid, boxName): Promise<any> {
		return new Promise((resolve, reject) => {
			this.move(muid, boxName, this.handle1(resolve, reject));
		});
	}

	moveToTrash = muid => this.moveToBox(muid, 'Trash');
}

// tslint:disable-next-line:no-namespace
export namespace ImapService {
	export enum Event {
		Error = 'error',
		FetchMessage = 'fetchMessage',
		FetchError = 'fetchError',
		FetchEnd = 'fetchEnd',
	}
}
