import { Transaction } from 'sequelize';
import { todoItemStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError from '~errors/ResponseError';
import { TodoCreateWarnings } from '~utils/TodoCreateWarnings';
import { checkSqlDate } from '~utils/date';
import { arrayToSetThenToArrayByField, distinctListByField, distinctListById, mappingArrayByFields } from '~utils/list';
import { validateString } from '~utils/validate';
import { ShopManagerRoleService, ShopManagerService, ShopService, UserService } from './index';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('TodoItemService');

export default class TodoItemService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, todoItemStore);
	}

	/**
	 * Создание новых туду.
	 * @param date
	 * @param todos
	 * @returns {Promise<void>}
	 */
	createItems = async (date, todos) => {
		if (!date || !checkSqlDate(date)) {
			throw ResponseError.validationInput('Дата не соответствует формату YYYY-MM-DD');
		}
		if (!todos?.length) {
			throw ResponseError.validationInput('Список дел не должен быть пустой');
		}
		const warnings = new TodoCreateWarnings();
		// проверка уже созданных задач
		todos.forEach(i => i.todo_id = String(i.todo_id));
		const todoExIds = todos.map(i => i.todo_id);
		const existTodos = await this.findAll({
			where: { externalId: todoExIds },
			attributes: ['externalId'],
		});
		const existTodoExIds = new Set(existTodos?.map(i => i.externalId) || []);
		// селектим пользователей
		const userService = new UserService(this.req);
		const userExIds = todos.map(i => i.user_id).filter(i => !!i);
		const existUsers = await userService.findAll({
			where: { externalId: userExIds },
			attributes: ['id', 'externalId'],
			raw: true,
		});
		const existUserMap = mappingArrayByFields(existUsers, 'externalId', 'id');
		// селектим магазины
		const shopExIds = Array.from(new Set(todos.map(i => i.shop_id).filter(Boolean)));
		const existShops = await new ShopService(this.req).findAll({
			where: { externalId: shopExIds },
			attributes: ['id', 'externalId'],
			raw: true,
		});
		const existShopsIds = arrayToSetThenToArrayByField(existShops, 'id');
		// дополнительно селектим ДМ и ЗДМ для этих магазинов
		const shopManagerService = new ShopManagerService(this.req);
		const shopManagerRoleService = new ShopManagerRoleService(this.req);
		const shopUserRoles = await shopManagerRoleService.getShopUserRoles();
		const shopUserRoleIds = distinctListById(shopUserRoles);
		const existDmsManagers = await shopManagerService.findAll({
			where: {
				shopId: existShopsIds,
				roleId: shopUserRoleIds,
			},
			attributes: ['userId', 'shopId'],
		});
		const shopIdToDmsIdsMap = existShops.reduce((map, shop) => {
			const dmIds = existDmsManagers.filter(dm => dm.shopId === shop.id).map(i => i.userId);
			return map.set(shop.externalId, dmIds);
		}, new Map());
		// проверка и подготовка новых задач
		todos = todos.reduce((list, {
			todo_id,
			user_id,
			shop_id,
			title,
			description,
			status,
		}, index) => {
			user_id = user_id ? String(user_id) : undefined;
			shop_id = shop_id ? String(shop_id) : undefined;
			if (existTodoExIds.has(todo_id)) {
				warnings.addExistTodoExIds(todo_id);
			} else if (!title) {
				warnings.addEmptyTitles(index);
			} else if (!status?.length) {
				warnings.addWrongStatuses(index);
			} else if (user_id && !existUserMap.has(user_id)) {
				warnings.addNonExistUserExIds(user_id);
			} else if (shop_id && !shopIdToDmsIdsMap.has(shop_id)) {
				warnings.addNonExistShopExIds(shop_id);
			} else {
				const data = {
					externalId: todo_id,
					date,
					status,
					title,
					description,
				};
				if (user_id) {
					list.push({
						...data,
						addresseeId: existUserMap.get(user_id),
					});
				}
				if (shop_id) {
					Array.from(shopIdToDmsIdsMap.get(shop_id)).forEach(userId => {
						list.push({
							...data,
							addresseeId: userId,
						});
					});
				}
			}
			return list;
		}, []);
		// тут можно без транзакции
		await this.createAll(todos);
		return warnings.length > 0 ? { warnings: warnings.getLines() } : undefined;
	}

	/**
	 * Обновление списка туду
	 * @param todoItems {any[]}
	 * @param todoItemData {any}
	 * @param transaction
	 * @returns {Promise<void>}
	 */
	updateTodos = async (todoItems: any[], todoItemData: any, transaction?: Transaction): Promise<any[]> => {
		const newData = checkUpdateTodoItemData(todoItemData);
		if (!Object.keys(newData).length) {
			throw ResponseError.validationInput('Необходимо указать хотя бы одно поле для обновления записи');
		}
		return this.updateAll(todoItems, newData, transaction);
	}

	updateItemsByDate = async (date: string, todoItems: any[]) => {
		// проверка данных
		const todoIdMap = new Map<string, object>();
		todoItems.forEach((todoItem, index) => {
			const { todo_id, title, description, status } = todoItem;
			if (!todo_id) {
				throw ResponseError.validationInput(`Для записи #${index} нужно передать значение для поля todo_id. Запрос не выполнен.`);
			}
			if (!title && description === undefined && !status) {
				throw ResponseError.validationInput(`В записи #${index} нет данных для обновления. Запрос не выполнен.`);
			}
			if (title) {
				const check = validateString(1, 100)(title);
				if (check) {
					throw ResponseError.validationInput(`В записи #${index} значение в поле title: ${check}. Запрос не выполнен.`);
				}
			}
			if (description) {
				const check = validateString(1, 2000)(description);
				if (check) {
					throw ResponseError.validationInput(`В записи #${index} значение в поле description: ${check}. Запрос не выполнен.`);
				}
			}
			if (status) {
				const check = validateString(1, 50)(status);
				if (check) {
					throw ResponseError.validationInput(`В записи #${index} значение в поле status: ${check}`);
				}
			}
			todoIdMap.set(String(todo_id), todoItem);
		});
		// загружаем текущие задачи с указанными номерами
		let caughtError: any = false;
		const externalIds = Array.from(todoIdMap.keys());
		const existTodoItems = await this
			.findAll({
				where: {
					externalId: externalIds,
					date,
				},
			})
			.catch(error => caughtError = error);
		if (caughtError) throw caughtError;
		// обновляем задачи
		for (const todoItem of existTodoItems) {
			const newData: any = {};
			const index = String(todoItem.externalId);
			const data: any = todoIdMap.get(index);
			if (!data) {
				throw new Error(`Индексы не совпадают. В списке импортных задач не найдена запись с externalId ${index}`);
			}
			todoIdMap.delete(index);
			if (data.title) newData.title = data.title;
			if (data.description !== undefined) newData.description = data.description;
			if (data.status !== undefined) newData.status = data.status;
			await this.updateOne(todoItem, newData)
				.catch(error => caughtError = error);
			if (caughtError) throw caughtError;
		}
		// проверяем что осталось
		if (todoIdMap.size) {
			this.logger.warn(`При обновлении списка TodoItem не все задачи из запроса были обновлены. Не удалось найти в БД записи для следующих externalId: ${Array.from(todoIdMap.keys()).join(', ')}`);
		}
	}
}

function checkUpdateTodoItemData(todoItemData) {
	const newData: any = {};
	if (todoItemData?.title) {
		newData.title = todoItemData.title;
		const warn = validateString(1, 2000)(newData.title);
		if (warn) {
			throw ResponseError.validationInput('TodoItem.title: ' + warn);
		}
	}
	if (todoItemData?.status) {
		newData.status = todoItemData.status;
	}
	if (todoItemData?.description) {
		newData.description = todoItemData.description;
		const warn = validateString(1, 2000)(newData.description);
		if (!newData.description && warn) {
			throw ResponseError.validationInput('TodoItem.title: ' + warn);
		}
	}
	return newData;
}

