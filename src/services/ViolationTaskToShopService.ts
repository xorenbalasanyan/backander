import fs from 'fs';
import util from 'util';
import { Op } from 'sequelize';
import { ViolationTaskStatusEnum } from '~enums/ViolationTaskStatusEnum';
import ResponseError, { NOT_ALL_REQUIRED_FIELDS } from '~errors/ResponseError';
import { copyFields } from '~utils/copyFields';
import { validateIsPositiveNumber, validateString } from '~utils/validate';
import sequelize from '~lib/sequelize';
import { FileService } from '~services';
import { fileTypeStore, violationTaskToShopStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { File } from '../stores/db/entity';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('ViolationTaskToShopService');

const validateOneViolationTaskToShopExecuteData = violationTaskToShopData => {
	const availableKeys = ['executionComment', 'executionPhotos'];
	const data = copyFields(violationTaskToShopData, {}, availableKeys);
	if (!data) {
		throw NOT_ALL_REQUIRED_FIELDS;
	}
	if (validateString(5, 2000)(data.executionComment)) {
		throw ResponseError.validationInput('Поле executionComment должно быть строкой длиной от 5 до 2000');
	}
	if (!data.executionPhotos?.length) {
		throw ResponseError.validationInput('Поле executionPhotos должно быть массивом и содержать значения');
	}
	data.executionPhotos.forEach(({ isNew, data: photoData, id, isDeleted }) => {
		if (isDeleted && isNew) {
			throw ResponseError.validationInput('Информация о фотографии не может быть new и deleted одновременно');
		}
		if (isDeleted) {
			if (!id) {
				throw ResponseError.validationInput('Если указан флаг isDeleted, то должен быть указан значение для id');
			}
			if (validateIsPositiveNumber()(id)) {
				throw ResponseError.validationInput('Поле id для удаления фотографии должно быть положительным числом');
			}
		}
		if (isNew) {
			if (!/^data:image\/(png|jpg|jpeg|gif);base64,/.test(photoData)) {
				throw ResponseError.validationInput('Поле data фотографии должно содержать данные картинки в кодировке base64. '
					+ 'Поддерживаемые mime-типы: image/png, image/jpg, image/jpeg, image/gif.');
			}
		}
	});
};

const validateOneViolationTaskToShopRejectData = violationTaskToShopData => {
	const availableKeys = ['rejectionComment'];
	const data = copyFields(violationTaskToShopData, {}, availableKeys);
	if (!data) {
		throw NOT_ALL_REQUIRED_FIELDS;
	}
	if (validateString(5, 2000)(data.rejectionComment)) {
		throw ResponseError.validationInput('Поле rejectionComment должно быть строкой длиной от 5 до 2000');
	}
};

export default class ViolationTaskToShopService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, violationTaskToShopStore);
	}

	executeOne = async (violationTaskToShopId, violationTaskToShopData) => {
		validateOneViolationTaskToShopExecuteData(violationTaskToShopData);
		this.logger.http(util.format('Сохранение решения ViolationTaskToShop с данными: %o', violationTaskToShopData));
		const oldViolationTaskToShop = await this.findOneByIdWithoutAccountId(violationTaskToShopId);

		const currentUser = this.req.getAuthorizedUser();
		return sequelize.transaction(async transaction => {
			const { photos = [] } = JSON.parse(oldViolationTaskToShop.executionInfo || '{}');
			const filesToDelete: number[] = [];
			// сначала проверяем и удаляем старые фото
			violationTaskToShopData.executionPhotos.filter(p => p.isDeleted)
				.forEach(({ id: photoFileId }) => {
					const index = photos.indexOf(photoFileId);
					if (index === -1) {
						throw ResponseError.validationInput('Фотография %d не существует в нарушении %d', photoFileId, violationTaskToShopId);
					}
					filesToDelete.push(photoFileId);
					photos.splice(index, 1);
				});
			// удаляем ссылки на файлы
			if (filesToDelete.length) {
				await File.destroy({ where: { id: { [Op.in]: filesToDelete } } }, { transaction });
			}
			// теперь добавляем новые фото
			await Promise.all(violationTaskToShopData.executionPhotos.filter(p => p.isNew).map(async ({ data: photoData }) => {
				const [found, mimeType, imageFileExt] = photoData.match('^data:(image/(png|jpg|jpeg|gif));base64,');
				const data = Buffer.from(photoData.substr(found.length), 'base64');
				const imageFileType = await fileTypeStore.findIdByMimeType(mimeType, transaction);
				// сохраняем картинку в файл
				const {
					fileName: name,
					pathName,
					fullPath,
				} = FileService.generateNewFileName('', `.${imageFileExt}`);
				fs.writeFileSync(fullPath, data);
				const size = fs.statSync(fullPath).size;
				const file: any = await File.create({
					name,
					size,
					fileTypeId: imageFileType.id,
					pathName,
					ownerId: currentUser.id,
					accountId: currentUser.accountId,
				}, { transaction });
				photos.push(file.id);
			}));
			// сохраняем изменения
			return this.store.updateOne(oldViolationTaskToShop, {
				status: ViolationTaskStatusEnum.DONE,
				executor: currentUser,
				executionComment: violationTaskToShopData.executionComment.trim(),
				executionInfo: JSON.stringify({ photos }),
				executedAt: new Date(),
			}, transaction);
		});
	}

	rejectOne = async (violationTaskToShopId, violationTaskToShopData) => {
		validateOneViolationTaskToShopRejectData(violationTaskToShopData);
		this.logger.http(util.format('Отклонение ViolationTaskToShop с данными: %o', violationTaskToShopData));
		const oldViolationTaskToShop = await this.findOneByIdWithoutAccountId(violationTaskToShopId);
		const currentUser = this.req.getAuthorizedUser();
		// сохраняем изменения
		return sequelize.transaction(async transaction => this.store.updateOne(oldViolationTaskToShop, {
			status: ViolationTaskStatusEnum.REJECTED,
			rejector: currentUser,
			rejectionComment: violationTaskToShopData.rejectionComment.trim(),
			rejectedAt: new Date(),
		}, transaction));
	}
}
