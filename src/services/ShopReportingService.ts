import moment from 'moment';
import { ShopReportingType } from '~enums/ShopReportingType';
import ResponseError from '~errors/ResponseError';
import { validateInteger } from '~utils/validate';
import { shopReportingStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';
import { ShopService } from './index';

const moduleLogger = new AppLogger('ShopReportingService');

export default class ShopReportingService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, shopReportingStore);
	}

	findEmployeeCountReportForCurrentUser = async (user) => {
		const shopService = new ShopService(this.req);
		const shops = await shopService.findAllWhereUserIsShopUser(user.id);
		// проверяем пользователя и получаем для него магазин
		if (!shops.length) {
			throw ResponseError.accessDenied('У пользователя нет доступа к отчетам');
		}
		const shop = shops[0]; // FIXME может быть несколько магазинов
		// для магазина ищем репорт за текущий день
		const shopToday = moment().utcOffset(shop.timeOffset).format('YYYY-MM-DD');
		return shopReportingStore.findOne({
			where: {
				shopId: shop.id,
				date: shopToday,
				reportType: ShopReportingType.EMPLOYEE_COUNT,
			},
		});
	}

	findTemperatureCheckReportForCurrentUser = async (user) => {
		const shopService = new ShopService(this.req);
		const shops = await shopService.findAllWhereUserIsShopUser(user.id);
		// проверяем пользователя и получаем для него магазин
		if (!shops.length) {
			throw ResponseError.accessDenied('У пользователя нет доступа к отчетам');
		}
		const shop = shops[0]; // FIXME может быть несколько магазинов
		// для магазина ищем репорт за текущий день
		const shopToday = moment().utcOffset(shop.timeOffset).format('YYYY-MM-DD');
		return shopReportingStore.findOne({
			where: {
				shopId: shop.id,
				date: shopToday,
				reportType: ShopReportingType.TEMPERATURE_CHECK,
			},
		});
	}

	findProblemCheckReportForCurrentUser = async (user) => {
		const shopService = new ShopService(this.req);
		const shops = await shopService.findAllWhereUserIsShopUser(user.id);
		// проверяем пользователя и получаем для него магазин
		if (!shops.length) {
			throw ResponseError.accessDenied('У пользователя нет доступа к отчетам');
		}
		const shop = shops[0]; // FIXME может быть несколько магазинов
		// для магазина ищем репорт за текущий день
		const shopToday = moment().utcOffset(shop.timeOffset).format('YYYY-MM-DD');
		return shopReportingStore.findOne({
			where: {
				shopId: shop.id,
				date: shopToday,
				reportType: ShopReportingType.PROBLEM_CHECK,
			},
		});
	}

	findPersonnelCheckReportForCurrentUser = async (user) => {
		const shopService = new ShopService(this.req);
		const shops = await shopService.findAllWhereUserIsShopUser(user.id);
		// проверяем пользователя и получаем для него магазин
		if (!shops.length) {
			throw ResponseError.accessDenied('У пользователя нет доступа к отчетам');
		}
		const shop = shops[0]; // FIXME может быть несколько магазинов
		// для магазина ищем репорт за текущий день
		const shopToday = moment().utcOffset(shop.timeOffset).format('YYYY-MM-DD');
		return shopReportingStore.findOne({
			where: {
				shopId: shop.id,
				date: shopToday,
				reportType: ShopReportingType.PERSONNEL_CHECK,
			},
		});
	}

	findCameraCheckReportForCurrentUser = async (user) => {
		const shopService = new ShopService(this.req);
		const shops = await shopService.findAllWhereUserIsShopUser(user.id);
		// проверяем пользователя и получаем для него магазин
		if (!shops.length) {
			throw ResponseError.accessDenied('У пользователя нет доступа к отчетам');
		}
		const shop = shops[0]; // FIXME может быть несколько магазинов
		// для магазина ищем репорт за текущий день
		const shopToday = moment().utcOffset(shop.timeOffset).format('YYYY-MM-DD');
		return shopReportingStore.findOne({
			where: {
				shopId: shop.id,
				date: shopToday,
				reportType: ShopReportingType.CAMERA_CHECK,
			},
		});
	}

	updateEmployeeCountReportForCurrentUser = async (user, report) => {
		// проверка репорта
		const value = report?.employeeCount;
		const validateWarn = validateInteger(0, 50)(value);
		if (validateWarn) {
			throw ResponseError.validationInput(`Ошибка проверки отчета: ${validateWarn}`);
		}
		const shopService = new ShopService(this.req);
		const shops = await shopService.findAllWhereUserIsShopUser(user.id);
		// проверяем пользователя и получаем для него магазин
		if (!shops.length) {
			throw ResponseError.accessDenied('У пользователя нет доступа к отчетам');
		}
		const shop = shops[0]; // FIXME может быть несколько магазинов
		// проверка времени магазина, очтет можно отправлять до 12:00
		const shopDate = moment().utcOffset(shop.timeOffset);
		if (shopDate.hour() > 11) {
			throw ResponseError.validationInput(`Данный вид отчета можно отправлять до 12:00`);
		}
		// для магазина ищем репорт за текущий день
		const shopToday = shopDate.format('YYYY-MM-DD');
		const shopReport = await shopReportingStore.findOne({
			where: {
				shopId: shop.id,
				date: shopToday,
				reportType: ShopReportingType.EMPLOYEE_COUNT,
			},
		});
		if (!shopReport) {
			return shopReportingStore.createOne({
				accountId: user.accountId,
				shop,
				reportType: ShopReportingType.EMPLOYEE_COUNT,
				date: shopToday,
				value,
				ownerId: user.id,
			});
		} else {
			if (shopReport.isReported) {
				throw ResponseError.validationInput(`Отчет для магазина уже отправлен`);
			}
			return shopReportingStore.updateOne(shopReport, {
				value,
				moderatorId: user.id,
			});
		}
	}

	updateTemperatureCheckReportForCurrentUser = async (user, report) => {
		// проверка репорта
		const { temp1, temp3 } = report || {};
		const warn1 = validateInteger(-50, 50)(temp1);
		const warn3 = validateInteger(0, 1)(temp3);
		const validateWarn = warn1 || warn3;
		if (validateWarn) {
			throw ResponseError.validationInput(`Ошибка проверки отчета: ${validateWarn}`);
		}
		const shopService = new ShopService(this.req);
		const shops = await shopService.findAllWhereUserIsShopUser(user.id);
		// проверяем пользователя и получаем для него магазин
		if (!shops.length) {
			throw ResponseError.accessDenied('У пользователя нет доступа к отчетам');
		}
		const shop = shops[0]; // FIXME может быть несколько магазинов
		// проверка времени магазина, очтет можно отправлять до 18:00
		const shopDate = moment().utcOffset(shop.timeOffset);
		if (shopDate.hour() > 17) {
			throw ResponseError.validationInput(`Данный вид отчета можно отправлять до 18:00`);
		}
		// для магазина ищем репорт за текущий день
		const shopToday = shopDate.format('YYYY-MM-DD');
		const shopReport = await shopReportingStore.findOne({
			where: {
				shopId: shop.id,
				date: shopToday,
				reportType: ShopReportingType.TEMPERATURE_CHECK,
			},
		});
		if (!shopReport) {
			return shopReportingStore.createOne({
				accountId: user.accountId,
				shop,
				reportType: ShopReportingType.TEMPERATURE_CHECK,
				date: shopToday,
				value: JSON.stringify({ temp1, temp3 }),
				ownerId: user.id,
			});
		} else {
			if (shopReport.isReported) {
				throw ResponseError.validationInput(`Отчет для магазина уже отправлен`);
			}
			return shopReportingStore.updateOne(shopReport, {
				value: JSON.stringify({ temp1, temp3 }),
				moderatorId: user.id,
			});
		}
	}

	updateProblemCheckReportForCurrentUser = async (user, report) => {
		// проверка репорта
		const { radio1, radio2 } = report || {};
		const good1 = radio1 >= 1 && radio1 <= 2;
		const good2 = radio1 !== 1 || (radio2 >= 1 && radio2 <= 4);
		const warn = !good1 || !good2;
		if (warn) {
			throw ResponseError.validationInput(`Ошибка проверки отчета`);
		}
		const shopService = new ShopService(this.req);
		const shops = await shopService.findAllWhereUserIsShopUser(user.id);
		// проверяем пользователя и получаем для него магазин
		if (!shops.length) {
			throw ResponseError.accessDenied('У пользователя нет доступа к отчетам');
		}
		const shop = shops[0]; // FIXME может быть несколько магазинов
		// проверка времени магазина, очтет можно отправлять до 18:00
		const shopDate = moment().utcOffset(shop.timeOffset);
		if (shopDate.hour() > 17) {
			throw ResponseError.validationInput(`Данный вид отчета можно отправлять до 18:00`);
		}
		// для магазина ищем репорт за текущий день
		const shopToday = shopDate.format('YYYY-MM-DD');
		const shopReport = await shopReportingStore.findOne({
			where: {
				shopId: shop.id,
				date: shopToday,
				reportType: ShopReportingType.PROBLEM_CHECK,
			},
		});
		const saveValue = JSON.stringify({ radio1, radio2 });
		if (!shopReport) {
			return shopReportingStore.createOne({
				accountId: user.accountId,
				shop,
				reportType: ShopReportingType.PROBLEM_CHECK,
				date: shopToday,
				value: saveValue,
				ownerId: user.id,
			});
		} else {
			if (shopReport.isReported) {
				throw ResponseError.validationInput(`Отчет для магазина уже отправлен`);
			}
			return shopReportingStore.updateOne(shopReport, {
				value: saveValue,
				moderatorId: user.id,
			});
		}
	}

	updatePersonnelCheckReportForCurrentUser = async (user, report) => {
		// проверка репорта
		const { text1, text2 } = report || {};
		const good1 = text1 === 0 || text1 === '0' || !!Number(text1);
		const good2 = text2 === 0 || text2 === '0' || !!Number(text2);
		const warn = !good1 || !good2;
		if (warn) {
			throw ResponseError.validationInput(`Ошибка проверки отчета`);
		}
		const shopService = new ShopService(this.req);
		const shops = await shopService.findAllWhereUserIsShopUser(user.id);
		// проверяем пользователя и получаем для него магазин
		if (!shops.length) {
			throw ResponseError.accessDenied('У пользователя нет доступа к отчетам');
		}
		const shop = shops[0]; // FIXME может быть несколько магазинов
		// проверка времени магазина, очтет можно отправлять до 18:00
		const shopDate = moment().utcOffset(shop.timeOffset);
		if (shopDate.hour() > 17) {
			throw ResponseError.validationInput(`Данный вид отчета можно отправлять до 18:00`);
		}
		// для магазина ищем репорт за текущий день
		const shopToday = shopDate.format('YYYY-MM-DD');
		const shopReport = await shopReportingStore.findOne({
			where: {
				shopId: shop.id,
				date: shopToday,
				reportType: ShopReportingType.PERSONNEL_CHECK,
			},
		});
		const saveValue = JSON.stringify({ text1, text2 });
		if (!shopReport) {
			return shopReportingStore.createOne({
				accountId: user.accountId,
				shop,
				reportType: ShopReportingType.PERSONNEL_CHECK,
				date: shopToday,
				value: saveValue,
				ownerId: user.id,
			});
		} else {
			if (shopReport.isReported) {
				throw ResponseError.validationInput(`Отчет для магазина уже отправлен`);
			}
			return shopReportingStore.updateOne(shopReport, {
				value: saveValue,
				moderatorId: user.id,
			});
		}
	}

	updateCameraCheckReportForCurrentUser = async (user, report) => {
		// проверка репорта
		const { radio1, radio2, text3 } = report || {};
		const isFirst = radio1 === true;
		const isSecond = radio1 === false && (radio2 === 'noInstall' || radio2 === 'noWork' || radio2 === 'halfWork');
		const isThird = radio1 === false && radio2 === 'myAnswer' && !!text3;
		const warn = !isFirst && !isSecond && !isThird;
		if (warn) {
			throw ResponseError.validationInput(`Ошибка проверки отчета`);
		}
		const shopService = new ShopService(this.req);
		const shops = await shopService.findAllWhereUserIsShopUser(user.id);
		// проверяем пользователя и получаем для него магазин
		if (!shops.length) {
			throw ResponseError.accessDenied('У пользователя нет доступа к отчетам');
		}
		const shop = shops[0]; // FIXME может быть несколько магазинов
		// проверка времени магазина, очтет можно отправлять до 18:00
		const shopDate = moment().utcOffset(shop.timeOffset);
		if (shopDate.hour() > 18) {
			throw ResponseError.validationInput(`Данный вид отчета можно отправлять до 18:00`);
		}
		// для магазина ищем репорт за текущий день
		const shopToday = shopDate.format('YYYY-MM-DD');
		const shopReport = await shopReportingStore.findOne({
			where: {
				shopId: shop.id,
				date: shopToday,
				reportType: ShopReportingType.CAMERA_CHECK,
			},
		});
		const saveValue = JSON.stringify({ radio1, radio2, text3 });
		if (!shopReport) {
			return shopReportingStore.createOne({
				accountId: user.accountId,
				shop,
				reportType: ShopReportingType.CAMERA_CHECK,
				date: shopToday,
				value: saveValue,
				ownerId: user.id,
			});
		} else {
			if (shopReport.isReported) {
				throw ResponseError.validationInput(`Отчет для магазина уже отправлен`);
			}
			return shopReportingStore.updateOne(shopReport, {
				value: saveValue,
				moderatorId: user.id,
			});
		}
	}
}
