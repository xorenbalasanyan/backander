import { scheduleJobSettingStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('ScheduleJobSettingService');

export default class ScheduleJobSettingService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, scheduleJobSettingStore);
	}

	async findAllByJobNameAndKey(jobName: string, key: string) {
		return this.findAll({
			where: {
				jobName,
				key,
			},
		});
	}
}
