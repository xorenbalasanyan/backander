import 'source-map-support/register';
import WebServer from '~lib/WebServer';
import Scheduler from './background/Scheduler';
import { loadJobsFromConfig } from './background/scheduleJobs/jobLoader';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const logger = new AppLogger('backend');

const webServer = new WebServer();

async function run() {
	//  стартуем работу веб-сервера
	await webServer
		.init()
		.then(() => webServer.start())
		.then(() => {
			// после старта веб-сервера запускаем шедулер
			const jobs = loadJobsFromConfig();
			if (jobs?.length) {
				// запускаем шедулер, добавляем джобы
				const scheduler = Scheduler.getInstance();
				jobs.forEach(job => scheduler.addJob(job));
				scheduler.start();
			}
		})
		.catch(error => {
			logger.error(fillError(Error, '[Ошибка при запуске приложения]', error));
			process.exit(1);
		});
}

run();
