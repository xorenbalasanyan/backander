System.config({
  paths: {
    // common source
    '~enums/*': '../pajero-common/src/enums/*',
    '~errors/*': '../pajero-common/src/errors/*',
    '~types': '../pajero-common/src/types',
    '~utils/*': '../pajero-common/src/utils/*',
    // backend source
    '~lib/*': './src/lib/*',
    '~middlewares': './src/middlewares',
    '~stores': './src/stores',
    '~jobs': './src/jobs',
    '~controllers': './src/controllers',
    '~services': './src/services',
    '~repositories': './src/repositories',
  },
});
