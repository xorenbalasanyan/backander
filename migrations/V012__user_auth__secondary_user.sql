DROP TRIGGER user_auth_after_insert;
DROP TRIGGER user_auth_after_update;
DROP TRIGGER user_auth_before_delete;

ALTER TABLE user_auth
    ADD COLUMN secondaryUserId INT NULL AFTER ipAddress,
    ADD COLUMN lastUrl VARCHAR(200) NULL AFTER secondaryUserId;
ALTER TABLE user_auth_log
    ADD COLUMN secondaryUserId INT NULL AFTER ipAddress,
    ADD COLUMN lastUrl VARCHAR(200) NULL AFTER secondaryUserId;

CREATE TRIGGER user_auth_after_insert
    AFTER INSERT
    ON user_auth
    FOR EACH ROW
BEGIN
    INSERT INTO user_auth_log
    VALUES (NEW.id, NEW.userId, NEW.authToken, NEW.refreshToken, NEW.ipAddress, NEW.secondaryUserId, NEW.lastUrl, NEW.createdAt,            NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER user_auth_after_update
    AFTER UPDATE
    ON user_auth
    FOR EACH ROW
BEGIN
    INSERT INTO user_auth_log
    VALUES (NEW.id, NEW.userId, NEW.authToken, NEW.refreshToken, NEW.ipAddress, NEW.secondaryUserId, NEW.lastUrl, NEW.createdAt,            NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER user_auth_before_delete
    BEFORE DELETE
    ON user_auth
    FOR EACH ROW
BEGIN
    INSERT INTO user_auth_log
    VALUES (OLD.id, OLD.userId, OLD.authToken, OLD.refreshToken, OLD.ipAddress, OLD.secondaryUserId, OLD.lastUrl,OLD.createdAt,            OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;
