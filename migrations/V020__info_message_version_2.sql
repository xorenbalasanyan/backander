DROP TRIGGER info_message_after_insert;
DROP TRIGGER info_message_after_update;
DROP TRIGGER info_message_before_delete;

DROP TABLE info_message_to_addressee;
DROP TABLE info_message;
DROP TABLE info_message_log;

-- TABLE info_message

CREATE TABLE info_message
(
    id                  INT AUTO_INCREMENT PRIMARY KEY,
    accountId           INT                                     NOT NULL,
    subject             VARCHAR(300) COLLATE utf8mb4_unicode_ci NOT NULL,
    fileId              INT                                     NULL,
    isImportant         TINYINT                                 NOT NULL DEFAULT 0,
    externalMessageId   VARCHAR(200)                            NULL,
    externalMessageDate TIMESTAMP                               NULL,
    createdAt           TIMESTAMP                               NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    ownerId             INT                                     NOT NULL,
    updatedAt           TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId         INT                                     NULL,
    CONSTRAINT fk__info_message__account FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT fk__info_message__file FOREIGN KEY (fileId) REFERENCES file (id) ON DELETE RESTRICT,
    CONSTRAINT fk__info_message__owner FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__info_message__moderator FOREIGN KEY (moderatorId) REFERENCES user (id) ON DELETE RESTRICT
);

CREATE TABLE info_message__log
(
    id                  INT,
    accountId           INT,
    subject             VARCHAR(300) COLLATE utf8mb4_unicode_ci,
    fileId              INT,
    isImportant         TINYINT,
    externalMessageId   VARCHAR(200),
    externalMessageDate TIMESTAMP,
    createdAt           TIMESTAMP,
    ownerId             INT,
    updatedAt           TIMESTAMP,
    moderatorId         INT,
    __type              TINYINT NOT NULL
);

CREATE TRIGGER info_message__after_insert
    AFTER INSERT
    ON info_message
    FOR EACH ROW
BEGIN
    INSERT INTO info_message__log
    VALUES (NEW.id, NEW.accountId, NEW.subject, NEW.fileId, NEW.isImportant, NEW.externalMessageId,
            NEW.externalMessageDate, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER info_message__after_update
    AFTER UPDATE
    ON info_message
    FOR EACH ROW
BEGIN
    INSERT INTO info_message__log
    VALUES (NEW.id, NEW.accountId, NEW.subject, NEW.fileId, NEW.isImportant, NEW.externalMessageId,
            NEW.externalMessageDate, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER info_message__before_delete
    BEFORE DELETE
    ON info_message
    FOR EACH ROW
BEGIN
    INSERT INTO info_message__log
    VALUES (OLD.id, OLD.accountId, OLD.subject, OLD.fileId, OLD.isImportant, OLD.externalMessageId,
                OLD.externalMessageDate, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;

-- TABLE info_message_to_addressee

CREATE TABLE info_message_to_addressee
(
    infoMessageId   INT                                     NOT NULL,
    addresseeUserId INT                                     NOT NULL,
    status          ENUM ('CREATED', 'VIEWED')              NOT NULL,
    updatedAt       TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId     INT                                     NULL,
    CONSTRAINT fk__info_message_to_addressee__info_message FOREIGN KEY (infoMessageId) REFERENCES info_message (id) ON DELETE RESTRICT,
    CONSTRAINT fk__info_message_to_addressee__addressee FOREIGN KEY (addresseeUserId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__info_message_to_addressee__moderator FOREIGN KEY (moderatorId) REFERENCES user (id) ON DELETE RESTRICT,
    UNIQUE info_message_to_addressee__unique_index (infoMessageId, addresseeUserId)
);
