ALTER TABLE file
    ADD COLUMN isDeleted TINYINT DEFAULT 0 NOT NULL AFTER downloadCount;
