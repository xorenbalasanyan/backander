-- incident_import

CREATE TABLE incident_import
(
    id          INT PRIMARY KEY AUTO_INCREMENT,
    accountId   INT                                                          NOT NULL,
    fileId      INT                                                          NOT NULL,
    logId       INT,
    `date`      DATE                                                         NOT NULL,
    type        ENUM ('INCIDENT_TYPE_1', 'INCIDENT_TYPE2', 'INCIDENT_TYPE3') NOT NULL,
    state       ENUM ('OK', 'WARN', 'ERROR') DEFAULT 'OK'                    NOT NULL,
    addedCount  INT                                                          NOT NULL,
    warnCount   INT                                                          NOT NULL,
    createdAt   TIMESTAMP                    DEFAULT CURRENT_TIMESTAMP()     NOT NULL,
    ownerId     INT                                                          NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP()                      NULL,
    moderatorId INT                                                          NULL,
    CONSTRAINT incident_import_account_fk FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT incident_import_file_fk FOREIGN KEY (fileId) REFERENCES file (id) ON DELETE RESTRICT,
    CONSTRAINT incident_import_log_fk FOREIGN KEY (logId) REFERENCES log (id) ON DELETE RESTRICT
);

ALTER TABLE incident_import
    AUTO_INCREMENT = 3000;

CREATE TABLE incident_import_log
(
    id          INT,
    accountId   INT,
    fileId      INT,
    logId       INT,
    `date`      DATE,
    type        ENUM ('INCIDENT_TYPE_1', 'INCIDENT_TYPE2', 'INCIDENT_TYPE3'),
    state       ENUM ('OK', 'WARN', 'ERROR'),
    addedCount  INT,
    warnCount   INT,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER incident_import_after_insert
    AFTER INSERT
    ON incident_import
    FOR EACH ROW
BEGIN
    INSERT INTO incident_import_log
    VALUES (NEW.id, NEW.accountId, NEW.fileId, NEW.logId, NEW.date, NEW.type, NEW.state, NEW.addedCount, NEW.warnCount,
            NEW.createdAt, NEW.ownerId, NEw.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER incident_import_after_update
    AFTER UPDATE
    ON incident_import
    FOR EACH ROW
BEGIN
    INSERT INTO incident_import_log
    VALUES (NEW.id, NEW.accountId, NEW.fileId, NEW.logId, NEW.date, NEW.type, NEW.state, NEW.addedCount, NEW.warnCount,
            NEW.createdAt, NEW.ownerId, NEw.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER incident_import_before_delete
    BEFORE DELETE
    ON incident_import
    FOR EACH ROW
BEGIN
    INSERT INTO incident_import_log
    VALUES (OLD.id, OLD.accountId, OLD.fileId, OLD.logId, OLD.date, OLD.type, OLD.state, OLD.addedCount, OLD.warnCount,
            OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;

-- update incident_type

DROP TRIGGER incident_type_after_insert;
DROP TRIGGER incident_type_after_update;
DROP TRIGGER incident_type_before_delete;

ALTER TABLE incident_type ADD COLUMN type VARCHAR(50) NOT NULL AFTER name;
ALTER TABLE incident_type_log ADD COLUMN type VARCHAR(50) NOT NULL AFTER name;

CREATE TRIGGER incident_type_after_insert
    AFTER INSERT
    ON incident_type
    FOR EACH ROW
BEGIN
    INSERT INTO incident_type_log
    VALUES (NEW.id, NEW.accountId, NEW.name, NEW.type, NEW.data, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER incident_type_after_update
    AFTER UPDATE
    ON incident_type
    FOR EACH ROW
BEGIN
    INSERT INTO incident_type_log
    VALUES (NEW.id, NEW.accountId, NEW.name, NEW.type, NEW.data, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER incident_type_before_delete
    BEFORE DELETE
    ON incident_type
    FOR EACH ROW
BEGIN
    INSERT INTO incident_type_log
    VALUES (OLD.id, OLD.accountId, OLD.name, OLD.type, OLD.data, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;

UPDATE incident_type SET type = 'INCIDENT_TYPE_1' WHERE id = 200;

-- update log
ALTER TABLE log
    MODIFY message TEXT COLLATE utf8mb4_unicode_ci NOT NULL;
