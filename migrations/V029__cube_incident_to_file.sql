-- TABLE cube_incident_to_file

CREATE TABLE cube_incident_to_file
(
    id         INT AUTO_INCREMENT PRIMARY KEY,
    accountId  INT       NOT NULL,
    incidentId INT       NOT NULL,
    goodId     INT       NOT NULL,
    fileId     INT       NOT NULL,
    createdAt  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP()
);
