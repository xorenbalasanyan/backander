-- Добавляем таблицу shop_discount

CREATE TABLE shop_discount
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    accountId   INT                                     NOT NULL,
    shopId      INT                                     NOT NULL,
    name        VARCHAR(200) COLLATE utf8mb4_unicode_ci NOT NULL,
    createdAt   TIMESTAMP                               NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL,
    CONSTRAINT fk__shop_discount__account FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT fk__shop_discount__shop FOREIGN KEY (shopId) REFERENCES shop (id) ON DELETE RESTRICT,
    CONSTRAINT fk__shop_discount__owner FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__shop_discount__moderator FOREIGN KEY (moderatorId) REFERENCES user (id) ON DELETE RESTRICT
);

CREATE TABLE shop_discount__log
(
    id          INT,
    accountId   INT,
    shopId      INT,
    name        VARCHAR(200) COLLATE utf8mb4_unicode_ci,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER shop_discount__after_insert
    AFTER INSERT
    ON shop_discount
    FOR EACH ROW
BEGIN
    INSERT INTO shop_discount__log
    VALUES (NEW.id, NEW.accountId, NEW.shopId, NEW.name, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER shop_discount__after_update
    AFTER UPDATE
    ON shop_discount
    FOR EACH ROW
BEGIN
    INSERT INTO shop_discount__log
    VALUES (NEW.id, NEW.accountId, NEW.shopId, NEW.name, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER shop_discount__before_delete
    BEFORE DELETE
    ON shop_discount
    FOR EACH ROW
BEGIN
    INSERT INTO shop_discount__log
    VALUES (OLD.id, OLD.accountId, OLD.shopId, OLD.name, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;

-- Обновляем инциденты

DROP TRIGGER incident_after_insert;
DROP TRIGGER incident_after_update;
DROP TRIGGER incident_before_delete;

ALTER TABLE incident
    ADD COLUMN shopDiscountId INT NULL AFTER shopId;

ALTER TABLE incident
    ADD CONSTRAINT fk__incident__shop_discount FOREIGN KEY (shopDiscountId) REFERENCES shop_discount (id) ON DELETE RESTRICT;

ALTER TABLE incident
    DROP INDEX incident_unique_index,
    ADD UNIQUE incident__unique_index (shopId, shopDiscountId, goodId, `date`, incidentTypeId);

RENAME TABLE incident_log TO incident__log;

ALTER TABLE incident__log
    ADD COLUMN shopDiscountId INT NULL AFTER shopId;

CREATE TRIGGER incident__after_insert
    AFTER INSERT
    ON incident
    FOR EACH ROW
BEGIN
    INSERT INTO incident__log
    VALUES (NEW.id, NEW.externalId, NEW.shopId, NEW.shopDiscountId, NEW.incidentTypeId, NEW.incidentTypeVersion,
            NEW.goodId, NEW.date, NEW.state, NEW.inputData, NEW.report, NEW.isReported, NEW.createdAt, NEW.ownerId,
            NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER incident__after_update
    AFTER UPDATE
    ON incident
    FOR EACH ROW
BEGIN
    INSERT INTO incident__log
    VALUES (NEW.id, NEW.externalId, NEW.shopId, NEW.shopDiscountId, NEW.incidentTypeId, NEW.incidentTypeVersion,
            NEW.goodId, NEW.date, NEW.state, NEW.inputData, NEW.report, NEW.isReported, NEW.createdAt, NEW.ownerId,
            NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER incident__before_delete
    BEFORE DELETE
    ON incident
    FOR EACH ROW
BEGIN
    INSERT INTO incident__log
    VALUES (OLD.id, OLD.externalId, OLD.shopId, OLD.shopDiscountId, OLD.incidentTypeId, OLD.incidentTypeVersion,
            OLD.goodId, OLD.date, OLD.state, OLD.inputData, OLD.report, OLD.isReported, OLD.createdAt, OLD.ownerId,
            OLD.updatedAt, OLD.moderatorId, 3);
END;
