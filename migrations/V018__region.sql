-- Добавляем таблицу region

CREATE TABLE region
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    accountId   INT                                     NOT NULL,
    name        VARCHAR(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    email       VARCHAR(100) COLLATE utf8mb4_unicode_ci NULL,
    createdAt   TIMESTAMP                               NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL,
    CONSTRAINT fk__region__account FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT fk__region__owner FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__region__moderator FOREIGN KEY (moderatorId) REFERENCES user (id) ON DELETE RESTRICT
);

CREATE TABLE region__log
(
    id          INT,
    accountId   INT,
    name        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    email       VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER region__after_insert
    AFTER INSERT
    ON region
    FOR EACH ROW
BEGIN
    INSERT INTO region__log
    VALUES (NEW.id, NEW.accountId, NEW.name, NEW.email, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER region__after_update
    AFTER UPDATE
    ON region
    FOR EACH ROW
BEGIN
    INSERT INTO region__log
    VALUES (NEW.id, NEW.accountId, NEW.name, NEW.email, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER region__before_delete
    BEFORE DELETE
    ON region
    FOR EACH ROW
BEGIN
    INSERT INTO region__log
    VALUES (OLD.id, OLD.accountId, OLD.name, OLD.email, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;

-- Обновляем структуру магазинов

DROP TRIGGER shop_after_insert;
DROP TRIGGER shop_after_update;
DROP TRIGGER shop_before_delete;

ALTER TABLE shop ADD COLUMN regionId INT NULL AFTER region;
ALTER TABLE shop ADD CONSTRAINT fk__shop__region FOREIGN KEY (regionId) REFERENCES region (id) ON DELETE RESTRICT;

RENAME TABLE shop_log TO shop__log;

ALTER TABLE shop__log ADD COLUMN regionId INT NULL AFTER region;
ALTER TABLE shop__log DROP COLUMN region;

CREATE TRIGGER shop__after_insert
    AFTER INSERT
    ON shop
    FOR EACH ROW
BEGIN
    INSERT INTO shop__log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.dfId, NEW.tmId, NEW.upfId, NEW.name, NEW.regionId, NEW.city,
            NEW.address, NEW.phone, NEW.email, NEW.description, NEW.timeOffset, NEW.isDeleted, NEW.createdAt,
            NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER shop__after_update
    AFTER UPDATE
    ON shop
    FOR EACH ROW
BEGIN
    INSERT INTO shop__log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.dfId, NEW.tmId, NEW.upfId, NEW.name, NEW.regionId, NEW.city,
            NEW.address, NEW.phone, NEW.email, NEW.description, NEW.timeOffset, NEW.isDeleted, NEW.createdAt,
            NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER shop__before_delete
    BEFORE DELETE
    ON shop
    FOR EACH ROW
BEGIN
    INSERT INTO shop__log
    VALUES (OLD.id, OLD.externalId, OLD.accountId, OLD.dfId, OLD.tmId, OLD.upfId, OLD.name, OLD.regionId, OLD.city,
            OLD.address, OLD.phone, OLD.email, OLD.description, OLD.timeOffset, OLD.isDeleted, OLD.createdAt,
            OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;
