CREATE TABLE structure_import
(
    id                INT AUTO_INCREMENT PRIMARY KEY,
    accountId         INT       NOT NULL,
    fileId            INT       NULL,
    logId             INT       NULL,
    status            ENUM ('OK', 'WARN', 'ERROR') DEFAULT 'OK' NOT NULL,
    createdUsersCount INT       NOT NULL,
    updatedUsersCount INT       NOT NULL,
    deletedUsersCount INT       NOT NULL,
    createdShopsCount INT       NOT NULL,
    updatedShopsCount INT       NOT NULL,
    deletedShopsCount INT       NOT NULL,
    warnCount         INT       NOT NULL,
    createdAt         TIMESTAMP NOT NULL           DEFAULT CURRENT_TIMESTAMP(),
    ownerId           INT       NOT NULL,
    CONSTRAINT fk__structure_import__account FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT fk__structure_import__file    FOREIGN KEY (fileId)    REFERENCES file (id)    ON DELETE RESTRICT,
    CONSTRAINT fk__structure_import__log     FOREIGN KEY (logId)     REFERENCES log (id)     ON DELETE RESTRICT,
    CONSTRAINT fk__structure_import__owner   FOREIGN KEY (ownerId)   REFERENCES user (id)    ON DELETE RESTRICT
);
