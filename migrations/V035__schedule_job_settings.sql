CREATE TABLE schedule_job_settings
(
    id        INT AUTO_INCREMENT PRIMARY KEY,
    jobName   VARCHAR(100)                    NOT NULL,
    `key`     VARCHAR(100)                    NOT NULL,
    value     TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
    createdAt TIMESTAMP                       NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    updatedAt TIMESTAMP                       NOT NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()
);
