CREATE VIEW fresh_task_view AS
SELECT *
FROM task t
WHERE (DATE_ADD(t.updatedAt, INTERVAL t.executionPeriodDays + 30 DAY) > CURRENT_DATE);
