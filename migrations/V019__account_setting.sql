CREATE TABLE account_setting
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    accountId   INT                                     NOT NULL,
    `key`       VARCHAR(50)                             NOT NULL,
    value       TEXT COLLATE utf8mb4_unicode_ci         NOT NULL,
    createdAt   TIMESTAMP                               NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL,
    CONSTRAINT fk__account_setting__account FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT fk__account_setting__owner FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__account_setting__moderator FOREIGN KEY (moderatorId) REFERENCES user (id) ON DELETE RESTRICT
);

CREATE TABLE account_setting__log
(
    id          INT,
    accountId   INT,
    `key`       VARCHAR(50),
    value       TEXT COLLATE utf8mb4_unicode_ci,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER account_setting__after_insert
    AFTER INSERT
    ON account_setting
    FOR EACH ROW
BEGIN
    INSERT INTO account_setting__log
    VALUES (NEW.id, NEW.accountId, NEW.`key`, NEW.value, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER account_setting__after_update
    AFTER UPDATE
    ON account_setting
    FOR EACH ROW
BEGIN
    INSERT INTO account_setting__log
    VALUES (NEW.id, NEW.accountId, NEW.`key`, NEW.value, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER account_setting__before_delete
    BEFORE DELETE
    ON account_setting
    FOR EACH ROW
BEGIN
    INSERT INTO account_setting__log
    VALUES (OLD.id, OLD.accountId, OLD.`key`, OLD.value, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;
