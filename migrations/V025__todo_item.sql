-- TABLE todo_item

CREATE TABLE todo_item
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    accountId   INT                                      NOT NULL,
    externalId  VARCHAR(100)                             NULL,
    `date`      DATE                                     NOT NULL,
    addresseeId INT                                      NOT NULL,
    status      ENUM ('IN_PROGRESS', 'DONE')             NOT NULL,
    title       VARCHAR(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
    description TEXT COLLATE utf8mb4_unicode_ci          NULL,
    createdAt   TIMESTAMP                                NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    ownerId     INT                                      NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP()  NULL,
    moderatorId INT                                      NULL,
    CONSTRAINT fk__todo_item__account FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT fk__todo_item__addressee FOREIGN KEY (addresseeId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__todo_item__owner FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__todo_item__moderator FOREIGN KEY (moderatorId) REFERENCES user (id) ON DELETE RESTRICT
);

CREATE TABLE todo_item__log
(
    id          INT,
    accountId   INT,
    externalId  VARCHAR(100),
    `date`      DATE,
    addresseeId INT,
    status      ENUM ('IN_PROGRESS', 'DONE'),
    title       VARCHAR(2000) COLLATE utf8mb4_unicode_ci,
    description TEXT COLLATE utf8mb4_unicode_ci,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(),
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER todo_item__after_insert
    AFTER INSERT
    ON todo_item
    FOR EACH ROW
BEGIN
    INSERT INTO todo_item__log
    VALUES (NEW.id, NEW.accountId, NEW.externalId, NEW.`date`, NEW.addresseeId, NEW.status, NEW.title, NEW.description,
            NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER todo_item__after_update
    AFTER UPDATE
    ON todo_item
    FOR EACH ROW
BEGIN
    INSERT INTO todo_item__log
    VALUES (NEW.id, NEW.accountId, NEW.externalId, NEW.`date`, NEW.addresseeId, NEW.status, NEW.title, NEW.description,
            NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER todo_item__before_delete
    BEFORE DELETE
    ON todo_item
    FOR EACH ROW
BEGIN
    INSERT INTO todo_item__log
    VALUES (OLD.id, OLD.accountId, OLD.externalId, OLD.`date`, OLD.addresseeId, OLD.status, OLD.title, OLD.description,
            OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;
