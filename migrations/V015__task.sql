-- TABLE task

CREATE TABLE task
(
    id                  INT AUTO_INCREMENT PRIMARY KEY,
    accountId           INT                                     NOT NULL,
    title               VARCHAR(200) COLLATE utf8mb4_unicode_ci NOT NULL,
    description         TEXT COLLATE utf8mb4_unicode_ci         NULL,
    executionPeriodDays INT                                     NOT NULL,
    createdAt           TIMESTAMP                               NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    ownerId             INT                                     NOT NULL,
    updatedAt           TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId         INT                                     NULL,
    CONSTRAINT fk__task__account FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT fk__task__owner FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__task__moderator FOREIGN KEY (moderatorId) REFERENCES user (id) ON DELETE RESTRICT
);

CREATE TABLE task__log
(
    id                  INT,
    accountId           INT,
    title               VARCHAR(200) COLLATE utf8mb4_unicode_ci,
    description         TEXT COLLATE utf8mb4_unicode_ci,
    executionPeriodDays INT,
    createdAt           TIMESTAMP,
    ownerId             INT,
    updatedAt           TIMESTAMP,
    moderatorId         INT,
    __type              TINYINT NOT NULL
);

CREATE TRIGGER task__after_insert
    AFTER INSERT
    ON task
    FOR EACH ROW
BEGIN
    INSERT INTO task__log
    VALUES (NEW.id, NEW.accountId, NEW.title, NEW.description, NEW.executionPeriodDays, NEW.createdAt, NEW.ownerId,
            NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER task__after_update
    AFTER UPDATE
    ON task
    FOR EACH ROW
BEGIN
    INSERT INTO task__log
    VALUES (NEW.id, NEW.accountId, NEW.title, NEW.description, NEW.executionPeriodDays, NEW.createdAt, NEW.ownerId,
            NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER task__before_delete
    BEFORE DELETE
    ON task
    FOR EACH ROW
BEGIN
    INSERT INTO task__log
    VALUES (OLD.id, OLD.accountId, OLD.title, OLD.description, OLD.executionPeriodDays, OLD.createdAt, OLD.ownerId,
            OLD.updatedAt, OLD.moderatorId, 3);
END;

-- TABLE task_to_shop

CREATE TABLE task_to_shop
(
    id               INT AUTO_INCREMENT PRIMARY KEY,
    taskId           INT                                      NOT NULL,
    shopId           INT                                      NOT NULL,
    status           ENUM ('IN_PROGRESS', 'REJECTED', 'DONE') NOT NULL,
    dueDate          TIMESTAMP DEFAULT NULL                   NULL,
    executorUserId   INT                                      NULL,
    executionComment TEXT COLLATE utf8mb4_unicode_ci          NULL,
    executionInfo    TEXT COLLATE utf8mb4_unicode_ci          NULL,
    executedAt       TIMESTAMP                                NULL,
    rejectorUserId   INT                                      NULL,
    rejectionComment TEXT COLLATE utf8mb4_unicode_ci          NULL,
    rejectedAt       TIMESTAMP                                NULL,
    updatedAt        TIMESTAMP                                NULL ON UPDATE CURRENT_TIMESTAMP(),
    moderatorId      INT                                      NULL,
    CONSTRAINT fk__task_to_shop__task FOREIGN KEY (taskId) REFERENCES task (id) ON DELETE RESTRICT,
    CONSTRAINT fk__task_to_shop__shop FOREIGN KEY (shopId) REFERENCES shop (id) ON DELETE RESTRICT,
    CONSTRAINT fk__task_to_shop__executor FOREIGN KEY (executorUserId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__task_to_shop__rejector FOREIGN KEY (rejectorUserId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__task_to_shop__moderator FOREIGN KEY (moderatorId) REFERENCES user (id) ON DELETE RESTRICT,
    UNIQUE task_to_shop__unique_index (taskId, shopId)
);

CREATE TABLE task_to_shop__log
(
    id               INT,
    taskId           INT,
    shopId           INT,
    status           ENUM ('IN_PROGRESS', 'REJECTED', 'DONE'),
    dueDate          TIMESTAMP,
    executorUserId   INT,
    executionComment TEXT COLLATE utf8mb4_unicode_ci,
    executionInfo    TEXT COLLATE utf8mb4_unicode_ci,
    executedAt       TIMESTAMP,
    rejectorUserId   INT,
    rejectionComment TEXT COLLATE utf8mb4_unicode_ci,
    rejectedAt       TIMESTAMP,
    updatedAt        TIMESTAMP,
    moderatorId      INT,
    __type           TINYINT NOT NULL
);

CREATE TRIGGER task_to_shop__after_insert
    AFTER INSERT
    ON task_to_shop
    FOR EACH ROW
BEGIN
    INSERT INTO task_to_shop__log
    VALUES (NEW.id, NEW.taskId, NEW.shopId, NEW.status, NEW.dueDate, NEW.executorUserId, NEW.executionComment,
            NEW.executionInfo, NEW.executedAt, NEW.rejectorUserId, NEW.rejectionComment, NEW.rejectedAt, NEW.updatedAt,
            NEW.moderatorId, 1);
END;

CREATE TRIGGER task_to_shop__after_update
    AFTER UPDATE
    ON task_to_shop
    FOR EACH ROW
BEGIN
    INSERT INTO task_to_shop__log
    VALUES (NEW.id, NEW.taskId, NEW.shopId, NEW.status, NEW.dueDate, NEW.executorUserId, NEW.executionComment,
            NEW.executionInfo, NEW.executedAt, NEW.rejectorUserId, NEW.rejectionComment, NEW.rejectedAt, NEW.updatedAt,
            NEW.moderatorId, 2);
END;

CREATE TRIGGER task_to_shop__before_delete
    BEFORE DELETE
    ON task_to_shop
    FOR EACH ROW
BEGIN
    INSERT INTO task_to_shop__log
    VALUES (OLD.id, OLD.taskId, OLD.shopId, OLD.status, OLD.dueDate, OLD.executorUserId, OLD.executionComment,
            OLD.executionInfo, OLD.executedAt, OLD.rejectorUserId, OLD.rejectionComment, OLD.rejectedAt, OLD.updatedAt,
            OLD.moderatorId, 3);
END;
