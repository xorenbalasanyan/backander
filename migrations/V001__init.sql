-- TABLE account

CREATE TABLE account
(
    id          INT PRIMARY KEY AUTO_INCREMENT          NOT NULL,
    title       VARCHAR(100) COLLATE utf8mb4_unicode_ci NOT NULL UNIQUE,
    contacts    TEXT COLLATE utf8mb4_unicode_ci,
    description TEXT COLLATE utf8mb4_unicode_ci,
    email       VARCHAR(100) COLLATE utf8mb4_unicode_ci UNIQUE,
    phone       VARCHAR(100) COLLATE utf8mb4_unicode_ci UNIQUE,
    createdAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP()   NOT NULL,
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL
);

ALTER TABLE account
    AUTO_INCREMENT = 1000;

CREATE TABLE account_log
(
    id          INT,
    title       VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    contacts    TEXT COLLATE utf8mb4_unicode_ci,
    description TEXT COLLATE utf8mb4_unicode_ci,
    email       VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    phone       VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER account_after_insert
    AFTER INSERT
    ON account
    FOR EACH ROW
BEGIN
    INSERT INTO account_log
    VALUES (NEW.id, NEW.title, NEW.contacts, NEW.description, NEW.email, NEW.phone, NEW.createdAt, NEW.ownerId,
            NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER account_after_update
    AFTER UPDATE
    ON account
    FOR EACH ROW
BEGIN
    INSERT INTO account_log
    VALUES (NEW.id, NEW.title, NEW.contacts, NEW.description, NEW.email, NEW.phone, NEW.createdAt, NEW.ownerId,
            NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER account_before_delete
    BEFORE DELETE
    ON account
    FOR EACH ROW
BEGIN
    INSERT INTO account_log
    VALUES (OLD.id, OLD.title, OLD.contacts, OLD.description, OLD.email, OLD.phone, OLD.createdAt, OLD.ownerId,
            OLD.updatedAt, OLD.moderatorId, 3);
END;

-- TABLE filial

CREATE TABLE filial
(
    id          INT PRIMARY KEY AUTO_INCREMENT          NOT NULL,
    externalId  VARCHAR(100),
    accountId   INT                                     NOT NULL,
    name        VARCHAR(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    place       VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    description TEXT COLLATE utf8mb4_unicode_ci,
    createdAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP()   NOT NULL,
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL,
    CONSTRAINT filial_account_fk FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT
);

ALTER TABLE filial
    AUTO_INCREMENT = 1000000;

CREATE TABLE filial_log
(
    id          INT,
    externalId  VARCHAR(100),
    accountId   INT,
    name        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    place       VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    description TEXT COLLATE utf8mb4_unicode_ci,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER filial_after_insert
    AFTER INSERT
    ON filial
    FOR EACH ROW
BEGIN
    INSERT INTO filial_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.name, NEW.place, NEW.description, NEW.createdAt, NEW.ownerId,
            NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER filial_after_update
    AFTER UPDATE
    ON filial
    FOR EACH ROW
BEGIN
    INSERT INTO filial_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.name, NEW.place, NEW.description, NEW.createdAt, NEW.ownerId,
            NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER filial_before_delete
    BEFORE DELETE
    ON filial
    FOR EACH ROW
BEGIN
    INSERT INTO filial_log
    VALUES (OLD.id, OLD.externalId, OLD.accountId, OLD.name, OLD.place, OLD.description, OLD.createdAt, OLD.ownerId,
            OLD.updatedAt, OLD.moderatorId, 3);
END;

-- TABLE tm

CREATE TABLE tm
(
    id          INT PRIMARY KEY AUTO_INCREMENT          NOT NULL,
    externalId  VARCHAR(100),
    accountId   INT                                     NOT NULL,
    filialId    INT                                     NOT NULL,
    name        VARCHAR(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    place       VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    description TEXT COLLATE utf8mb4_unicode_ci,
    createdAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP()   NOT NULL,
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL,
    CONSTRAINT tm_account_fk FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT tm_filial_fk FOREIGN KEY (filialId) REFERENCES filial (id) ON DELETE RESTRICT
);

ALTER TABLE tm
    AUTO_INCREMENT = 1100000;

CREATE TABLE tm_log
(
    id          INT,
    externalId  VARCHAR(100),
    accountId   INT,
    filialId    INT,
    name        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    place       VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    description TEXT COLLATE utf8mb4_unicode_ci,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER tm_after_insert
    AFTER INSERT
    ON tm
    FOR EACH ROW
BEGIN
    INSERT INTO tm_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.filialId, NEW.name, NEW.place, NEW.description, NEW.createdAt,
            NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER tm_after_update
    AFTER UPDATE
    ON tm
    FOR EACH ROW
BEGIN
    INSERT INTO tm_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.filialId, NEW.name, NEW.place, NEW.description, NEW.createdAt,
            NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER tm_before_delete
    BEFORE DELETE
    ON tm
    FOR EACH ROW
BEGIN
    INSERT INTO tm_log
    VALUES (OLD.id, OLD.externalId, OLD.accountId, OLD.filialId, OLD.name, OLD.place, OLD.description, OLD.createdAt,
            OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;

-- TABLE upf

CREATE TABLE upf
(
    id          INT PRIMARY KEY AUTO_INCREMENT          NOT NULL,
    externalId  VARCHAR(100),
    accountId   INT                                     NOT NULL,
    filialId    INT                                     NOT NULL,
    tmId        INT                                     NOT NULL,
    name        VARCHAR(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    place       VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    description TEXT COLLATE utf8mb4_unicode_ci,
    createdAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP()   NOT NULL,
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL,
    CONSTRAINT upf_account_fk FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT upf_filial_fk FOREIGN KEY (filialId) REFERENCES filial (id) ON DELETE RESTRICT,
    CONSTRAINT upf_tm_fk FOREIGN KEY (tmId) REFERENCES tm (id) ON DELETE RESTRICT
);

ALTER TABLE upf
    AUTO_INCREMENT = 1200000;

CREATE TABLE upf_log
(
    id          INT,
    externalId  VARCHAR(100),
    accountId   INT,
    filialId    INT,
    tmId        INT,
    name        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    place       VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    description TEXT COLLATE utf8mb4_unicode_ci,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER upf_after_insert
    AFTER INSERT
    ON upf
    FOR EACH ROW
BEGIN
    INSERT INTO upf_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.filialId, NEW.tmId, NEW.name, NEW.place, NEW.description,
            NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER upf_after_update
    AFTER UPDATE
    ON upf
    FOR EACH ROW
BEGIN
    INSERT INTO upf_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.filialId, NEW.tmId, NEW.name, NEW.place, NEW.description,
            NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER upf_before_delete
    BEFORE DELETE
    ON upf
    FOR EACH ROW
BEGIN
    INSERT INTO upf_log
    VALUES (OLD.id, OLD.externalId, OLD.accountId, OLD.filialId, OLD.tmId, OLD.name, OLD.place, OLD.description,
            OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;

-- TABLE shop

CREATE TABLE shop
(
    id          INT PRIMARY KEY AUTO_INCREMENT          NOT NULL,
    externalId  VARCHAR(100),
    accountId   INT                                     NOT NULL,
    filialId    INT,
    tmId        INT,
    upfId       INT,
    name        VARCHAR(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    city        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    address     VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    phone       VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    email       VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    description TEXT COLLATE utf8mb4_unicode_ci,
    timeOffset  INT                                     NOT NULL,
    createdAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP()   NOT NULL,
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL,
    CONSTRAINT shop_account_fk FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT shop_filial_fk FOREIGN KEY (filialId) REFERENCES filial (id) ON DELETE RESTRICT,
    CONSTRAINT shop_tm_fk FOREIGN KEY (tmId) REFERENCES tm (id) ON DELETE RESTRICT,
    CONSTRAINT shop_upf_fk FOREIGN KEY (upfId) REFERENCES upf (id) ON DELETE RESTRICT
);

ALTER TABLE shop
    ADD UNIQUE shop_unique_index (accountId, externalId);

ALTER TABLE shop
    AUTO_INCREMENT = 1300000;

CREATE TABLE shop_log
(
    id          INT,
    externalId  VARCHAR(100),
    accountId   INT,
    filialId    INT,
    tmId        INT,
    upfId       INT,
    name        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    city        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    address     VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    phone       VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    email       VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    description TEXT COLLATE utf8mb4_unicode_ci,
    timeOffset  INT,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER shop_after_insert
    AFTER INSERT
    ON shop
    FOR EACH ROW
BEGIN
    INSERT INTO shop_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.filialId, NEW.tmId, NEW.upfId, NEW.name, NEW.city, NEW.address,
            NEW.phone, NEW.email, NEW.description, NEW.timeOffset, NEW.createdAt, NEW.ownerId, NEW.updatedAt,
            NEW.moderatorId, 1);
END;

CREATE TRIGGER shop_after_update
    AFTER UPDATE
    ON shop
    FOR EACH ROW
BEGIN
    INSERT INTO shop_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.filialId, NEW.tmId, NEW.upfId, NEW.name, NEW.city, NEW.address,
            NEW.phone, NEW.email, NEW.description, NEW.timeOffset, NEW.createdAt, NEW.ownerId, NEW.updatedAt,
            NEW.moderatorId, 2);
END;

CREATE TRIGGER shop_before_delete
    BEFORE DELETE
    ON shop
    FOR EACH ROW
BEGIN
    INSERT INTO shop_log
    VALUES (OLD.id, OLD.externalId, OLD.accountId, OLD.filialId, OLD.tmId, OLD.upfId, OLD.name, OLD.city, OLD.address,
            OLD.phone, OLD.email, OLD.description, OLD.timeOffset, OLD.createdAt, OLD.ownerId, OLD.updatedAt,
            OLD.moderatorId, 3);
END;

-- TABLE user

CREATE TABLE user
(
    id           INT PRIMARY KEY AUTO_INCREMENT                NOT NULL,
    externalId   VARCHAR(100),
    accountId    INT                                           NOT NULL,
    filialId     INT,
    tmId         INT,
    upfId        INT,
    shopId       INT,
    role         ENUM ('ADMIN', 'FILIAL', 'TM', 'UPF', 'SHOP') NOT NULL,
    username     VARCHAR(100) COLLATE utf8mb4_unicode_ci       NOT NULL,
    passwordHash VARCHAR(100),
    `key`        VARCHAR(100) COLLATE utf8mb4_unicode_ci       NOT NULL,
    firstName    VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    lastName     VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    middleName   VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    email        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    phone        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    skype        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    telegram     VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    comment      TEXT COLLATE utf8mb4_unicode_ci,
    isDisabled   TINYINT   DEFAULT 0                           NOT NULL,
    createdAt    TIMESTAMP DEFAULT CURRENT_TIMESTAMP()         NOT NULL,
    ownerId      INT                                           NOT NULL,
    updatedAt    TIMESTAMP ON UPDATE CURRENT_TIMESTAMP()       NULL,
    moderatorId  INT                                           NULL,
    CONSTRAINT user_account_fk FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT user_filial_fk FOREIGN KEY (filialId) REFERENCES filial (id) ON DELETE RESTRICT,
    CONSTRAINT user_tm_fk FOREIGN KEY (tmId) REFERENCES tm (id) ON DELETE RESTRICT,
    CONSTRAINT user_upf_fk FOREIGN KEY (upfId) REFERENCES upf (id) ON DELETE RESTRICT,
    CONSTRAINT user_shop_fk FOREIGN KEY (shopId) REFERENCES shop (id) ON DELETE RESTRICT
);

ALTER TABLE user
    ADD UNIQUE user_unique_index (accountId, username);

ALTER TABLE user
    AUTO_INCREMENT = 2000000;

CREATE TABLE user_log
(
    id           INT,
    externalId   VARCHAR(100),
    accountId    INT,
    filialId     INT,
    tmId         INT,
    upfId        INT,
    shopId       INT,
    role         ENUM ('ADMIN', 'FILIAL', 'TM', 'UPF', 'SHOP'),
    username     VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    passwordHash VARCHAR(100),
    `key`        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    firstName    VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    lastName     VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    middleName   VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    email        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    phone        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    skype        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    telegram     VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    comment      TEXT COLLATE utf8mb4_unicode_ci,
    isDisabled   TINYINT,
    createdAt    TIMESTAMP,
    ownerId      INT,
    updatedAt    TIMESTAMP,
    moderatorId  INT,
    __type       TINYINT NOT NULL
);

CREATE TRIGGER user_after_insert
    AFTER INSERT
    ON user
    FOR EACH ROW
BEGIN
    INSERT INTO user_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.filialId, NEW.tmId, NEW.upfId, NEW.shopId, NEW.role,
            NEW.username, NEW.passwordHash, NEW.key, NEW.firstName, NEW.lastName, NEW.middleName, NEW.email,
            NEW.phone, NEW.skype, NEW.telegram, NEW.comment, NEW.isDisabled, NEW.createdAt, NEW.ownerId, NEW.updatedAt,
            NEW.moderatorId, 1);
END;

CREATE TRIGGER user_after_update
    AFTER UPDATE
    ON user
    FOR EACH ROW
BEGIN
    INSERT INTO user_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.filialId, NEW.tmId, NEW.upfId, NEW.shopId, NEW.role,
            NEW.username, NEW.passwordHash, NEW.key, NEW.firstName, NEW.lastName, NEW.middleName, NEW.email,
            NEW.phone, NEW.skype, NEW.telegram, NEW.comment, NEW.isDisabled, NEW.createdAt, NEW.ownerId, NEW.updatedAt,
            NEW.moderatorId, 2);
END;

CREATE TRIGGER user_before_delete
    BEFORE DELETE
    ON user
    FOR EACH ROW
BEGIN
    INSERT INTO user_log
    VALUES (OLD.id, OLD.externalId, OLD.accountId, OLD.filialId, OLD.tmId, OLD.upfId, OLD.shopId, OLD.role,
            OLD.username, OLD.passwordHash, OLD.key, OLD.firstName, OLD.lastName, OLD.middleName, OLD.email,
            OLD.phone, OLD.skype, OLD.telegram, OLD.comment, OLD.isDisabled, OLD.createdAt, OLD.ownerId, OLD.updatedAt,
            OLD.moderatorId, 3);
END;

-- TABLE user_auth

CREATE TABLE user_auth
(
    id          INT PRIMARY KEY AUTO_INCREMENT          NOT NULL,
    userId      INT                                     NOT NULL,
    token       VARCHAR(100)                            NOT NULL,
    ipAddress   VARCHAR(15)                             NOT NULL,
    createdAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP()   NOT NULL,
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL,
    CONSTRAINT user_auth_user_fk FOREIGN KEY (userId) REFERENCES user (id) ON DELETE RESTRICT
);

CREATE TABLE user_auth_log
(
    id          INT,
    userId      INT,
    token       VARCHAR(100),
    ipAddress   VARCHAR(15),
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER user_auth_after_insert
    AFTER INSERT
    ON user_auth
    FOR EACH ROW
BEGIN
    INSERT INTO user_auth_log
    VALUES (NEW.id, NEW.userId, NEW.token, NEW.ipAddress, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId,
            1);
END;

CREATE TRIGGER user_auth_after_update
    AFTER UPDATE
    ON user_auth
    FOR EACH ROW
BEGIN
    INSERT INTO user_auth_log
    VALUES (NEW.id, NEW.userId, NEW.token, NEW.ipAddress, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId,
            2);
END;

CREATE TRIGGER user_auth_before_delete
    BEFORE DELETE
    ON user_auth
    FOR EACH ROW
BEGIN
    INSERT INTO user_auth_log
    VALUES (OLD.id, OLD.userId, OLD.token, OLD.ipAddress, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId,
            3);
END;

-- TABLE good

CREATE TABLE good
(
    id          INT PRIMARY KEY AUTO_INCREMENT          NOT NULL,
    externalId  VARCHAR(100),
    accountId   INT                                     NOT NULL,
    name        VARCHAR(500) COLLATE utf8mb4_unicode_ci NOT NULL,
    createdAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP()   NOT NULL,
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL,
    CONSTRAINT good_account_fk FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT
);

ALTER TABLE good
    ADD UNIQUE good_unique_index (accountId, externalId);

ALTER TABLE good
    AUTO_INCREMENT = 3000000;

CREATE TABLE good_log
(
    id          INT,
    externalId  VARCHAR(100),
    accountId   INT,
    name        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER good_after_insert
    AFTER INSERT
    ON good
    FOR EACH ROW
BEGIN
    INSERT INTO good_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.name, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId,
            1);
END;

CREATE TRIGGER good_after_update
    AFTER UPDATE
    ON good
    FOR EACH ROW
BEGIN
    INSERT INTO good_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.name, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId,
            2);
END;

CREATE TRIGGER good_before_delete
    BEFORE DELETE
    ON good
    FOR EACH ROW
BEGIN
    INSERT INTO good_log
    VALUES (OLD.id, OLD.externalId, OLD.accountId, OLD.name, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId,
            3);
END;

-- TABLE incident_type

CREATE TABLE incident_type
(
    id          INT PRIMARY KEY AUTO_INCREMENT          NOT NULL,
    accountId   INT                                     NOT NULL,
    name        VARCHAR(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    data        TEXT COLLATE utf8mb4_unicode_ci         NOT NULL,
    createdAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP()   NOT NULL,
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL,
    CONSTRAINT incident_type_account_fk FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT
);

ALTER TABLE incident_type
    AUTO_INCREMENT = 2000;

CREATE TABLE incident_type_log
(
    id          INT,
    accountId   INT,
    name        VARCHAR(100) COLLATE utf8mb4_unicode_ci,
    data        TEXT COLLATE utf8mb4_unicode_ci,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER incident_type_after_insert
    AFTER INSERT
    ON incident_type
    FOR EACH ROW
BEGIN
    INSERT INTO incident_type_log
    VALUES (NEW.id, NEW.accountId, NEW.name, NEW.data, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER incident_type_after_update
    AFTER UPDATE
    ON incident_type
    FOR EACH ROW
BEGIN
    INSERT INTO incident_type_log
    VALUES (NEW.id, NEW.accountId, NEW.name, NEW.data, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER incident_type_before_delete
    BEFORE DELETE
    ON incident_type
    FOR EACH ROW
BEGIN
    INSERT INTO incident_type_log
    VALUES (OLD.id, OLD.accountId, OLD.name, OLD.data, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;

-- file

CREATE TABLE file
(
    id            INT PRIMARY KEY AUTO_INCREMENT          NOT NULL,
    accountId     INT,
    fileName      VARCHAR(100)                            NOT NULL,
    fileNameExt   VARCHAR(20)                             NOT NULL,
    fileSize      INT                                     NOT NULL,
    fileType      VARCHAR(100),
    pathName      VARCHAR(500)                            NOT NULL,
    downloadCount INT       DEFAULT 0                     NOT NULL,
    createdAt     TIMESTAMP DEFAULT CURRENT_TIMESTAMP()   NOT NULL,
    ownerId       INT                                     NOT NULL,
    updatedAt     TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId   INT                                     NULL,
    CONSTRAINT file_account_fk FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT
);

ALTER TABLE file
    AUTO_INCREMENT = 10000,
    ADD UNIQUE file_unique_index (pathName);

CREATE TABLE file_log
(
    id            INT,
    accountId     INT,
    fileName      VARCHAR(100),
    fileNameExt   VARCHAR(20),
    fileSize      INT,
    fileType      VARCHAR(100),
    pathName      VARCHAR(500),
    downloadCount INT,
    createdAt     TIMESTAMP,
    ownerId       INT,
    updatedAt     TIMESTAMP,
    moderatorId   INT,
    __type        TINYINT NOT NULL
);

CREATE TRIGGER file_after_insert
    AFTER INSERT
    ON file
    FOR EACH ROW
BEGIN
    INSERT INTO file_log
    VALUES (NEW.id, NEW.accountId, NEW.fileName, NEW.fileNameExt, NEW.fileSize, NEW.fileType, NEW.pathName,
            NEW.downloadCount, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER file_after_update
    AFTER UPDATE
    ON file
    FOR EACH ROW
BEGIN
    INSERT INTO file_log
    VALUES (NEW.id, NEW.accountId, NEW.fileName, NEW.fileNameExt, NEW.fileSize, NEW.fileType, NEW.pathName,
            NEW.downloadCount, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER file_before_delete
    BEFORE DELETE
    ON file
    FOR EACH ROW
BEGIN
    INSERT INTO file_log
    VALUES (OLD.id, OLD.accountId, OLD.fileName, OLD.fileNameExt, OLD.fileSize, OLD.fileType, OLD.pathName,
            OLD.downloadCount, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;

-- incident_dayly_report

CREATE TABLE incident_dayly_report
(
    id          INT PRIMARY KEY AUTO_INCREMENT          NOT NULL,
    accountId   INT                                     NOT NULL,
    fileId      INT                                     NOT NULL,
    `date`      DATE                                    NOT NULL,
    createdAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP()   NOT NULL,
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL,
    CONSTRAINT incident_dayly_report_account_fk FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT incident_dayly_report_file_fk FOREIGN KEY (fileId) REFERENCES file (id) ON DELETE RESTRICT
);

ALTER TABLE incident_dayly_report
    AUTO_INCREMENT = 20000,
    ADD UNIQUE incident_dayly_report_unique_index (accountId, date);

CREATE TABLE incident_dayly_report_log
(
    id          INT,
    accountId   INT,
    fileId      INT,
    `date`      DATE,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER incident_dayly_report_after_insert
    AFTER INSERT
    ON incident_dayly_report
    FOR EACH ROW
BEGIN
    INSERT INTO incident_dayly_report_log
    VALUES (NEW.id, NEW.accountId, NEW.fileId, NEW.date, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER incident_dayly_report_after_update
    AFTER UPDATE
    ON incident_dayly_report
    FOR EACH ROW
BEGIN
    INSERT INTO incident_dayly_report_log
    VALUES (NEW.id, NEW.accountId, NEW.fileId, NEW.date, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER incident_dayly_report_before_delete
    BEFORE DELETE
    ON incident_dayly_report
    FOR EACH ROW
BEGIN
    INSERT INTO incident_dayly_report_log
    VALUES (OLD.id, OLD.accountId, OLD.fileId, OLD.date, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;

-- TABLE incident

CREATE TABLE incident
(
    id             INT PRIMARY KEY AUTO_INCREMENT                                                       NOT NULL,
    externalId     VARCHAR(100),
    shopId         INT                                                                                  NOT NULL,
    incidentTypeId INT                                                                                  NOT NULL,
    goodId         INT                                                                                  NOT NULL,
    `date`         DATE                                                                                 NOT NULL,
    state          ENUM ('TODO', 'STARTED', 'FINISHED', 'REPORTED', 'DONE') DEFAULT 'TODO'              NOT NULL,
    inputData      TEXT COLLATE utf8mb4_unicode_ci,
    report         TEXT COLLATE utf8mb4_unicode_ci,
    isReported     TINYINT                                                  DEFAULT 0                   NOT NULL,
    createdAt      TIMESTAMP                                                DEFAULT CURRENT_TIMESTAMP() NOT NULL,
    ownerId        INT                                                                                  NOT NULL,
    updatedAt      TIMESTAMP ON UPDATE CURRENT_TIMESTAMP()                                              NULL,
    moderatorId    INT                                                                                  NULL,
    CONSTRAINT incident_shop_fk FOREIGN KEY (shopId) REFERENCES shop (id) ON DELETE RESTRICT,
    CONSTRAINT incident_incident_type_fk FOREIGN KEY (incidentTypeId) REFERENCES incident_type (id) ON DELETE RESTRICT,
    CONSTRAINT incident_good_fk FOREIGN KEY (goodId) REFERENCES good (id) ON DELETE RESTRICT
);

ALTER TABLE incident
    ADD UNIQUE incident_unique_index (shopId, goodId, date);

ALTER TABLE incident
    AUTO_INCREMENT = 4000000;

CREATE TABLE incident_log
(
    id             INT,
    externalId     VARCHAR(100),
    shopId         INT,
    incidentTypeId INT,
    goodId         INT,
    `date`         DATE,
    state          ENUM ('TODO', 'STARTED', 'FINISHED', 'REPORTED', 'DONE'),
    inputData      TEXT COLLATE utf8mb4_unicode_ci,
    report         TEXT COLLATE utf8mb4_unicode_ci,
    isReported     TINYINT,
    createdAt      TIMESTAMP,
    ownerId        INT,
    updatedAt      TIMESTAMP,
    moderatorId    INT,
    __type         TINYINT NOT NULL
);

CREATE TRIGGER incident_after_insert
    AFTER INSERT
    ON incident
    FOR EACH ROW
BEGIN
    INSERT INTO incident_log
    VALUES (NEW.id, NEW.externalId, NEW.shopId, NEW.incidentTypeId, NEW.goodId, NEW.date, NEW.state, NEW.inputData,
            NEW.report, NEW.isReported, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER incident_after_update
    AFTER UPDATE
    ON incident
    FOR EACH ROW
BEGIN
    INSERT INTO incident_log
    VALUES (NEW.id, NEW.externalId, NEW.shopId, NEW.incidentTypeId, NEW.goodId, NEW.date, NEW.state, NEW.inputData,
            NEW.report, NEW.isReported, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER incident_before_delete
    BEFORE DELETE
    ON incident
    FOR EACH ROW
BEGIN
    INSERT INTO incident_log
    VALUES (OLD.id, OLD.externalId, OLD.shopId, OLD.incidentTypeId, OLD.goodId, OLD.date, OLD.state, OLD.inputData,
            OLD.report, OLD.isReported, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;

-- TABLE log

CREATE TABLE log
(
    id         INT PRIMARY KEY AUTO_INCREMENT        NOT NULL,
    accountId  INT,
    userId     INT,
    shopId     INT,
    incidentId INT,
    type       VARCHAR(30)                           NULL,
    message    TEXT                                  NULL,
    reason     VARCHAR(200)                          NULL,
    ownerId    INT,
    createdAt  TIMESTAMP DEFAULT CURRENT_TIMESTAMP() NOT NULL
);
