ALTER TABLE cube_incident_user_stats
    ADD COLUMN date DATE NOT NULL DEFAULT NOW() AFTER userId;

ALTER TABLE cube_incident_shop_stats
    ADD COLUMN date DATE NOT NULL DEFAULT NOW() AFTER shopId;
