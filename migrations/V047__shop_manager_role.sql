-- fill links from exists data
INSERT INTO shop_manager (shopId, roleId, userId, ownerId)
SELECT id, 1, dfId, 1000
FROM shop
WHERE dfId IS NOT NULL;

INSERT INTO shop_manager (shopId, roleId, userId, ownerId)
SELECT id, 2, tmId, 1000
FROM shop
WHERE tmId IS NOT NULL;

INSERT INTO shop_manager (shopId, roleId, userId, ownerId)
SELECT id, 3, upfId, 1000
FROM shop
WHERE upfId IS NOT NULL;

INSERT INTO shop_manager (shopId, roleId, userId, ownerId)
SELECT shopId, 4, id, 1000
FROM user
WHERE role = 'DM'
  AND shopId IS NOT NULL;

INSERT INTO shop_manager (shopId, roleId, userId, ownerId)
SELECT shopId, 5, id, 1000
FROM user
WHERE role = 'ZDM'
  AND shopId IS NOT NULL;
