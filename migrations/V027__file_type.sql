-- TABLE file_type

CREATE TABLE file_type
(
    id        INT AUTO_INCREMENT PRIMARY KEY,
    ext       VARCHAR(10)                             NOT NULL UNIQUE,
    mimeType  VARCHAR(200)                            NOT NULL,
    createdAt TIMESTAMP                               NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    updatedAt TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NOT NULL DEFAULT CURRENT_TIMESTAMP()
);

-- Первые данные
INSERT INTO file_type (id, ext, mimeType)
VALUES (101, 'zip', 'application/zip'),
       (102, 'rar', 'application/vnd.rar'),
       (103, 'png', 'image/png'),
       (104, 'jpg', 'image/jpeg'),
       (105, 'jpeg', 'image/jpeg'),
       (106, 'svg', 'image/svg+xml'),
       (107, 'bmp', 'image/bmp'),
       (108, 'gif', 'image/gif'),
       (109, 'tif', 'image/tiff'),
       (110, 'tiff', 'image/tiff'),
       (111, 'txt', 'text/plain'),
       (112, 'json', 'application/json'),
       (113, 'xml', 'application/xml'),
       (114, 'html', 'text/html'),
       (115, 'pdf', 'application/pdf'),
       (116, 'csv', 'text/csv'),
       (117, 'docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
       (118, 'xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

CREATE TABLE file_type__log
(
    id        INT,
    ext       VARCHAR(10),
    mimeType  VARCHAR(200),
    createdAt TIMESTAMP,
    updatedAt TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(),
    __type    TINYINT NOT NULL
);

CREATE TRIGGER file_type__after_insert
    AFTER INSERT
    ON file_type
    FOR EACH ROW
BEGIN
    INSERT INTO file_type__log
    VALUES (NEW.id, NEW.ext, NEW.mimeType, NEW.createdAt, NEW.updatedAt, 1);
END;

CREATE TRIGGER file_type__after_update
    AFTER UPDATE
    ON file_type
    FOR EACH ROW
BEGIN
    INSERT INTO file_type__log
    VALUES (NEW.id, NEW.ext, NEW.mimeType, NEW.createdAt, NEW.updatedAt, 2);
END;

CREATE TRIGGER file_type__before_delete
    BEFORE DELETE
    ON file_type
    FOR EACH ROW
BEGIN
    INSERT INTO file_type__log
    VALUES (OLD.id, OLD.ext, OLD.mimeType, OLD.createdAt, OLD.updatedAt, 3);
END;

-- update files
DROP TRIGGER file_after_insert;
DROP TRIGGER file_after_update;
DROP TRIGGER file_before_delete;

ALTER TABLE file
    ADD COLUMN fileTypeId INT NOT NULL AFTER fileName;

ALTER TABLE file_log
    ADD COLUMN fileTypeId INT AFTER fileName;

UPDATE file SET fileTypeId=101 WHERE fileNameExt = 'zip';
UPDATE file SET fileTypeId=103 WHERE fileNameExt = 'png';
UPDATE file SET fileTypeId=114 WHERE fileNameExt = 'html';
UPDATE file SET fileTypeId=116 WHERE fileNameExt = 'csv';
UPDATE file SET fileTypeId=118 WHERE fileNameExt = 'xlsx';

UPDATE file_log SET fileTypeId=101 WHERE fileNameExt = 'zip';
UPDATE file_log SET fileTypeId=103 WHERE fileNameExt = 'png';
UPDATE file_log SET fileTypeId=114 WHERE fileNameExt = 'html';
UPDATE file_log SET fileTypeId=116 WHERE fileNameExt = 'csv';
UPDATE file_log SET fileTypeId=118 WHERE fileNameExt = 'xlsx';

ALTER TABLE file
    ADD CONSTRAINT fk__file__file_type FOREIGN KEY (fileTypeId) REFERENCES file_type (id) ON DELETE RESTRICT;

ALTER TABLE file
    DROP COLUMN fileNameExt,
    DROP COLUMN fileType;

ALTER TABLE file
    CHANGE COLUMN fileName name VARCHAR(100) NOT NULL,
    CHANGE COLUMN fileSize size INT NOT NULL;

ALTER TABLE file_log
    DROP COLUMN fileNameExt,
    DROP COLUMN fileType;

ALTER TABLE file_log
    CHANGE COLUMN fileName name VARCHAR(100),
    CHANGE COLUMN fileSize size INT;

RENAME TABLE file_log TO file__log;

-- create triggers
CREATE TRIGGER file__after_insert
    AFTER INSERT
    ON file
    FOR EACH ROW
BEGIN
    INSERT INTO file__log
    VALUES (NEW.id, NEW.accountId, NEW.name, NEW.fileTypeId, NEW.size, NEW.pathName,
            NEW.downloadCount, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER file__after_update
    AFTER UPDATE
    ON file
    FOR EACH ROW
BEGIN
    INSERT INTO file__log
    VALUES (NEW.id, NEW.accountId, NEW.name, NEW.fileTypeId, NEW.size, NEW.pathName,
            NEW.downloadCount, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER file__before_delete
    BEFORE DELETE
    ON file
    FOR EACH ROW
BEGIN
    INSERT INTO file__log
    VALUES (OLD.id, OLD.accountId, OLD.name, OLD.fileTypeId, OLD.size, OLD.pathName,
            OLD.downloadCount, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;
