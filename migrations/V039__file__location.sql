ALTER TABLE file
    ADD COLUMN location ENUM ('FS', 'CDN') NOT NULL DEFAULT 'FS' AFTER size;
