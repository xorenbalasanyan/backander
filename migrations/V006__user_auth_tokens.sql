DROP TRIGGER user_auth_after_insert;
DROP TRIGGER user_auth_after_update;
DROP TRIGGER user_auth_before_delete;

ALTER TABLE user_auth
    CHANGE COLUMN token authToken VARCHAR(100) NOT NULL,
    ADD COLUMN refreshToken       VARCHAR(100) NOT NULL AFTER authToken;
ALTER TABLE user_auth_log
    CHANGE COLUMN token authToken VARCHAR(100) NOT NULL,
    ADD COLUMN refreshToken       VARCHAR(100) NOT NULL AFTER authToken;

CREATE TRIGGER user_auth_after_insert
    AFTER INSERT
    ON user_auth
    FOR EACH ROW
BEGIN
    INSERT INTO user_auth_log
    VALUES (NEW.id, NEW.userId, NEW.authToken, NEW.refreshToken, NEW.ipAddress, NEW.createdAt, NEW.ownerId,
            NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER user_auth_after_update
    AFTER UPDATE
    ON user_auth
    FOR EACH ROW
BEGIN
    INSERT INTO user_auth_log
    VALUES (NEW.id, NEW.userId, NEW.authToken, NEW.refreshToken, NEW.ipAddress, NEW.createdAt, NEW.ownerId,
            NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER user_auth_before_delete
    BEFORE DELETE
    ON user_auth
    FOR EACH ROW
BEGIN
    INSERT INTO user_auth_log
    VALUES (OLD.id, OLD.userId, OLD.authToken, OLD.refreshToken, OLD.ipAddress, OLD.createdAt, OLD.ownerId,
            OLD.updatedAt, OLD.moderatorId, 3);
END;
