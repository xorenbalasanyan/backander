CREATE TABLE incident_history
(
    id                  INT PRIMARY KEY AUTO_INCREMENT,
    externalId          VARCHAR(100)                          NULL,
    shopId              INT                                   NOT NULL,
    shopDiscountId      INT                                   NULL,
    incidentTypeId      INT                                   NOT NULL,
    incidentTypeVersion INT                                   NOT NULL,
    goodId              INT                                   NOT NULL,
    date                DATE                                  NOT NULL,
    state               VARCHAR(20)                           NOT NULL,
    inputData           TEXT COLLATE utf8mb4_unicode_ci       NULL,
    report              TEXT COLLATE utf8mb4_unicode_ci       NULL,
    isReported          TINYINT                               NOT NULL,
    createdAt           TIMESTAMP DEFAULT CURRENT_TIMESTAMP() NOT NULL,
    ownerId             INT                                   NOT NULL,
    updatedAt           TIMESTAMP                             NULL,
    moderatorId         INT                                   NULL,
    CONSTRAINT fk__incident_history__shop_discount FOREIGN KEY (shopDiscountId) REFERENCES shop_discount (id),
    CONSTRAINT fk__incident_history__good FOREIGN KEY (goodId) REFERENCES good (id),
    CONSTRAINT fk__incident_history__incident_type FOREIGN KEY (incidentTypeId) REFERENCES incident_type (id),
    CONSTRAINT fk__incident_history__shop FOREIGN KEY (shopId) REFERENCES shop (id)
);
