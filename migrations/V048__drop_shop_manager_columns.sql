DROP TRIGGER shop__after_insert;
DROP TRIGGER shop__after_update;
DROP TRIGGER shop__before_delete;

-- drop migrated fields
ALTER TABLE shop
    DROP FOREIGN KEY shop_dfId__user_id__fk,
    DROP COLUMN dfId,
    DROP FOREIGN KEY shop_tmId__user_id__fk,
    DROP COLUMN tmId,
    DROP FOREIGN KEY shop_upfId__user_id__fk,
    DROP COLUMN upfId;

ALTER TABLE shop__log
    DROP COLUMN dfId,
    DROP COLUMN tmId,
    DROP COLUMN upfId;

-- recreate triggers
CREATE TRIGGER shop__after_insert
    AFTER INSERT
    ON shop
    FOR EACH ROW
BEGIN
    INSERT INTO shop__log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.name, NEW.regionId, NEW.city, NEW.address,
            NEW.phone, NEW.email, NEW.description, NEW.timeOffset, NEW.isDeleted, NEW.createdAt,
            NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER shop__after_update
    AFTER UPDATE
    ON shop
    FOR EACH ROW
BEGIN
    INSERT INTO shop__log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.name, NEW.regionId, NEW.city, NEW.address,
            NEW.phone, NEW.email, NEW.description, NEW.timeOffset, NEW.isDeleted, NEW.createdAt,
            NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER shop__before_delete
    BEFORE DELETE
    ON shop
    FOR EACH ROW
BEGIN
    INSERT INTO shop__log
    VALUES (OLD.id, OLD.externalId, OLD.accountId, OLD.name, OLD.regionId, OLD.city, OLD.address,
            OLD.phone, OLD.email, OLD.description, OLD.timeOffset, OLD.isDeleted, OLD.createdAt,
            OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;
