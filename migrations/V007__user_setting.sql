-- TABLE user_setting

CREATE TABLE user_setting
(
    id          INT PRIMARY KEY AUTO_INCREMENT          NOT NULL,
    userId      INT                                     NOT NULL,
    `key`       VARCHAR(50)                             NOT NULL,
    value       TEXT COLLATE utf8mb4_unicode_ci         NOT NULL,
    createdAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP()   NOT NULL,
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL,
    CONSTRAINT user_setting_user_fk FOREIGN KEY (userId) REFERENCES user (id) ON DELETE RESTRICT
);

ALTER TABLE user_setting
    AUTO_INCREMENT = 4000;

CREATE TABLE user_setting_log
(
    id          INT,
    userId      INT,
    `key`       VARCHAR(50),
    value       TEXT COLLATE utf8mb4_unicode_ci,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER user_setting_after_insert
    AFTER INSERT
    ON user_setting
    FOR EACH ROW
BEGIN
    INSERT INTO user_setting_log
    VALUES (NEW.id, NEW.userId, NEW.`key`, NEW.value, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER user_setting_after_update
    AFTER UPDATE
    ON user_setting
    FOR EACH ROW
BEGIN
    INSERT INTO user_setting_log
    VALUES (NEW.id, NEW.userId, NEW.`key`, NEW.value, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER user_setting_before_delete
    BEFORE DELETE
    ON user_setting
    FOR EACH ROW
BEGIN
    INSERT INTO user_setting_log
    VALUES (OLD.id, OLD.userId, OLD.`key`, OLD.value, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;
