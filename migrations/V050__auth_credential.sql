-- table contains authorization requisits
CREATE TABLE auth_credential
(
    id           INT PRIMARY KEY AUTO_INCREMENT                                      NOT NULL,
    accountId    INT       DEFAULT 100                                               NOT NULL,
    userId       INT                                                                 NOT NULL,
    username     VARCHAR(100) COLLATE utf8mb4_unicode_ci                             NOT NULL,
    passwordHash VARCHAR(100)                                                        NULL,
    `key`        VARCHAR(100) COLLATE utf8mb4_unicode_ci                             NOT NULL,
    createdAt    TIMESTAMP DEFAULT CURRENT_TIMESTAMP()                               NOT NULL,
    ownerId      INT                                                                 NOT NULL,
    updatedAt    TIMESTAMP DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP() NOT NULL,
    moderatorId  INT                                                                 NULL,
    CONSTRAINT fk__auth_credential__user FOREIGN KEY (userId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__auth_credential__owner FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__auth_credential__moderator FOREIGN KEY (moderatorid) REFERENCES user (id) ON DELETE RESTRICT,
    UNIQUE auth_credential__unique_index (accountId, userId)
);

-- fill the table from exist data
INSERT INTO auth_credential (userId, username, passwordHash, `key`, createdAt, ownerId)
SELECT id, username, passwordHash, `key`, createdAt, id
FROM user
WHERE username IS NOT NULL;

DROP TRIGGER user__after_insert;
DROP TRIGGER user__after_update;
DROP TRIGGER user__before_delete;

-- drop migrated fields
ALTER TABLE user
    DROP FOREIGN KEY `user_account_fk`,
    DROP INDEX `user_unique_index`,
    DROP COLUMN username,
    DROP COLUMN passwordHash,
    DROP COLUMN `key`;

ALTER TABLE user
    ADD CONSTRAINT fk__user__account FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT;

ALTER TABLE user__log
    DROP COLUMN username,
    DROP COLUMN passwordHash,
    DROP COLUMN `key`;

-- recreate triggers
CREATE TRIGGER user__after_insert
    AFTER INSERT
    ON user
    FOR EACH ROW
BEGIN
    INSERT INTO user__log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.role, NEW.firstName, NEW.lastName,
            NEW.middleName, NEW.email, NEW.phone, NEW.skype, NEW.telegram, NEW.comment,
            NEW.isDeleted, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER user__after_update
    AFTER UPDATE
    ON user
    FOR EACH ROW
BEGIN
    INSERT INTO user__log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.role, NEW.firstName, NEW.lastName,
            NEW.middleName, NEW.email, NEW.phone, NEW.skype, NEW.telegram, NEW.comment,
            NEW.isDeleted, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER user__before_delete
    BEFORE DELETE
    ON user
    FOR EACH ROW
BEGIN
    INSERT INTO user__log
    VALUES (OLD.id, OLD.externalId, OLD.accountId, OLD.role, OLD.firstName, OLD.lastName,
            OLD.middleName, OLD.email, OLD.phone, OLD.skype, OLD.telegram, OLD.comment,
            OLD.isDeleted, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;
