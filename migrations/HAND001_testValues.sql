-- создание тестовых данных

-- аккаунт
INSERT INTO account (id, title, ownerId)
VALUES (100, 'Test Account', -1);

-- админ
INSERT INTO user (id, accountId, role, firstName, ownerId)
VALUES (1000, 100, 'ADMIN', 'Admin', -1);

-- креды
INSERT INTO auth_credential (accountId, userId, username, `key`, ownerId)
VALUES (100, 1000, 'testadmin', 'testadmin', 1000);

-- роли
INSERT INTO shop_manager_role (id, accountid, title, shortTitle, engShortTitle, subLevel, isShopUser, ownerId)
VALUES (1, 100, 'Директор филиала', 'ДФ', 'DF', 100, 0, 1000),
       (2, 100, 'Территориальный менеджер', 'ТМ', 'TM', 90, 0, 1000),
       (3, 100, 'Управляющий по формату', 'УПФ', 'UPF', 80, 0, 1000),
       (4, 100, 'Директор магазина', 'ДМ', 'DM', 20, 1, 1000),
       (5, 100, 'Заместитель директора магазина', 'ЗДМ', 'ZDM', 10, 1, 1000);
